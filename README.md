# v_2_0
## Arquivo de Pedido
gerador-layouts arquivoDePedido IdentificacoesCliente Identificador:int32:0:1 CnpjCliente:string:1:15 TipoFaturamento:int32:15:16 ApontadorPromocao:string:16:29 CodigoControle:string:29:31

gerador-layouts arquivoDePedido NumeroPedido Identificador:int32:0:1 NumeroPedidoPharmaLink:string:1:9 CnpjDistribuidor:string:9:23 NumeroPedidoCliente:string:23:31

gerador-layouts arquivoDePedido EmBrancoRegistro3 Identificador:int32:0:1 TipoPagamento:int32:1:2 SemUtilizacao:string:2:31

gerador-layouts arquivoDePedido EmBrancoRegistro4 Identificador:int32:0:1 SemUtilizacao:string:1:31

gerador-layouts arquivoDePedido DataPedido Indentificador:int32:0:1 Data:string:1:9 SemUtilizacao:string:9:31

gerador-layouts arquivoDePedido HoraPedido Indentificador:int32:0:1 Hora:string:1:7 SemUtilizacao:string:7:31

gerador-layouts arquivoDePedido item Identificador:int32:0:1 CodigoEanProduto:int64:1:14 QuantidadePedida:int32:14:18 TipoOcorrencia:int32:18:20 CodigoProdutoCliente:string:20:27 SemUtilizacao:string:27:31

gerador-layouts arquivoDePedido rodape Identificador:int32:0:1 QUantidadeItens:int32:1:3 SemUtilizacao:string:3:31


## Arquivo de Retorno
gerador-layouts retornoPedido IdentificacoesCliente Indetificador:string:0:2 CnpjCliente:string:2:16 NumeroPedidoCliente:string:16:24 NumeroPedidoPharmaLink:string:24:32 SemUtilizacao:string:32:33

gerador-layouts retornoPedido DadosMercadorias Identificador:string:0:2 DataEmissao:string:2:10 SemUtilizacao1:float64:10:20:2 SemUtilizacao2:string:20:33

gerador-layouts retornoPedido DadosDescontos Identificador:string:0:2 SemUtilizacao1:float64:2:12:2 SemUtilizacao2:float64:12:22:2 SemUtilizacao3:string:22:33

gerador-layouts retornoPedido DadosFiscais4 Identificador:string:0:2 SemUtilizacao1:float64:2:12:2 SemUtilizacao2:float64:12:22:2 SemUtilizacao3:string:22:33

gerador-layouts retornoPedido DadosFiscais5 Identificador:string:0:2 SemUtilizacao1:float64:2:6:2 SemUtilizacao2:float64:6:16:2 SemUtilizacao3:string:16:33

gerador-layouts retornoPedido itens Identificador:string:0:2 CodigoEanProduto:int64:2:15 QuantidadeAtendidaProduto:int32:15:19 SemUtilizacao:int64:19:30 MotivoNaoAtendimento:int32:30:32 Retorno:string:32:33

gerador-layouts retornoPedido Indentificador:int32:0:2 LivreParaCliente:string:2:24 SemUtilizacao:string:24:33



#BPR
## Arquivo de Pedido
gerador-layouts arquivoDePedido cabecalho TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 IndicadorSegundaVia:int32:23:24 IndicadorCancelamento:int32:24:25 ClassePedido:string:25:28 DataEmissao:int32:28:36 HoraEmissao:string:36:40 DataInicioEntrega:int32:40:48 HoraInicioEntrega:string:48:52 DataFimEntrega:int32:51:60 HoraFimEntrega:string:60:64 DataEntrega:int32:64:72 HoraEntrega:string:72:76 DataInicioPromocao:int32:76:84 DataFimPromocao:int32:84:92 DataEmbarque:int32:92:100 DataLimiteEmbarque:int32:100:108 NumeroContato:string:108:128 NumeroTabelaPreco:string:128:148 TipoPedido:int32:148:150 CodigoTextoLivre:string:150:155 TipoNegociacao:string:155:156 CodigoCondicaoAceite:string:156:157 EamComprador:int64:157:170 CGCComprador:int64:170:185 NomeComprador:string:185:220 EamFornecedor:int64:220:233 CGCFornecedor:int64:233:248 NomeFornecedor:string:248:283 CodigoInternoFornecedor:string:283:292 EanLocalEntrega:int64:292:305 CGCLocalEntrega:int64:305:320 EanLocalFatura:int64:320:333 CGCLocalFatura:int64:333:348 EanEmissorPedido:int64:348:361 EanLocalEmbarque:int64:361:374 IdentificacaoDocaDescarga:string:374:384 NumeroPromocao:string:384:394 IdentificacaoDepartamentoVendas:string:394:396 CodigoMoedaUtilizada:string:396:399

gerador-layouts arquivoDePedido observacao TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 Observacao1:string:36:106 Observacao2:string:106:176 Observacao3:string:176:246 Observacao4:string:246:316 Observacao5:string:316:386 Observacao6:string:386:456 Observacao7:string:456:526 Observacao8:string:526:596 Observacao9:string:596:666 Observacao10:string:666:736 Observacao11:string:736:806 Observacao12:string:806:876 Observacao13:string:876:946 Observacao14:string:946:1016 Observacao15:string:1016:1086

gerador-layouts arquivoDePedido pagamento TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int32:23:36 QualificadorTipoCondicaoPagamento:string:36:39 ReferenciaPrazoPagamento:string:39:42 ReferenciaTempoCodificada:string:42:45 TipoPeriodoCodificado:string:45:48 NumeroPeriodos:int32:48:51 DataVencimentoLiquido:int32:51:59 PorcentagemFatura:float32:59:64:2 ValorParcela:float64:64:82:2 PorcentagemDescontoFinanceiro:float32:82:87:2 ValorImpostos:float64:87:105:2 IdentificacaoCondicaoPagamento:string:105:106 DescricaoCondicaoPagamento:string:106:141 DataBase:int32:141:149 Juros:float32:149:154:2

gerador-layouts arquivoDePedido embarque TipoRegistro:string:0:2 TipoDOcumento:int32:2:3 NumeroPedido:string:3:23 EamComprador:int64:23:36 TipoCondicaoEntrega:string:36:39 TipoCodigoTransportadora:int32:39:42 EanCGCTransportadora:int64:42:57 TipoVeiculo:string:57:61 ModoTransporte:string:61:64 NomeTransportadora:string:64:99

gerador-layouts arquivoDePedido abatimentosEncargos TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 QuanlificadorAbatimentoEncargo:string:36:39 TipoAcordoAbatimentoEncargo:string:39:42 TipoRazaoAbatimentoEncargo:string:42:45 PercentualAbatimentoEncargo:float32:45:50:2 ValorAbatimentoEncargo:float64:50:67:2 QuantidadeIndividualAbatimentoEncargo:float64:67:84:2 TaxaAbatimento:float32:84:89:2

gerador-layouts arquivoDePedido dadosItem TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 TipoCodigoItem:string:54:57 QuantidadePedida:float64:57:69:3 QuantidadeGratuita:float64:69:81:3 UnidadeMedida:string:81:84 QuantidadeUnidadesConsumo:int32:84:91 ValorTotalLiquidoItem:float64:91:108:2 ValorTotalBrutoItem:float64:108:125:2 PrecoUnitarioBruto:float64:125:142:2 PrecoUnitarioLiquido:float64:142:159:2 PrecoUnitarioCalculoLiquido:float64:159:176:2 PrecoUnitarioCalculoBruto:float64:176:193:2 NumeroEmbalagens:int32:193:200 NumeroEmbalagensInternas:int32:200:207 NumeroTabelaPreco:string:207:227 DescricaoItem:string:227:262 CodigoProdutoFornecedor:string:262:272 CodigoProdutoComprador:string:272:282 CodigoCor:string:282:299 CodigoTamanho:string:299:316 DataEntrega:int32:316:324 DataInicioEntrega:int32:324:332 DataFimEntrega:int32:332:340 CodigoComplementarItem:int64:340:353 CodigoProjetoPharmalink:int32:353:360 DescontoProjetoPharmalink:float32:360:364:2 CodigoItemComercial:int32:364:369 EmbalagemProduto:string:369:371 DescontoNegociado:float32:371:375:2

gerador-layouts arquivoDePedido ObservacaoItem TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 Observacao1:string:54:124 Observacao2:string:124:194 Observacao3:string:194:264 Observacao4:string:264:334 Observacao5:string:334:404 Observacao6:string:404:474 Observacao7:string:474:544 Observacao8:string:544:614 Observacao9:string:614:684 Observacao10:string:684:754 Observacao11:string:754:824 Observacao12:string:824:894 Observacao13:string:894:964 Observacao14:string:964:1034 Observacao15:string:1034:1104

gerador-layouts arquivoDePedido crossdocking TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 NrSequencialParcela:int32:54:57 QuantidadeLoja:float64:57:69:3 EanOuDunsLocalEntrega:int64:69:82 TipoCodigoLocalEntrega:string:82:85

gerador-layouts arquivoDePedido impostos TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 AliquotaIPI:float32:54:59:2 ValorIPI:float64:59:76:2 ValorICMS:float64:76:93:2

gerador-layouts arquivoDePedido EncargosAbatimentos TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 QualificadorAbatimentoEncargo:string:54:57 TipoAcordoAbatimentoEncargo:string:57:60 TipoRazaoAbatimentoEncargo:string:60:63 PercentualAbatimentoEncargo:float32:63:68:2 ValorAbatimentoEncargo:float64:68:85:2 QuantidadeIndividualAbatimentoEncargo:float64:85:102:2 TaxaAbatimento:float32:102:107:2

gerador-layouts arquivoDePedido sumario TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 ValorTotalPedido:float64:36:53:2 ValorTotalEncargos:float64:53:70:2 ValorTotalMercadorias:float64:70:87:2 ValorTotalDescontoComercial:float64:87:104:2 ValorDespesasAcessorias:float64:104:121:2 ValorEncargosFinanceiros:float64:121:138:2 ValorFrete:float64:138:155:2 ValorTotalIPI:float64:155:172:2 ValorTotalBonificacao:float64:172:189:2 NumeroTotalLinhasItem:int32:189:194


## Retorno de Pedido
gerador-layouts retornoDePedido cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjFornecedor:int64:14:28 IdentificacaoInicialPedidoFornecedor:string:28:40 IdentificacaoFinalPedidoFornecedor:string:40:52 PrevisaoDataFaturamento:int32:52:60 PrevisaoDataEntregaMercadoria:int32:60:68 StatusProcessamentoPedido:int32:68:69 CodigoIternoLoja:string:69:73 IdentificacaoPedidoFornecedor:string:73:85 TipoAcaoPedido:string:85:86 CnpjCLiente:int64:86:100

gerador-layouts retornoDePedido detalhe CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IndentificacaoProduto:string:3:17 QuantidadeAtendida:float64:17:28:3 QuantidadeRecusada:float64:28:39:3 MotivoRejeicao:string:39:69

gerador-layouts retornoDePedido rodape CodigoRegistro:string:0:2 TotalItensAtendidos:int32:2:8 TotalQuantidadeAtendida:float64:8:19:3 TotaisItensRecusados:int32:19:25 TotalQuantidadeRecusado:float64:25:36:3


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjEmissorNotaFiscal:string:14:28 NumeroNotaFiscal:int32:28:34 SerieNotaFiscal:string:34:37 DataEmissaoNotaFiscal:int32:37:45 CodigoFiscalOperacao:int32:45:49 CodigoValorFiscal:int32:49:50 ValorBrutoNotaFiscal:float64:50:65:2 ValorContabilNotaFiscal:float64:65:80:2 ValorTributadoNotaFiscal:float64:80:95:2 ValorOutrasNotaFiscal:float64:95:110:2 BaseICMSRetido:float64:110:125:2 ValorICMSRetido:float64:125:140:2 BaseICMSEntrada:float64:140:155:2 ValorICMSEntrada:float64:155:170:2 BaseIPI:float64:170:185:2 ValorIPI:float64:185:200:2 CGCTransportadora:string:200:214 TipoFrete:string:214:215 ValorFrete:float64:215:230:2 PercentualDescontoComercial:float32:230:236:4 PercentualDescontoRepasseICMS:float32:236:242:4 TotalItens:int64:242:255 TotalUnidades:float64:255:266:3 TotalNotasPedido:int32:266:272 ValorDescontoICMSNormal:float64:272:287:2 ValorDescontoPIS:float64:287:302:2 ValorDescontoCofins:float64:302:317:2 TipoNotaFiscalEntrada:int32:317:319 ChaveAcessoNotaFiscalEletronica:string:319:363 CodigoInternoLoja:string:363:367 NumeroCorrelativoPedidoFornecedor:int32:367:371 CodigoEanDistribuidor:int64:371:384 CodigoEanPDV:int64:384:397 HoraMinutoNota:int32:397:401

gerador-layouts arquivoDeNotaFiscal produtos CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IdentificacaoProduto:string:3:17 EmbalagemProduto:string:17:19 QuantidadeEmbalagens:float64:19:30:3 CodigoFiscalOperacao:int32:30:34 SituacaoTributaria:string:34:36 PrecoProduto:float64:36:51:5 PercentualDescontoItem:float32:51:57:4 ValorDescontoItem:float64:57:72:5 PercentualDescontoRepasse:float32:72:78:2 ValorDescontoRepasse:float64:78:93:5 PercentualDescontoComercial:float32:93:99:5 ValorDescontoComercial:float64:99:114:5 ValorDespesaAcessorias:float64:114:129:5 ValorDespesaEmbalagem:float64:129:144:5

gerador-layouts arquivoDeNotaFiscal loteFabricacaoProdutos CodigoRegistro:string:0:2 LoteFabricacao:string:2:22 QuantidadeProduto:float64:22:33:3 DataValidade:int32:33:41 DataFabricacao:int32:41:49

gerador-layouts arquivoDeNotaFiscal titulos CodigoRegistro:string:0:2 NumeroTitulo:string:2:12 LocalPagamento:string:12:16 DataVencimento:int32:16:24 ValorBruto:float64:24:39:2 ValorLiquido:float64:39:54:2 DataAntecipacao:int32:54:62 DescontoAntecipado:float32:62:66:2 DescontoLimite:float32:66:70:2



### GBB
## Arquivo de pedido
gerador-layouts arquivoDePedido cabecalho TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 IndicadorSegundaVia:int32:23:24 IndicadorCancelamento:int32:24:25 ClassePedido:string:25:28 DataEmissao:int32:28:36 HoraEmissao:string:36:40 DataInicioEntrega:int32:40:48 HoraInicioEntrega:string:48:52 DataFimEntrega:int32:51:60 HoraFimEntrega:string:60:64 DataEntrega:int32:64:72 HoraEntrega:string:72:76 DataInicioPromocao:int32:76:84 DataFimPromocao:int32:84:92 DataEmbarque:int32:92:100 DataLimiteEmbarque:int32:100:108 NumeroContato:string:108:128 NumeroTabelaPreco:string:128:148 TipoPedido:int32:148:150 CodigoTextoLivre:string:150:155 TipoNegociacao:string:155:156 CodigoCondicaoAceite:string:156:157 EamComprador:int64:157:170 CGCComprador:int64:170:185 NomeComprador:string:185:220 EamFornecedor:int64:220:233 CGCFornecedor:int64:233:248 NomeFornecedor:string:248:283 CodigoInternoFornecedor:string:283:292 EanLocalEntrega:int64:292:305 CGCLocalEntrega:int64:305:320 EanLocalFatura:int64:320:333 CGCLocalFatura:int64:333:348 EanEmissorPedido:int64:348:361 EanLocalEmbarque:int64:361:374 IdentificacaoDocaDescarga:string:374:384 NumeroPromocao:string:384:394 IdentificacaoDepartamentoVendas:string:394:396 CodigoMoedaUtilizada:string:396:399 CodigoInternoLoja:string:399:403 NumeroCorrelativoPedidoFornecedor:int32:403:407

gerador-layouts arquivoDePedido observacao TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 Observacao1:string:36:106 Observacao2:string:106:176 Observacao3:string:176:246 Observacao4:string:246:316 Observacao5:string:316:386 Observacao6:string:386:456 Observacao7:string:456:526 Observacao8:string:526:596 Observacao9:string:596:666 Observacao10:string:666:736 Observacao11:string:736:806 Observacao12:string:806:876 Observacao13:string:876:946 Observacao14:string:946:1016 Observacao15:string:1016:1086

gerador-layouts arquivoDePedido pagamento TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int32:23:36 QualificadorTipoCondicaoPagamento:string:36:39 ReferenciaPrazoPagamento:string:39:42 ReferenciaTempoCodificada:string:42:45 TipoPeriodoCodificado:string:45:48 NumeroPeriodos:int32:48:51 DataVencimentoLiquido:int32:51:59 PorcentagemFatura:float32:59:64:2 ValorParcela:float64:64:82:2 PorcentagemDescontoFinanceiro:float32:82:87:2 ValorImpostos:float64:87:105:2 IdentificacaoCondicaoPagamento:string:105:106 DescricaoCondicaoPagamento:string:106:141 DataBase:int32:141:149 Juros:float32:149:154:2

gerador-layouts arquivoDePedido embarque TipoRegistro:string:0:2 TipoDOcumento:int32:2:3 NumeroPedido:string:3:23 EamComprador:int64:23:36 TipoCondicaoEntrega:string:36:39 TipoCodigoTransportadora:int32:39:42 EanCGCTransportadora:int64:42:57 TipoVeiculo:string:57:61 ModoTransporte:string:61:64 NomeTransportadora:string:64:99

gerador-layouts arquivoDePedido abatimentosEncargos TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 QuanlificadorAbatimentoEncargo:string:36:39 TipoAcordoAbatimentoEncargo:string:39:42 TipoRazaoAbatimentoEncargo:string:42:45 PercentualAbatimentoEncargo:float32:45:50:2 ValorAbatimentoEncargo:float64:50:67:2 QuantidadeIndividualAbatimentoEncargo:float64:67:84:2 TaxaAbatimento:float32:84:89:2

gerador-layouts arquivoDePedido dadosItem TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 TipoCodigoItem:string:54:57 QuantidadePedida:float64:57:69:3 QuantidadeGratuita:float64:69:81:3 UnidadeMedida:string:81:84 QuantidadeUnidadesConsumo:int32:84:91 ValorTotalLiquidoItem:float64:91:108:2 ValorTotalBrutoItem:float64:108:125:2 PrecoUnitarioBruto:float64:125:142:2 PrecoUnitarioLiquido:float64:142:159:2 PrecoUnitarioCalculoLiquido:float64:159:176:2 PrecoUnitarioCalculoBruto:float64:176:193:2 NumeroEmbalagens:int32:193:200 NumeroEmbalagensInternas:int32:200:207 NumeroTabelaPreco:string:207:227 DescricaoItem:string:227:262 CodigoProdutoFornecedor:string:262:272 CodigoProdutoComprador:string:272:282 CodigoCor:string:282:299 CodigoTamanho:string:299:316 DataEntrega:int32:316:324 DataInicioEntrega:int32:324:332 DataFimEntrega:int32:332:340 CodigoComplementarItem:int64:340:353 CodigoProjetoPharmalink:int32:353:360 DescontoProjetoPharmalink:float32:360:364:2 CodigoItemComercial:int32:365:369 EmbalagemProduto:string:369:371 DescontoNegociado:float32:371:375:2

gerador-layouts arquivoDePedido ObservacaoItem TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 Observacao1:string:54:124 Observacao2:string:124:194 Observacao3:string:194:264 Observacao4:string:264:334 Observacao5:string:334:404 Observacao6:string:404:474 Observacao7:string:474:544 Observacao8:string:544:614 Observacao9:string:614:684 Observacao10:string:684:754 Observacao11:string:754:824 Observacao12:string:824:894 Observacao13:string:894:964 Observacao14:string:964:1034 Observacao15:string:1034:1104

gerador-layouts arquivoDePedido crossdocking TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 NrSequencialParcela:int32:54:57 QuantidadeLoja:float64:57:69:3 EanOuDunsLocalEntrega:int64:69:82 TipoCodigoLocalEntrega:string:82:85

gerador-layouts arquivoDePedido impostos TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 AliquotaIPI:float32:54:59:2 ValorIPI:float64:59:76:2 ValorICMS:float64:76:93:2

gerador-layouts arquivoDePedido EncargosAbatimentos TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 NumeroSequencialLinha:int32:36:40 CodigoItem:int64:40:54 QualificadorAbatimentoEncargo:string:54:57 TipoAcordoAbatimentoEncargo:string:57:60 TipoRazaoAbatimentoEncargo:string:60:63 PercentualAbatimentoEncargo:float32:63:68:2 ValorAbatimentoEncargo:float64:68:85:2 QuantidadeIndividualAbatimentoEncargo:float64:85:102:2 TaxaAbatimento:float32:102:107:2

gerador-layouts arquivoDePedido sumario TipoRegistro:string:0:2 TipoDocumento:int32:2:3 NumeroPedido:string:3:23 EanComprador:int64:23:36 ValorTotalPedido:float64:36:53:2 ValorTotalEncargos:float64:53:70:2 ValorTotalMercadorias:float64:70:87:2 ValorTotalDescontoComercial:float64:87:104:2 ValorDespesasAcessorias:float64:104:121:2 ValorEncargosFinanceiros:float64:121:138:2 ValorFrete:float64:138:155:2 ValorTotalIPI:float64:155:172:2 ValorTotalBonificacao:float64:172:189:2 NumeroTotalLinhasItem:int32:189:194


## Retorno de Pedido
gerador-layouts retornoDePedido cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjFornecedor:int64:14:28 IdentificacaoInicialPedidoFornecedor:string:28:40 IdentificacaoFinalPedidoFornecedor:string:40:52 PrevisaoDataFaturamento:int32:52:60 PrevisaoDataEntregaMercadoria:int32:60:68 StatusProcessamentoPedido:int32:68:69 CodigoIternoLoja:string:69:73 IdentificacaoPedidoFornecedor:string:73:85 TipoAcaoPedido:string:85:86 CnpjCLiente:int64:86:100

gerador-layouts retornoDePedido detalhe CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IndentificacaoProduto:string:3:17 QuantidadeAtendida:float64:17:28:3 QuantidadeRecusada:float64:28:39:3 MotivoRejeicao:string:39:69 NumeroItemRequisicaoCompra:int32:69:74 EmbalagemProduto:string:74:76

gerador-layouts retornoDePedido rodape CodigoRegistro:string:0:2 TotalItensAtendidos:int32:2:8 TotalQuantidadeAtendida:float64:8:19:3 TotaisItensRecusados:int32:19:25 TotalQuantidadeRecusado:float64:25:36:3


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjEmissorNotaFiscal:string:14:28 NumeroNotaFiscal:int32:28:34 SerieNotaFiscal:string:34:37 DataEmissaoNotaFiscal:int32:37:45 CodigoFiscalOperacao:int32:45:49 CodigoValorFiscal:int32:49:50 ValorBrutoNotaFiscal:float64:50:65:2 ValorContabilNotaFiscal:float64:65:80:2 ValorTributadoNotaFiscal:float64:80:95:2 ValorOutrasNotaFiscal:float64:95:110:2 BaseICMSRetido:float64:110:125:2 ValorICMSRetido:float64:125:140:2 BaseICMSEntrada:float64:140:155:2 ValorICMSEntrada:float64:155:170:2 BaseIPI:float64:170:185:2 ValorIPI:float64:185:200:2 CGCTransportadora:string:200:214 TipoFrete:string:214:215 ValorFrete:float64:215:230:2 PercentualDescontoComercial:float32:230:236:4 PercentualDescontoRepasseICMS:float32:236:242:4 TotalItens:int64:242:255 TotalUnidades:float64:255:266:3 TotalNotasPedido:int32:266:272 ValorDescontoICMSNormal:float64:272:287:2 ValorDescontoPIS:float64:287:302:2 ValorDescontoCofins:float64:302:317:2 TipoNotaFiscalEntrada:int32:317:319 ChaveAcessoNotaFiscalEletronica:string:319:363 CodigoInternoLoja:string:363:367 NumeroCorrelativoPedidoFornecedor:int32:367:371 CodigoEanDistribuidor:int64:371:384 CodigoEanPDV:int64:384:397 HoraMinutoNota:int32:397:401 CnpjCliente:int64:401:415

gerador-layouts arquivoDeNotaFiscal produtos CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IdentificacaoProduto:string:3:17 EmbalagemProduto:string:17:19 QuantidadeEmbalagens:float64:19:30:3 CodigoFiscalOperacao:int32:30:34 SituacaoTributaria:string:34:36 PrecoProduto:float64:36:51:5 PercentualDescontoItem:float32:51:57:4 ValorDescontoItem:float64:57:72:5 PercentualDescontoRepasse:float32:72:78:2 ValorDescontoRepasse:float64:78:93:5 PercentualDescontoComercial:float32:93:99:5 ValorDescontoComercial:float64:99:114:5 ValorDespesaAcessorias:float64:114:129:5 ValorDespesaEmbalagem:float64:129:144:5

gerador-layouts arquivoDeNotaFiscal loteFabricacaoProdutos CodigoRegistro:string:0:2 LoteFabricacao:string:2:22 QuantidadeProduto:float64:22:33:3 DataValidade:int32:33:41 DataFabricacao:int32:41:49

gerador-layouts arquivoDeNotaFiscal titulos CodigoRegistro:string:0:2 NumeroTitulo:string:2:12 LocalPagamento:string:12:16 DataVencimento:int32:16:24 ValorBruto:float64:24:39:2 ValorLiquido:float64:39:54:2 DataAntecipacao:int32:54:62 DescontoAntecipado:float32:62:66:2 DescontoLimite:float32:66:70:2
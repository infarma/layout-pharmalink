package arquivoDeNotaFiscal

import (
	"bufio"
	"os"
)

type ArquivoDeNotaFiscal struct {
	Cabecalho             Cabecalho             `json:"Cabecalho"`
	DadosDaNotaFiscal     DadosDaNotaFiscal     `json:"TotaisDaNotaFiscal"`
	TotaisDaNotaFiscal    TotaisDaNotaFiscal    `json:"TotaisDaNotaFiscal"`
	DadosIcmsDaNotaFiscal DadosIcmsDaNotaFiscal `json:"DadosIcmsDaNotaFiscal"`
	ItensDaNotaFiscal     []ItensDaNotaFiscal   `json:"ItensDaNotaFiscal"`
	FimDeArquivo          FimDeArquivo          `json:"FimDeArquivo"`
}

func GetArquivoDeEnvio(fileHandle *os.File) ArquivoDeNotaFiscal {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDeNotaFiscal{}
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			arquivo.Cabecalho = GetCabecalho(runes)
		} else if identificador == "02" {
			arquivo.DadosDaNotaFiscal = GetDadosDaNotaFiscal(runes)
		} else if identificador == "03" {
			arquivo.TotaisDaNotaFiscal = GetTotaisDaNotaFiscal(runes)
		} else if identificador == "04" {
			arquivo.DadosIcmsDaNotaFiscal = GetDadosIcmsDaNotaFiscal(runes)
		} else if identificador == "05" {
			arquivo.ItensDaNotaFiscal = append(arquivo.ItensDaNotaFiscal, GetItensDaNotaFiscal(runes))
		} else if identificador == "04" {
			arquivo.FimDeArquivo = GetFimDeArquivo(runes)
		}
	}
	return arquivo
}

package arquivoDeNotaFiscal

import (
	"strconv"
	"strings"
)

type Cabecalho struct {
	TipoRegistro           int64  `json:"TipoRegistro"`
	DataGeracaoArquivo     int64  `json:"DataGeracaoArquivo"`
	HoraGeracaoArquivo     int64  `json:"HoraGeracaoArquivo"`
	CnpjOperador           int64  `json:"CnpjOperador"`
	CodigoProjeto          string `json:"CodigoProjeto"`
	NumeroPedidoPharmaLink int64  `json:"NumeroPedidoPharmaLink"`
	NumeroPedidoCliente    string `json:"NumeroPedidoCliente"`
	Livre                  string `json:"Livre"`
}

func GetCabecalho(runes []rune) Cabecalho {
	cabecalho := Cabecalho{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	cabecalho.TipoRegistro = tipoRegistro

	dataGeracaoArquivo, _ := strconv.ParseInt(string(runes[1:9]), 10, 64)
	cabecalho.DataGeracaoArquivo = dataGeracaoArquivo

	horaGeracaoArquivo, _ := strconv.ParseInt(string(runes[9:15]), 10, 64)
	cabecalho.HoraGeracaoArquivo = horaGeracaoArquivo

	cnpjOperador, _ := strconv.ParseInt(string(runes[15:29]), 10, 64)
	cabecalho.CnpjOperador = cnpjOperador

	cabecalho.CodigoProjeto = strings.TrimSpace(string(runes[29:30]))

	numeroPedidoPharmaLink, _ := strconv.ParseInt(string(runes[30:37]), 10, 64)
	cabecalho.NumeroPedidoPharmaLink = numeroPedidoPharmaLink

	cabecalho.NumeroPedidoCliente = strings.TrimSpace(string(runes[37:52]))

	cabecalho.Livre = strings.TrimSpace(string(runes[52:80]))

	return cabecalho
}

package arquivoDeNotaFiscal

import (
	"strconv"
	"strings"
)

type DadosIcmsDaNotaFiscal struct {
	TipoRegistro                      int64   `json:"TipoRegistro"`
	BaseCalculoIcms                   float64 `json:"BaseCalculoIcms"`
	ValorIcms                         float64 `json:"ValorIcms"`
	BaseCalculoSubstituicaoTributaria float64 `json:"BaseCalculoSubstituicaoTributaria"`
	ValorIcmsSubstituicaoTributaria   float64 `json:"ValorIcmsSubstituicaoTributaria"`
	Livre                             string  `json:"Livre"`
}

func GetDadosIcmsDaNotaFiscal(runes []rune) DadosIcmsDaNotaFiscal {
	dadosIcmsDaNotaFiscal := DadosIcmsDaNotaFiscal{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	dadosIcmsDaNotaFiscal.TipoRegistro = tipoRegistro

	baseCalculoIcms, _ := strconv.ParseFloat(string(runes[1:7])+"."+string(runes[7:9]), 64)
	dadosIcmsDaNotaFiscal.BaseCalculoIcms = baseCalculoIcms

	valorIcms, _ := strconv.ParseFloat(string(runes[9:15])+"."+string(runes[15:17]), 64)
	dadosIcmsDaNotaFiscal.ValorIcms = valorIcms

	baseCalculoSubstituicaoTributaria, _ := strconv.ParseFloat(string(runes[17:23])+"."+string(runes[23:25]), 64)
	dadosIcmsDaNotaFiscal.BaseCalculoSubstituicaoTributaria = baseCalculoSubstituicaoTributaria

	valorIcmsSubstituicaoTributaria, _ := strconv.ParseFloat(string(runes[25:31])+"."+string(runes[31:33]), 64)
	dadosIcmsDaNotaFiscal.ValorIcmsSubstituicaoTributaria = valorIcmsSubstituicaoTributaria

	dadosIcmsDaNotaFiscal.Livre = strings.TrimSpace(string(runes[33:80]))

	return dadosIcmsDaNotaFiscal
}

package arquivoDeNotaFiscal

import (
	"strconv"
	"strings"
)

type ItensDaNotaFiscal struct {
	TipoRegistro           int64   `json:"TipoRegistro"`
	CodigoEAN              string  `json:"CodigoEAN"`
	CodigoProdutoOperador  string  `json:"CodigoProdutoOperador"`
	Quantidade             int64   `json:"Quantidade"`
	Unidade                string  `json:"Unidade"`
	PrecoFabrica           float64 `json:"PrecoFabrica"`
	DescontoComercial      float64 `json:"DescontoComercial"`
	ValorDescontoComercial float64 `json:"ValorDescontoComercial"`
	ValorRepasse           float64 `json:"ValorRepasse"`
	Repasse                float64 `json:"Repasse"`
	ValorUnitario          float64 `json:"ValorUnitario"`
	Fracionamento          int64   `json:"Fracionamento"`
	Livre                  string  `json:"Livre"`
}

func GetItensDaNotaFiscal(runes []rune) ItensDaNotaFiscal {
	itensDaNotaFiscal := ItensDaNotaFiscal{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	itensDaNotaFiscal.TipoRegistro = tipoRegistro

	itensDaNotaFiscal.CodigoEAN = strings.TrimSpace(string(runes[1:14]))

	itensDaNotaFiscal.CodigoProdutoOperador = strings.TrimSpace(string(runes[14:21]))

	quantidade, _ := strconv.ParseInt(string(runes[21:25]), 10, 64)
	itensDaNotaFiscal.Quantidade = quantidade

	itensDaNotaFiscal.Unidade = strings.TrimSpace(string(runes[25:28]))

	precoFabrica, _ := strconv.ParseFloat(string(runes[28:34])+"."+string(runes[34:36]), 64)
	itensDaNotaFiscal.PrecoFabrica = precoFabrica

	descontoComercial, _ := strconv.ParseFloat(string(runes[36:38])+"."+string(runes[38:40]), 64)
	itensDaNotaFiscal.DescontoComercial = descontoComercial

	valorDescontoComercial, _ := strconv.ParseFloat(string(runes[40:46])+"."+string(runes[46:48]), 64)
	itensDaNotaFiscal.ValorDescontoComercial = valorDescontoComercial

	valorRepasse, _ := strconv.ParseFloat(string(runes[48:54])+"."+string(runes[54:56]), 64)
	itensDaNotaFiscal.ValorRepasse = valorRepasse

	repasse, _ := strconv.ParseFloat(string(runes[56:58])+"."+string(runes[58:60]), 64)
	itensDaNotaFiscal.Repasse = repasse

	valorUnitario, _ := strconv.ParseFloat(string(runes[60:66])+"."+string(runes[66:68]), 64)
	itensDaNotaFiscal.ValorUnitario = valorUnitario

	fracionamento, _ := strconv.ParseInt(string(runes[68:72]), 10, 64)
	itensDaNotaFiscal.Fracionamento = fracionamento

	itensDaNotaFiscal.Livre = strings.TrimSpace(string(runes[72:80]))

	return itensDaNotaFiscal
}

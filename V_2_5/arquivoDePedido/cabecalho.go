package arquivoDePedido

import (
	"strconv"
	"strings"
)

type Cabecalho struct {
	TipoRegistro      int64  `json:"TipoRegistro"`
	CnpjCliente       string `json:"CnpjCliente"`
	TipoFaturamento   int64  `json:"TipoFaturamento"`
	ApontadorPromocao string `json:"ApontadorPromocao"`
	CodigoControle    string `json:"CodigoControle"`
}

func GetCabecalho(runes []rune) Cabecalho {
	cabecalho := Cabecalho{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	cabecalho.TipoRegistro = tipoRegistro

	cabecalho.CnpjCliente = strings.TrimSpace(string(runes[1:15]))

	tipoFaturamento, _ := strconv.ParseInt(string(runes[15:16]), 10, 64)
	cabecalho.TipoFaturamento = tipoFaturamento

	cabecalho.ApontadorPromocao = strings.TrimSpace(string(runes[16:29]))

	cabecalho.CodigoControle = strings.TrimSpace(string(runes[29:31]))

	return cabecalho
}

package arquivoDePedido

import (
	"strconv"
	"strings"
)

type DadosDoPedido struct {
	TipoRegistro           int64  `json:"TipoRegistro"`
	CodigoProjeto          string `json:"CodigoProjeto"`
	PedidoPharmaLink       string `json:"PedidoPharmaLink"`
	CnpjCentroDistribuicao string `json:"CnpjCentroDistribuicao"`
	SemUtilizacao          string `json:"SemUtilizacao"`
}

func GetDadosDoPedido(runes []rune) DadosDoPedido {
	dadosPedido := DadosDoPedido{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	dadosPedido.TipoRegistro = tipoRegistro

	dadosPedido.CodigoProjeto = strings.TrimSpace(string(runes[1:2]))

	dadosPedido.PedidoPharmaLink = strings.TrimSpace(string(runes[2:9]))

	dadosPedido.CnpjCentroDistribuicao = strings.TrimSpace(string(runes[9:23]))

	dadosPedido.SemUtilizacao = strings.TrimSpace(string(runes[23:31]))

	return dadosPedido
}

package arquivoDePedido

import (
	"strconv"
)

type DataDoPedido struct {
	TipoRegistro  int64 `json:"TipoRegistro"`
	Data          int64 `json:"Data"`
	SemUtilizacao int64 `json:"SemUtilizacao"`
}

func GetDataDoPedido(runes []rune) DataDoPedido {
	dataDoPedido := DataDoPedido{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	dataDoPedido.TipoRegistro = tipoRegistro

	data, _ := strconv.ParseInt(string(runes[1:9]), 10, 64)
	dataDoPedido.Data = data

	semUtilizacao, _ := strconv.ParseInt(string(runes[9:31]), 10, 64)
	dataDoPedido.SemUtilizacao = semUtilizacao

	return dataDoPedido
}

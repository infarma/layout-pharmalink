package arquivoDePedido

import (
	"strconv"
)

type Rodape struct {
	TipoRegistro    int64 `json:"TipoRegistro"`
	QuantidadeItens int64 `json:"QuantidadeItens"`
	SemUtilizacao   int64 `json:"SemUtilizacao"`
}

func GetRodape(runes []rune) Rodape {
	rodape := Rodape{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	rodape.TipoRegistro = tipoRegistro

	quantidadeItens, _ := strconv.ParseInt(string(runes[1:3]), 10, 64)
	rodape.QuantidadeItens = quantidadeItens

	semUtilizacao, _ := strconv.ParseInt(string(runes[3:31]), 10, 64)
	rodape.SemUtilizacao = semUtilizacao

	return rodape
}

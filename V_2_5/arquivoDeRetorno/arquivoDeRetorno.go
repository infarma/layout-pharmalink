package arquivoDeRetorno

import (
	"bufio"
	"os"
)

type ArquivoDeRetorno struct {
	Cabecalho           Cabecalho           `json:"Cabecalho"`
	DadosComplementares DadosComplementares `json:"DadosComplementares"`
	ItensDoPedido       []ItensDoPedido     `json:"ItensDoPedido"`
	FimDeArquivo        FimDeArquivo        `json:"FimDeArquivo"`
}

func GetArquivoDeRetorno(fileHandle *os.File) ArquivoDeRetorno {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDeRetorno{}
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			arquivo.Cabecalho = GetCabecalho(runes)
		} else if identificador == "02" {
			arquivo.DadosComplementares = GetDadosComplementares(runes)
		} else if identificador == "03" {
			arquivo.ItensDoPedido = append(arquivo.ItensDoPedido, GetItensDoPedido(runes))
		} else if identificador == "04" {
			arquivo.FimDeArquivo = GetFimDeArquivo(runes)
		}
	}
	return arquivo
}

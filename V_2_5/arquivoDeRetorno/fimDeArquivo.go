package arquivoDeRetorno

import (
	"strconv"
	"strings"
)

type FimDeArquivo struct {
	TipoRegistro                   int64  `json:"TipoRegistro"`
	QuantidadeUnidadesAtendidas    int64  `json:"QuantidadeUnidadesAtendidas"`
	QuantidadeUnidadesNaoAtendidas int64  `json:"QuantidadeUnidadesNaoAtendidas"`
	QuantidadeItensArquivo         int64  `json:"QuantidadeItensArquivo"`
	SemUtilizacao                  string `json:"SemUtilizacao"`
}

func GetFimDeArquivo(runes []rune) FimDeArquivo {
	fimDeArquivo := FimDeArquivo{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	fimDeArquivo.TipoRegistro = tipoRegistro

	quantidadeUnidadesAtendidas, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	fimDeArquivo.QuantidadeUnidadesAtendidas = quantidadeUnidadesAtendidas

	quantidadeUnidadesNaoAtendidas, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	fimDeArquivo.QuantidadeUnidadesNaoAtendidas = quantidadeUnidadesNaoAtendidas

	quantidadeItensArquivo, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	fimDeArquivo.QuantidadeItensArquivo = quantidadeItensArquivo

	fimDeArquivo.SemUtilizacao = strings.TrimSpace(string(runes[23:25]))

	return fimDeArquivo
}

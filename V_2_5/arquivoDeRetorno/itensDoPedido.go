package arquivoDeRetorno

import (
	"strconv"
)

type ItensDoPedido struct {
	TipoRegistro          int64 `json:"TipoRegistro"`
	EANProduto            int64 `json:"EANProduto"`
	QuantidadeAtendida    int64 `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int64 `json:"QuantidadeNaoAtendida"`
	Motivo                int64 `json:"Motivo"`
	Retorno               int64 `json:"Retorno"`
}

func GetItensDoPedido(runes []rune) ItensDoPedido {
	itensDoPedido := ItensDoPedido{}

	tipoRegistro, _ := strconv.ParseInt(string(runes[0:1]), 10, 64)
	itensDoPedido.TipoRegistro = tipoRegistro

	eanProduto, _ := strconv.ParseInt(string(runes[1:14]), 10, 64)
	itensDoPedido.EANProduto = eanProduto

	quantidadeAtendida, _ := strconv.ParseInt(string(runes[14:18]), 10, 64)
	itensDoPedido.QuantidadeAtendida = quantidadeAtendida

	quantidadeNaoAtendida, _ := strconv.ParseInt(string(runes[18:22]), 10, 64)
	itensDoPedido.QuantidadeNaoAtendida = quantidadeNaoAtendida

	motivo, _ := strconv.ParseInt(string(runes[22:24]), 10, 64)
	itensDoPedido.Motivo = motivo

	retorno, _ := strconv.ParseInt(string(runes[24:25]), 10, 64)
	itensDoPedido.Retorno = retorno

	return itensDoPedido
}

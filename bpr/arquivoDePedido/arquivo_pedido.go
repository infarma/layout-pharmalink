package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho           Cabecalho           `json:"Cabecalho"`
	Observacao          Observacao          `json:"Observacao"`
	Pagamento           Pagamento           `json:"Pagamento"`
	Embarque            Embarque            `json:"Embarque"`
	AbatimentosEncargos AbatimentosEncargos `json:"AbatimentosEncargos"`
	DadosItem           []DadosItem         `json:"DadosItem"`
	ObservacaoItem      []ObservacaoItem    `json:"ObservacaoItem"`
	Crossdocking        Crossdocking        `json:"Crossdocking"`
	Impostos            Impostos            `json:"Impostos"`
	EncargosAbatimentos EncargosAbatimentos `json:"EncargosAbatimentos"`
	Sumario             Sumario             `json:"Sumario"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var indexItem, indexObsItem int32
		if identificador == "01" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.Observacao.ComposeStruct(string(runes))
		} else if identificador == "03" {
			err = arquivo.Pagamento.ComposeStruct(string(runes))
		} else if identificador == "04" {
			err = arquivo.Embarque.ComposeStruct(string(runes))
		} else if identificador == "05" {
			err = arquivo.AbatimentosEncargos.ComposeStruct(string(runes))
		} else if identificador == "06" {
			err = arquivo.DadosItem[indexItem].ComposeStruct(string(runes))
			indexItem++
		} else if identificador == "07" {
			err = arquivo.ObservacaoItem[indexObsItem].ComposeStruct(string(runes))
			indexObsItem++
		} else if identificador == "08" {
			err = arquivo.Crossdocking.ComposeStruct(string(runes))
		} else if identificador == "09" {
			err = arquivo.Impostos.ComposeStruct(string(runes))
		} else if identificador == "10" {
			err = arquivo.EncargosAbatimentos.ComposeStruct(string(runes))
		} else if identificador == "99" {
			err = arquivo.Sumario.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}

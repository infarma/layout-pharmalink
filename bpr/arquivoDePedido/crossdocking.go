package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Crossdocking struct {
	TipoRegistro           string 	`json:"TipoRegistro"`
	TipoDocumento          int32  	`json:"TipoDocumento"`
	NumeroPedido           string 	`json:"NumeroPedido"`
	EanComprador           int64  	`json:"EanComprador"`
	NumeroSequencialLinha  int32  	`json:"NumeroSequencialLinha"`
	CodigoItem             int64  	`json:"CodigoItem"`
	NrSequencialParcela    int32  	`json:"NrSequencialParcela"`
	QuantidadeLoja         float64	`json:"QuantidadeLoja"`
	EanOuDunsLocalEntrega  int64  	`json:"EanOuDunsLocalEntrega"`
	TipoCodigoLocalEntrega string 	`json:"TipoCodigoLocalEntrega"`
}

func (c *Crossdocking) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCrossdocking

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanComprador, "EanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroSequencialLinha, "NumeroSequencialLinha")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoItem, "CodigoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NrSequencialParcela, "NrSequencialParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.QuantidadeLoja, "QuantidadeLoja")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanOuDunsLocalEntrega, "EanOuDunsLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoCodigoLocalEntrega, "TipoCodigoLocalEntrega")
	if err != nil {
		return err
	}


	return err
}

var PosicoesCrossdocking = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"TipoDocumento":                      {2, 3, 0},
	"NumeroPedido":                      {3, 23, 0},
	"EanComprador":                      {23, 36, 0},
	"NumeroSequencialLinha":                      {36, 40, 0},
	"CodigoItem":                      {40, 54, 0},
	"NrSequencialParcela":                      {54, 57, 0},
	"QuantidadeLoja":                      {57, 69, 3},
	"EanOuDunsLocalEntrega":                      {69, 82, 0},
	"TipoCodigoLocalEntrega":                      {82, 85, 0},
}
package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosItem struct {
	TipoRegistro                string  `json:"TipoRegistro"`
	TipoDocumento               int32   `json:"TipoDocumento"`
	NumeroPedido                string  `json:"NumeroPedido"`
	EanComprador                int64   `json:"EanComprador"`
	NumeroSequencialLinha       int32   `json:"NumeroSequencialLinha"`
	CodigoItem                  int64   `json:"CodigoItem"`
	TipoCodigoItem              string  `json:"TipoCodigoItem"`
	QuantidadePedida            float64 `json:"QuantidadePedida"`
	QuantidadeGratuita          float64 `json:"QuantidadeGratuita"`
	UnidadeMedida               string  `json:"UnidadeMedida"`
	QuantidadeUnidadesConsumo   int32   `json:"QuantidadeUnidadesConsumo"`
	ValorTotalLiquidoItem       float64 `json:"ValorTotalLiquidoItem"`
	ValorTotalBrutoItem         float64 `json:"ValorTotalBrutoItem"`
	PrecoUnitarioBruto          float64 `json:"PrecoUnitarioBruto"`
	PrecoUnitarioLiquido        float64 `json:"PrecoUnitarioLiquido"`
	PrecoUnitarioCalculoLiquido float64 `json:"PrecoUnitarioCalculoLiquido"`
	PrecoUnitarioCalculoBruto   float64 `json:"PrecoUnitarioCalculoBruto"`
	NumeroEmbalagens            int32   `json:"NumeroEmbalagens"`
	NumeroEmbalagensInternas    int32   `json:"NumeroEmbalagensInternas"`
	NumeroTabelaPreco           string  `json:"NumeroTabelaPreco"`
	DescricaoItem               string  `json:"DescricaoItem"`
	CodigoProdutoFornecedor     string  `json:"CodigoProdutoFornecedor"`
	CodigoProdutoComprador      string  `json:"CodigoProdutoComprador"`
	CodigoCor                   string  `json:"CodigoCor"`
	CodigoTamanho               string  `json:"CodigoTamanho"`
	DataEntrega                 int32   `json:"DataEntrega"`
	DataInicioEntrega           int32   `json:"DataInicioEntrega"`
	DataFimEntrega              int32   `json:"DataFimEntrega"`
	CodigoComplementarItem      int64   `json:"CodigoComplementarItem"`
	CodigoProjetoPharmalink     int32   `json:"CodigoProjetoPharmalink"`
	DescontoProjetoPharmalink   float32 `json:"DescontoProjetoPharmalink"`
	CodigoItemComercial         int32   `json:"CodigoItemComercial"`
	EmbalagemProduto            string  `json:"EmbalagemProduto"`
	DescontoNegociado           float32 `json:"DescontoNegociado"`
}

func (d *DadosItem) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosItem

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.EanComprador, "EanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroSequencialLinha, "NumeroSequencialLinha")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoItem, "CodigoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.TipoCodigoItem, "TipoCodigoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadePedida, "QuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadeGratuita, "QuantidadeGratuita")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.UnidadeMedida, "UnidadeMedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadeUnidadesConsumo, "QuantidadeUnidadesConsumo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorTotalLiquidoItem, "ValorTotalLiquidoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorTotalBrutoItem, "ValorTotalBrutoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PrecoUnitarioBruto, "PrecoUnitarioBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PrecoUnitarioLiquido, "PrecoUnitarioLiquido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PrecoUnitarioCalculoLiquido, "PrecoUnitarioCalculoLiquido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PrecoUnitarioCalculoBruto, "PrecoUnitarioCalculoBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroEmbalagens, "NumeroEmbalagens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroEmbalagensInternas, "NumeroEmbalagensInternas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroTabelaPreco, "NumeroTabelaPreco")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescricaoItem, "DescricaoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProdutoFornecedor, "CodigoProdutoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProdutoComprador, "CodigoProdutoComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoCor, "CodigoCor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoTamanho, "CodigoTamanho")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataEntrega, "DataEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataInicioEntrega, "DataInicioEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataFimEntrega, "DataFimEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoComplementarItem, "CodigoComplementarItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProjetoPharmalink, "CodigoProjetoPharmalink")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoProjetoPharmalink, "DescontoProjetoPharmalink")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoItemComercial, "CodigoItemComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.EmbalagemProduto, "EmbalagemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoNegociado, "DescontoNegociado")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosItem = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                {0, 2, 0},
	"TipoDocumento":               {2, 3, 0},
	"NumeroPedido":                {3, 23, 0},
	"EanComprador":                {23, 36, 0},
	"NumeroSequencialLinha":       {36, 40, 0},
	"CodigoItem":                  {40, 54, 0},
	"TipoCodigoItem":              {54, 57, 0},
	"QuantidadePedida":            {57, 69, 3},
	"QuantidadeGratuita":          {69, 81, 3},
	"UnidadeMedida":               {81, 84, 0},
	"QuantidadeUnidadesConsumo":   {84, 91, 0},
	"ValorTotalLiquidoItem":       {91, 108, 2},
	"ValorTotalBrutoItem":         {108, 125, 2},
	"PrecoUnitarioBruto":          {125, 142, 2},
	"PrecoUnitarioLiquido":        {142, 159, 2},
	"PrecoUnitarioCalculoLiquido": {159, 176, 2},
	"PrecoUnitarioCalculoBruto":   {176, 193, 2},
	"NumeroEmbalagens":            {193, 200, 0},
	"NumeroEmbalagensInternas":    {200, 207, 0},
	"NumeroTabelaPreco":           {207, 227, 0},
	"DescricaoItem":               {227, 262, 0},
	"CodigoProdutoFornecedor":     {262, 272, 0},
	"CodigoProdutoComprador":      {272, 282, 0},
	"CodigoCor":                   {282, 299, 0},
	"CodigoTamanho":               {299, 316, 0},
	"DataEntrega":                 {316, 324, 0},
	"DataInicioEntrega":           {324, 332, 0},
	"DataFimEntrega":              {332, 340, 0},
	"CodigoComplementarItem":      {340, 353, 0},
	"CodigoProjetoPharmalink":     {353, 360, 0},
	"DescontoProjetoPharmalink":   {360, 364, 2},
	"CodigoItemComercial":         {364, 369, 0},
	"EmbalagemProduto":            {369, 371, 0},
	"DescontoNegociado":           {371, 375, 2},
}

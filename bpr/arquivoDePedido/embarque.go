package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Embarque struct {
	TipoRegistro             string `json:"TipoRegistro"`
	TipoDOcumento            int32  `json:"TipoDOcumento"`
	NumeroPedido             string `json:"NumeroPedido"`
	EamComprador             int64  `json:"EamComprador"`
	TipoCondicaoEntrega      string `json:"TipoCondicaoEntrega"`
	TipoCodigoTransportadora int32  `json:"TipoCodigoTransportadora"`
	EanCGCTransportadora     int64  `json:"EanCGCTransportadora"`
	TipoVeiculo              string `json:"TipoVeiculo"`
	ModoTransporte           string `json:"ModoTransporte"`
	NomeTransportadora       string `json:"NomeTransportadora"`
}

func (e *Embarque) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesEmbarque

	err = posicaoParaValor.ReturnByType(&e.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoDOcumento, "TipoDOcumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.EamComprador, "EamComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoCondicaoEntrega, "TipoCondicaoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoCodigoTransportadora, "TipoCodigoTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.EanCGCTransportadora, "EanCGCTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoVeiculo, "TipoVeiculo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.ModoTransporte, "ModoTransporte")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.NomeTransportadora, "NomeTransportadora")
	if err != nil {
		return err
	}

	return err
}

var PosicoesEmbarque = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":             {0, 2, 0},
	"TipoDOcumento":            {2, 3, 0},
	"NumeroPedido":             {3, 23, 0},
	"EamComprador":             {23, 36, 0},
	"TipoCondicaoEntrega":      {36, 39, 0},
	"TipoCodigoTransportadora": {39, 42, 0},
	"EanCGCTransportadora":     {42, 57, 0},
	"TipoVeiculo":              {57, 61, 0},
	"ModoTransporte":           {61, 64, 0},
	"NomeTransportadora":       {64, 99, 0},
}

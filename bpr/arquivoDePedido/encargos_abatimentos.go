package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type EncargosAbatimentos struct {
	TipoRegistro                          string  `json:"TipoRegistro"`
	TipoDocumento                         int32   `json:"TipoDocumento"`
	NumeroPedido                          string  `json:"NumeroPedido"`
	EanComprador                          int64   `json:"EanComprador"`
	NumeroSequencialLinha                 int32   `json:"NumeroSequencialLinha"`
	CodigoItem                            int64   `json:"CodigoItem"`
	QualificadorAbatimentoEncargo         string  `json:"QualificadorAbatimentoEncargo"`
	TipoAcordoAbatimentoEncargo           string  `json:"TipoAcordoAbatimentoEncargo"`
	TipoRazaoAbatimentoEncargo            string  `json:"TipoRazaoAbatimentoEncargo"`
	PercentualAbatimentoEncargo           float32 `json:"PercentualAbatimentoEncargo"`
	ValorAbatimentoEncargo                float64 `json:"ValorAbatimentoEncargo"`
	QuantidadeIndividualAbatimentoEncargo float64 `json:"QuantidadeIndividualAbatimentoEncargo"`
	TaxaAbatimento                        float32 `json:"TaxaAbatimento"`
}

func (e *EncargosAbatimentos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesEncargosAbatimentos

	err = posicaoParaValor.ReturnByType(&e.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.EanComprador, "EanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.NumeroSequencialLinha, "NumeroSequencialLinha")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.CodigoItem, "CodigoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.QualificadorAbatimentoEncargo, "QualificadorAbatimentoEncargo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoAcordoAbatimentoEncargo, "TipoAcordoAbatimentoEncargo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoRazaoAbatimentoEncargo, "TipoRazaoAbatimentoEncargo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.PercentualAbatimentoEncargo, "PercentualAbatimentoEncargo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.ValorAbatimentoEncargo, "ValorAbatimentoEncargo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.QuantidadeIndividualAbatimentoEncargo, "QuantidadeIndividualAbatimentoEncargo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TaxaAbatimento, "TaxaAbatimento")
	if err != nil {
		return err
	}

	return err
}

var PosicoesEncargosAbatimentos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                          {0, 2, 0},
	"TipoDocumento":                         {2, 3, 0},
	"NumeroPedido":                          {3, 23, 0},
	"EanComprador":                          {23, 36, 0},
	"NumeroSequencialLinha":                 {36, 40, 0},
	"CodigoItem":                            {40, 54, 0},
	"QualificadorAbatimentoEncargo":         {54, 57, 0},
	"TipoAcordoAbatimentoEncargo":           {57, 60, 0},
	"TipoRazaoAbatimentoEncargo":            {60, 63, 0},
	"PercentualAbatimentoEncargo":           {63, 68, 2},
	"ValorAbatimentoEncargo":                {68, 85, 2},
	"QuantidadeIndividualAbatimentoEncargo": {85, 102, 2},
	"TaxaAbatimento":                        {102, 107, 2},
}

package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Observacao struct {
	TipoRegistro  string `json:"TipoRegistro"`
	TipoDocumento int32  `json:"TipoDocumento"`
	NumeroPedido  string `json:"NumeroPedido"`
	EanComprador  int64  `json:"EanComprador"`
	Observacao1   string `json:"Observacao1"`
	Observacao2   string `json:"Observacao2"`
	Observacao3   string `json:"Observacao3"`
	Observacao4   string `json:"Observacao4"`
	Observacao5   string `json:"Observacao5"`
	Observacao6   string `json:"Observacao6"`
	Observacao7   string `json:"Observacao7"`
	Observacao8   string `json:"Observacao8"`
	Observacao9   string `json:"Observacao9"`
	Observacao10  string `json:"Observacao10"`
	Observacao11  string `json:"Observacao11"`
	Observacao12  string `json:"Observacao12"`
	Observacao13  string `json:"Observacao13"`
	Observacao14  string `json:"Observacao14"`
	Observacao15  string `json:"Observacao15"`
}

func (o *Observacao) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesObservacao

	err = posicaoParaValor.ReturnByType(&o.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.EanComprador, "EanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao1, "Observacao1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao2, "Observacao2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao3, "Observacao3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao4, "Observacao4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao5, "Observacao5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao6, "Observacao6")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao7, "Observacao7")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao8, "Observacao8")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao9, "Observacao9")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao10, "Observacao10")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao11, "Observacao11")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao12, "Observacao12")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao13, "Observacao13")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao14, "Observacao14")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&o.Observacao15, "Observacao15")
	if err != nil {
		return err
	}

	return err
}

var PosicoesObservacao = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 2, 0},
	"TipoDocumento": {2, 3, 0},
	"NumeroPedido":  {3, 23, 0},
	"EanComprador":  {23, 36, 0},
	"Observacao1":   {36, 106, 0},
	"Observacao2":   {106, 176, 0},
	"Observacao3":   {176, 246, 0},
	"Observacao4":   {246, 316, 0},
	"Observacao5":   {316, 386, 0},
	"Observacao6":   {386, 456, 0},
	"Observacao7":   {456, 526, 0},
	"Observacao8":   {526, 596, 0},
	"Observacao9":   {596, 666, 0},
	"Observacao10":  {666, 736, 0},
	"Observacao11":  {736, 806, 0},
	"Observacao12":  {806, 876, 0},
	"Observacao13":  {876, 946, 0},
	"Observacao14":  {946, 1016, 0},
	"Observacao15":  {1016, 1086, 0},
}

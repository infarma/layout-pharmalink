package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	CodigoRegistro                       string `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente           string `json:"IdentificacaoPedidoCliente"`
	CnpjFornecedor                       int64  `json:"CnpjFornecedor"`
	IdentificacaoInicialPedidoFornecedor string `json:"IdentificacaoInicialPedidoFornecedor"`
	IdentificacaoFinalPedidoFornecedor   string `json:"IdentificacaoFinalPedidoFornecedor"`
	PrevisaoDataFaturamento              int32  `json:"PrevisaoDataFaturamento"`
	PrevisaoDataEntregaMercadoria        int32  `json:"PrevisaoDataEntregaMercadoria"`
	StatusProcessamentoPedido            int32  `json:"StatusProcessamentoPedido"`
	CodigoIternoLoja                     string `json:"CodigoIternoLoja"`
	IdentificacaoPedidoFornecedor        string `json:"IdentificacaoPedidoFornecedor"`
	TipoAcaoPedido                       string `json:"TipoAcaoPedido"`
	CnpjCLiente                          int64  `json:"CnpjCLiente"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoPedidoCliente, "IdentificacaoPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoInicialPedidoFornecedor, "IdentificacaoInicialPedidoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoFinalPedidoFornecedor, "IdentificacaoFinalPedidoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrevisaoDataFaturamento, "PrevisaoDataFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrevisaoDataEntregaMercadoria, "PrevisaoDataEntregaMercadoria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.StatusProcessamentoPedido, "StatusProcessamentoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoIternoLoja, "CodigoIternoLoja")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoPedidoFornecedor, "IdentificacaoPedidoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoAcaoPedido, "TipoAcaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCLiente, "CnpjCLiente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":                       {0, 2, 0},
	"IdentificacaoPedidoCliente":           {2, 14, 0},
	"CnpjFornecedor":                       {14, 28, 0},
	"IdentificacaoInicialPedidoFornecedor": {28, 40, 0},
	"IdentificacaoFinalPedidoFornecedor":   {40, 52, 0},
	"PrevisaoDataFaturamento":              {52, 60, 0},
	"PrevisaoDataEntregaMercadoria":        {60, 68, 0},
	"StatusProcessamentoPedido":            {68, 69, 0},
	"CodigoIternoLoja":                     {69, 73, 0},
	"IdentificacaoPedidoFornecedor":        {73, 85, 0},
	"TipoAcaoPedido":                       {85, 86, 0},
	"CnpjCLiente":                          {86, 100, 0},
}

package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	CodigoRegistro          string 	`json:"CodigoRegistro"`
	TotalItensAtendidos     int32  	`json:"TotalItensAtendidos"`
	TotalQuantidadeAtendida float64	`json:"TotalQuantidadeAtendida"`
	TotaisItensRecusados    int32  	`json:"TotaisItensRecusados"`
	TotalQuantidadeRecusado float64	`json:"TotalQuantidadeRecusado"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TotalItensAtendidos, "TotalItensAtendidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TotalQuantidadeAtendida, "TotalQuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TotaisItensRecusados, "TotaisItensRecusados")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TotalQuantidadeRecusado, "TotalQuantidadeRecusado")
	if err != nil {
		return err
	}


	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":                      {0, 2, 0},
	"TotalItensAtendidos":                      {2, 8, 0},
	"TotalQuantidadeAtendida":                      {8, 19, 3},
	"TotaisItensRecusados":                      {19, 25, 0},
	"TotalQuantidadeRecusado":                      {25, 36, 3},
}
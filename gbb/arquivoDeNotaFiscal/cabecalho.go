package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	CodigoRegistro                    string  `json:"CodigoRegistro"`
	IdentificacaoPedidoCliente        string  `json:"IdentificacaoPedidoCliente"`
	CnpjEmissorNotaFiscal             string  `json:"CnpjEmissorNotaFiscal"`
	NumeroNotaFiscal                  int32   `json:"NumeroNotaFiscal"`
	SerieNotaFiscal                   string  `json:"SerieNotaFiscal"`
	DataEmissaoNotaFiscal             int32   `json:"DataEmissaoNotaFiscal"`
	CodigoFiscalOperacao              int32   `json:"CodigoFiscalOperacao"`
	CodigoValorFiscal                 int32   `json:"CodigoValorFiscal"`
	ValorBrutoNotaFiscal              float64 `json:"ValorBrutoNotaFiscal"`
	ValorContabilNotaFiscal           float64 `json:"ValorContabilNotaFiscal"`
	ValorTributadoNotaFiscal          float64 `json:"ValorTributadoNotaFiscal"`
	ValorOutrasNotaFiscal             float64 `json:"ValorOutrasNotaFiscal"`
	BaseICMSRetido                    float64 `json:"BaseICMSRetido"`
	ValorICMSRetido                   float64 `json:"ValorICMSRetido"`
	BaseICMSEntrada                   float64 `json:"BaseICMSEntrada"`
	ValorICMSEntrada                  float64 `json:"ValorICMSEntrada"`
	BaseIPI                           float64 `json:"BaseIPI"`
	ValorIPI                          float64 `json:"ValorIPI"`
	CGCTransportadora                 string  `json:"CGCTransportadora"`
	TipoFrete                         string  `json:"TipoFrete"`
	ValorFrete                        float64 `json:"ValorFrete"`
	PercentualDescontoComercial       float32 `json:"PercentualDescontoComercial"`
	PercentualDescontoRepasseICMS     float32 `json:"PercentualDescontoRepasseICMS"`
	TotalItens                        int64   `json:"TotalItens"`
	TotalUnidades                     float64 `json:"TotalUnidades"`
	TotalNotasPedido                  int32   `json:"TotalNotasPedido"`
	ValorDescontoICMSNormal           float64 `json:"ValorDescontoICMSNormal"`
	ValorDescontoPIS                  float64 `json:"ValorDescontoPIS"`
	ValorDescontoCofins               float64 `json:"ValorDescontoCofins"`
	TipoNotaFiscalEntrada             int32   `json:"TipoNotaFiscalEntrada"`
	ChaveAcessoNotaFiscalEletronica   string  `json:"ChaveAcessoNotaFiscalEletronica"`
	CodigoInternoLoja                 string  `json:"CodigoInternoLoja"`
	NumeroCorrelativoPedidoFornecedor int32   `json:"NumeroCorrelativoPedidoFornecedor"`
	CodigoEanDistribuidor             int64   `json:"CodigoEanDistribuidor"`
	CodigoEanPDV                      int64   `json:"CodigoEanPDV"`
	HoraMinutoNota                    int32   `json:"HoraMinutoNota"`
	CnpjCliente                       int64   `json:"CnpjCliente"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.CodigoRegistro, "CodigoRegistro")
	err = posicaoParaValor.ReturnByType(&c.IdentificacaoPedidoCliente, "IdentificacaoPedidoCliente")
	err = posicaoParaValor.ReturnByType(&c.CnpjEmissorNotaFiscal, "CnpjEmissorNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.NumeroNotaFiscal, "NumeroNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.SerieNotaFiscal, "SerieNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.DataEmissaoNotaFiscal, "DataEmissaoNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.CodigoFiscalOperacao, "CodigoFiscalOperacao")
	err = posicaoParaValor.ReturnByType(&c.CodigoValorFiscal, "CodigoValorFiscal")
	err = posicaoParaValor.ReturnByType(&c.ValorBrutoNotaFiscal, "ValorBrutoNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.ValorContabilNotaFiscal, "ValorContabilNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.ValorTributadoNotaFiscal, "ValorTributadoNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.ValorOutrasNotaFiscal, "ValorOutrasNotaFiscal")
	err = posicaoParaValor.ReturnByType(&c.BaseICMSRetido, "BaseICMSRetido")
	err = posicaoParaValor.ReturnByType(&c.ValorICMSRetido, "ValorICMSRetido")
	err = posicaoParaValor.ReturnByType(&c.BaseICMSEntrada, "BaseICMSEntrada")
	err = posicaoParaValor.ReturnByType(&c.ValorICMSEntrada, "ValorICMSEntrada")
	err = posicaoParaValor.ReturnByType(&c.BaseIPI, "BaseIPI")
	err = posicaoParaValor.ReturnByType(&c.ValorIPI, "ValorIPI")
	err = posicaoParaValor.ReturnByType(&c.CGCTransportadora, "CGCTransportadora")
	err = posicaoParaValor.ReturnByType(&c.TipoFrete, "TipoFrete")
	err = posicaoParaValor.ReturnByType(&c.ValorFrete, "ValorFrete")
	err = posicaoParaValor.ReturnByType(&c.PercentualDescontoComercial, "PercentualDescontoComercial")
	err = posicaoParaValor.ReturnByType(&c.PercentualDescontoRepasseICMS, "PercentualDescontoRepasseICMS")
	err = posicaoParaValor.ReturnByType(&c.TotalItens, "TotalItens")
  err = posicaoParaValor.ReturnByType(&c.TotalUnidades, "TotalUnidades")
	err = posicaoParaValor.ReturnByType(&c.TotalNotasPedido, "TotalNotasPedido")
	err = posicaoParaValor.ReturnByType(&c.ValorDescontoICMSNormal, "ValorDescontoICMSNormal")
	err = posicaoParaValor.ReturnByType(&c.ValorDescontoPIS, "ValorDescontoPIS")
	err = posicaoParaValor.ReturnByType(&c.ValorDescontoCofins, "ValorDescontoCofins")
	err = posicaoParaValor.ReturnByType(&c.TipoNotaFiscalEntrada, "TipoNotaFiscalEntrada")
	err = posicaoParaValor.ReturnByType(&c.ChaveAcessoNotaFiscalEletronica, "ChaveAcessoNotaFiscalEletronica")
	err = posicaoParaValor.ReturnByType(&c.CodigoInternoLoja, "CodigoInternoLoja")
	err = posicaoParaValor.ReturnByType(&c.NumeroCorrelativoPedidoFornecedor, "NumeroCorrelativoPedidoFornecedor")
	err = posicaoParaValor.ReturnByType(&c.CodigoEanDistribuidor, "CodigoEanDistribuidor")
	err = posicaoParaValor.ReturnByType(&c.CodigoEanPDV, "CodigoEanPDV")
	err = posicaoParaValor.ReturnByType(&c.HoraMinutoNota, "HoraMinutoNota")
	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":                    {0, 2, 0},
	"IdentificacaoPedidoCliente":        {2, 14, 0},
	"CnpjEmissorNotaFiscal":             {14, 28, 0},
	"NumeroNotaFiscal":                  {28, 34, 0},
	"SerieNotaFiscal":                   {34, 37, 0},
	"DataEmissaoNotaFiscal":             {37, 45, 0},
	"CodigoFiscalOperacao":              {45, 49, 0},
	"CodigoValorFiscal":                 {49, 50, 0},
	"ValorBrutoNotaFiscal":              {50, 65, 2},
	"ValorContabilNotaFiscal":           {65, 80, 2},
	"ValorTributadoNotaFiscal":          {80, 95, 2},
	"ValorOutrasNotaFiscal":             {95, 110, 2},
	"BaseICMSRetido":                    {110, 125, 2},
	"ValorICMSRetido":                   {125, 140, 2},
	"BaseICMSEntrada":                   {140, 155, 2},
	"ValorICMSEntrada":                  {155, 170, 2},
	"BaseIPI":                           {170, 185, 2},
	"ValorIPI":                          {185, 200, 2},
	"CGCTransportadora":                 {200, 214, 0},
	"TipoFrete":                         {214, 215, 0},
	"ValorFrete":                        {215, 230, 2},
	"PercentualDescontoComercial":       {230, 236, 4},
	"PercentualDescontoRepasseICMS":     {236, 242, 4},
	"TotalItens":                        {242, 255, 0},
	"TotalUnidades":                     {255, 266, 3},
	"TotalNotasPedido":                  {266, 272, 0},
	"ValorDescontoICMSNormal":           {272, 287, 2},
	"ValorDescontoPIS":                  {287, 302, 2},
	"ValorDescontoCofins":               {302, 317, 2},
	"TipoNotaFiscalEntrada":             {317, 319, 0},
	"ChaveAcessoNotaFiscalEletronica":   {319, 363, 0},
	"CodigoInternoLoja":                 {363, 367, 0},
	"NumeroCorrelativoPedidoFornecedor": {367, 371, 0},
	"CodigoEanDistribuidor":             {371, 384, 0},
	"CodigoEanPDV":                      {384, 397, 0},
	"HoraMinutoNota":                    {397, 401, 0},
	"CnpjCliente":                       {401, 415, 0},
}

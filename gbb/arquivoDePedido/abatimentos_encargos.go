package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type AbatimentosEncargos struct {
	TipoRegistro                          string  `json:"TipoRegistro"`
	TipoDocumento                         int32   `json:"TipoDocumento"`
	NumeroPedido                          string  `json:"NumeroPedido"`
	EanComprador                          int64   `json:"EanComprador"`
	QuanlificadorAbatimentoEncargo        string  `json:"QuanlificadorAbatimentoEncargo"`
	TipoAcordoAbatimentoEncargo           string  `json:"TipoAcordoAbatimentoEncargo"`
	TipoRazaoAbatimentoEncargo            string  `json:"TipoRazaoAbatimentoEncargo"`
	PercentualAbatimentoEncargo           float32 `json:"PercentualAbatimentoEncargo"`
	ValorAbatimentoEncargo                float64 `json:"ValorAbatimentoEncargo"`
	QuantidadeIndividualAbatimentoEncargo float64 `json:"QuantidadeIndividualAbatimentoEncargo"`
	TaxaAbatimento                        float32 `json:"TaxaAbatimento"`
}

func (a *AbatimentosEncargos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesAbatimentosEncargos

	err = posicaoParaValor.ReturnByType(&a.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&a.TipoDocumento, "TipoDocumento")
	err = posicaoParaValor.ReturnByType(&a.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&a.EanComprador, "EanComprador")
	err = posicaoParaValor.ReturnByType(&a.QuanlificadorAbatimentoEncargo, "QuanlificadorAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&a.TipoAcordoAbatimentoEncargo, "TipoAcordoAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&a.TipoRazaoAbatimentoEncargo, "TipoRazaoAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&a.PercentualAbatimentoEncargo, "PercentualAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&a.ValorAbatimentoEncargo, "ValorAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&a.QuantidadeIndividualAbatimentoEncargo, "QuantidadeIndividualAbatimentoEncargo")
	err = posicaoParaValor.ReturnByType(&a.TaxaAbatimento, "TaxaAbatimento")

	return err
}

var PosicoesAbatimentosEncargos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                          {0, 2, 0},
	"TipoDocumento":                         {2, 3, 0},
	"NumeroPedido":                          {3, 23, 0},
	"EanComprador":                          {23, 36, 0},
	"QuanlificadorAbatimentoEncargo":        {36, 39, 0},
	"TipoAcordoAbatimentoEncargo":           {39, 42, 0},
	"TipoRazaoAbatimentoEncargo":            {42, 45, 0},
	"PercentualAbatimentoEncargo":           {45, 50, 2},
	"ValorAbatimentoEncargo":                {50, 67, 2},
	"QuantidadeIndividualAbatimentoEncargo": {67, 84, 2},
	"TaxaAbatimento":                        {84, 89, 2},
}

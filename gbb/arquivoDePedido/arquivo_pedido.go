package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho           Cabecalho           `json:"Cabecalho"`
	Observacao          Observacao          `json:"Observacao"`
	Pagamento           Pagamento           `json:"Pagamento"`
	Embarque            Embarque            `json:"Embarque"`
	AbatimentosEncargos AbatimentosEncargos `json:"AbatimentosEncargos"`
	DadosItem           []DadosItem         `json:"DadosItem"`
	ObservacaoItem      []ObservacaoItem    `json:"ObservacaoItem"`
	Crossdocking        Crossdocking        `json:"Crossdocking"`
	Impostos            Impostos            `json:"Impostos"`
	EncargosAbatimentos EncargosAbatimentos `json:"EncargosAbatimentos"`
	Sumario             Sumario             `json:"Sumario"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			 arquivo.Cabecalho = GetCabecalho(runes)
		} else if identificador == "02" {
			err = arquivo.Observacao.ComposeStruct(string(runes))
		} else if identificador == "03" {
			err = arquivo.Pagamento.ComposeStruct(string(runes))
		} else if identificador == "04" {
			err = arquivo.Embarque.ComposeStruct(string(runes))
		} else if identificador == "05" {
			err = arquivo.AbatimentosEncargos.ComposeStruct(string(runes))
		} else if identificador == "06" {
			dadosItem := DadosItem{}
			dadosItem = GetDadosItem(runes)
			arquivo.DadosItem = append(arquivo.DadosItem, dadosItem)
		} else if identificador == "07" {
			observacaoItem := ObservacaoItem{}
			observacaoItem.ComposeStruct(string(runes))
			arquivo.ObservacaoItem = append(arquivo.ObservacaoItem, observacaoItem)
		} else if identificador == "08" {
			err = arquivo.Crossdocking.ComposeStruct(string(runes))
		} else if identificador == "09" {
			err = arquivo.Impostos.ComposeStruct(string(runes))
		} else if identificador == "10" {
			arquivo.EncargosAbatimentos = GetEncargosAbatimentos(runes)
		} else if identificador == "99" {
			arquivo.Sumario = GetSumario(runes)
		}
	}
	return arquivo, err
}

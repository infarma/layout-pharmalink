package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pharmalink/util"
	"strings"
)

type Cabecalho struct {
	TipoRegistro                      string	`json:"TipoRegistro"`
	TipoDocumento                     int32 	`json:"TipoDocumento"`
	NumeroPedido                      string	`json:"NumeroPedido"`
	IndicadorSegundaVia               int32 	`json:"IndicadorSegundaVia"`
	IndicadorCancelamento             int32 	`json:"IndicadorCancelamento"`
	ClassePedido                      string	`json:"ClassePedido"`
	DataEmissao                       int32 	`json:"DataEmissao"`
	HoraEmissao                       string	`json:"HoraEmissao"`
	DataInicioEntrega                 int32 	`json:"DataInicioEntrega"`
	HoraInicioEntrega                 string	`json:"HoraInicioEntrega"`
	DataFimEntrega                    int32 	`json:"DataFimEntrega"`
	HoraFimEntrega                    string	`json:"HoraFimEntrega"`
	DataEntrega                       int32 	`json:"DataEntrega"`
	HoraEntrega                       string	`json:"HoraEntrega"`
	DataInicioPromocao                int32 	`json:"DataInicioPromocao"`
	DataFimPromocao                   int32 	`json:"DataFimPromocao"`
	DataEmbarque                      int32 	`json:"DataEmbarque"`
	DataLimiteEmbarque                int32 	`json:"DataLimiteEmbarque"`
	NumeroContato                     string	`json:"NumeroContato"`
	NumeroTabelaPreco                 string	`json:"NumeroTabelaPreco"`
	TipoPedido                        int32 	`json:"TipoPedido"`
	CodigoTextoLivre                  string	`json:"CodigoTextoLivre"`
	TipoNegociacao                    string	`json:"TipoNegociacao"`
	CodigoCondicaoAceite              string	`json:"CodigoCondicaoAceite"`
	EanComprador                      int64 	`json:"EanComprador"`
	CGCComprador                      string 	`json:"CGCComprador"`
	NomeComprador                     string	`json:"NomeComprador"`
	EanFornecedor                     int64 	`json:"EanFornecedor"`
	CGCFornecedor                     string 	`json:"CGCFornecedor"`
	NomeFornecedor                    string	`json:"NomeFornecedor"`
	CodigoInternoFornecedor           string	`json:"CodigoInternoFornecedor"`
	EanLocalEntrega                   int64 	`json:"EanLocalEntrega"`
	CGCLocalEntrega                   string 	`json:"CGCLocalEntrega"`
	EanLocalFatura                    int64 	`json:"EanLocalFatura"`
	CGCLocalFatura                    int64 	`json:"CGCLocalFatura"`
	EanEmissorPedido                  int64 	`json:"EanEmissorPedido"`
	EanLocalEmbarque                  int64 	`json:"EanLocalEmbarque"`
	IdentificacaoDocaDescarga         string	`json:"IdentificacaoDocaDescarga"`
	NumeroPromocao                    string	`json:"NumeroPromocao"`
	IdentificacaoDepartamentoVendas   string	`json:"IdentificacaoDepartamentoVendas"`
	CodigoMoedaUtilizada              string	`json:"CodigoMoedaUtilizada"`
	CodigoInternoLoja                 string	`json:"CodigoInternoLoja"`
	NumeroCorrelativoPedidoFornecedor int32 	`json:"NumeroCorrelativoPedidoFornecedor"`
}

func GetCabecalho(runes []rune) Cabecalho {
	cabecalho := Cabecalho{}

	cabecalho.TipoRegistro = string(runes[0:2])

	cabecalho.TipoDocumento = int32(util.ConvertStringToInt(string(runes[2:3])))

	cabecalho.NumeroPedido = strings.TrimSpace(string(runes[3:23]))

	cabecalho.IndicadorSegundaVia = int32(util.ConvertStringToInt(string(runes[23:24])))

	cabecalho.IndicadorCancelamento = int32(util.ConvertStringToInt(string(runes[24:25])))

	cabecalho.ClassePedido = strings.TrimSpace(string(runes[25:28]))

	cabecalho.DataEmissao = int32(util.ConvertStringToInt(string(runes[28:36])))

	cabecalho.HoraEmissao = strings.TrimSpace(string(runes[36:40]))

	cabecalho.DataInicioEntrega = int32(util.ConvertStringToInt(string(runes[40:48])))

	cabecalho.HoraInicioEntrega = strings.TrimSpace(string(runes[48:52]))

	cabecalho.DataFimEntrega = int32(util.ConvertStringToInt(string(runes[52:60])))

	cabecalho.HoraFimEntrega = strings.TrimSpace(string(runes[60:64]))

	cabecalho.DataEntrega = int32(util.ConvertStringToInt(string(runes[64:72])))

	cabecalho.HoraEntrega = strings.TrimSpace(string(runes[72:76]))

	cabecalho.DataInicioPromocao = int32(util.ConvertStringToInt(string(runes[76:84])))

	cabecalho.DataFimPromocao = int32(util.ConvertStringToInt(string(runes[84:92])))

	cabecalho.DataEmbarque = int32(util.ConvertStringToInt(string(runes[92:100])))

	cabecalho.DataLimiteEmbarque = int32(util.ConvertStringToInt(string(runes[100:108])))

	cabecalho.NumeroContato = strings.TrimSpace(string(runes[108:128]))

	cabecalho.NumeroTabelaPreco = strings.TrimSpace(string(runes[128:148]))

	cabecalho.TipoPedido = int32(util.ConvertStringToInt(string(runes[148:150])))

	cabecalho.CodigoTextoLivre = strings.TrimSpace(string(runes[150:155]))

	cabecalho.TipoNegociacao = strings.TrimSpace(string(runes[155:156]))

	cabecalho.CodigoCondicaoAceite = strings.TrimSpace(string(runes[156:157]))

	cabecalho.EanComprador = int64(util.ConvertStringToInt(string(runes[157:170])))

	cabecalho.CGCComprador = strings.TrimSpace(string(runes[170:185]))

	cabecalho.NomeComprador = strings.TrimSpace(string(runes[185:220]))

	cabecalho.EanFornecedor = int64(util.ConvertStringToInt(string(runes[220:233])))

	cabecalho.CGCFornecedor = strings.TrimSpace(string(runes[233:248]))

	cabecalho.NomeFornecedor = strings.TrimSpace(string(runes[248:283]))

	cabecalho.CodigoInternoFornecedor = strings.TrimSpace(string(runes[283:292]))

	cabecalho.EanLocalEntrega = int64(util.ConvertStringToInt(string(runes[292:305])))

	cabecalho.CGCLocalEntrega = strings.TrimSpace(string(runes[305:320]))

	cabecalho.EanLocalFatura = int64(util.ConvertStringToInt(string(runes[320:333])))

	cabecalho.CGCLocalFatura = int64(util.ConvertStringToInt(string(runes[333:348])))

	cabecalho.EanEmissorPedido = int64(util.ConvertStringToInt(string(runes[348:361])))

	cabecalho.EanLocalEmbarque = int64(util.ConvertStringToInt(string(runes[361:374])))

	cabecalho.IdentificacaoDocaDescarga = strings.TrimSpace(string(runes[374:384]))

	cabecalho.NumeroPromocao = strings.TrimSpace(string(runes[384:394]))

	cabecalho.IdentificacaoDepartamentoVendas = strings.TrimSpace(string(runes[394:396]))

	cabecalho.CodigoMoedaUtilizada = strings.TrimSpace(string(runes[396:399]))

	cabecalho.CodigoInternoLoja = strings.TrimSpace(string(runes[399:403]))

	cabecalho.NumeroCorrelativoPedidoFornecedor = int32(util.ConvertStringToInt(string(runes[403:407])))

	return cabecalho
}
package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pharmalink/util"
	"strings"
)

type DadosItem struct {
	TipoRegistro                string  `json:"TipoRegistro"`
	TipoDocumento               int32   `json:"TipoDocumento"`
	NumeroPedido                string  `json:"NumeroPedido"`
	EanComprador                int64   `json:"EanComprador"`
	NumeroSequencialLinha       int32   `json:"NumeroSequencialLinha"`
	CodigoItem                  int64   `json:"CodigoItem"`
	TipoCodigoItem              string  `json:"TipoCodigoItem"`
	QuantidadePedida            int64 `json:"QuantidadePedida"`
	QuantidadeGratuita          int64 `json:"QuantidadeGratuita"`
	UnidadeMedida               string  `json:"UnidadeMedida"`
	QuantidadeUnidadesConsumo   int32   `json:"QuantidadeUnidadesConsumo"`
	ValorTotalLiquidoItem       float64 `json:"ValorTotalLiquidoItem"`
	ValorTotalBrutoItem         float64 `json:"ValorTotalBrutoItem"`
	PrecoUnitarioBruto          float64 `json:"PrecoUnitarioBruto"`
	PrecoUnitarioLiquido        float64 `json:"PrecoUnitarioLiquido"`
	PrecoUnitarioCalculoLiquido float64 `json:"PrecoUnitarioCalculoLiquido"`
	PrecoUnitarioCalculoBruto   float64 `json:"PrecoUnitarioCalculoBruto"`
	NumeroEmbalagens            int32   `json:"NumeroEmbalagens"`
	NumeroEmbalagensInternas    int32   `json:"NumeroEmbalagensInternas"`
	NumeroTabelaPreco           string  `json:"NumeroTabelaPreco"`
	DescricaoItem               string  `json:"DescricaoItem"`
	CodigoProdutoFornecedor     string  `json:"CodigoProdutoFornecedor"`
	CodigoProdutoComprador      string  `json:"CodigoProdutoComprador"`
	CodigoCor                   string  `json:"CodigoCor"`
	CodigoTamanho               string  `json:"CodigoTamanho"`
	DataEntrega                 int32   `json:"DataEntrega"`
	DataInicioEntrega           int32   `json:"DataInicioEntrega"`
	DataFimEntrega              int32   `json:"DataFimEntrega"`
	CodigoComplementarItem      int64   `json:"CodigoComplementarItem"`
	CodigoProjetoPharmalink     int32   `json:"CodigoProjetoPharmalink"`
	DescontoProjetoPharmalink   float32 `json:"DescontoProjetoPharmalink"`
	NumeroItemRequisicaoCompra  int32   `json:"NumeroItemRequisicaoCompra"`
	EmbalagemProduto            string  `json:"EmbalagemProduto"`
	DescontoNegociado           float64 `json:"DescontoNegociado"`
}

func GetDadosItem(runes []rune) DadosItem {
	dadosItem := DadosItem{}

	dadosItem.TipoRegistro = string(runes[0:2])

	dadosItem.TipoDocumento = int32(util.ConvertStringToInt(string(runes[2:3])))

	dadosItem.NumeroPedido = strings.TrimSpace(string(runes[3:23]))

	dadosItem.EanComprador = int64(util.ConvertStringToInt(string(runes[23:36])))

	dadosItem.NumeroSequencialLinha = int32(util.ConvertStringToInt(string(runes[36:40])))

	dadosItem.CodigoItem = int64(util.ConvertStringToInt(string(runes[40:54])))

	dadosItem.TipoCodigoItem = strings.TrimSpace(string(runes[54:57]))

	dadosItem.QuantidadePedida = int64(util.ConvertStringToFloat(string(runes[57:69]))/1000)

	dadosItem.QuantidadeGratuita = int64(util.ConvertStringToFloat(string(runes[69:81]))/1000)

	dadosItem.UnidadeMedida = strings.TrimSpace(string(runes[81:84]))

	dadosItem.QuantidadeUnidadesConsumo = int32(util.ConvertStringToFloat(string(runes[84:91])))

	dadosItem.ValorTotalLiquidoItem = util.ConvertStringToFloat(string(runes[91:108]))

	dadosItem.ValorTotalBrutoItem = util.ConvertStringToFloat(string(runes[108:125]))

	dadosItem.PrecoUnitarioBruto = util.ConvertStringToFloat(string(runes[125:142]))

	dadosItem.PrecoUnitarioLiquido = util.ConvertStringToFloat(string(runes[142:159]))

	dadosItem.PrecoUnitarioCalculoLiquido = util.ConvertStringToFloat(string(runes[159:176]))

	dadosItem.PrecoUnitarioCalculoBruto = util.ConvertStringToFloat(string(runes[176:193]))

	dadosItem.NumeroEmbalagens = int32(util.ConvertStringToInt(string(runes[193:200])))

	dadosItem.NumeroEmbalagensInternas = int32(util.ConvertStringToInt(string(runes[200:207])))

	dadosItem.NumeroTabelaPreco = strings.TrimSpace(string(runes[207:227]))

	dadosItem.DescricaoItem = strings.TrimSpace(string(runes[227:262]))

	dadosItem.CodigoProdutoFornecedor = strings.TrimSpace(string(runes[262:272]))

	dadosItem.CodigoProdutoComprador = strings.TrimSpace(string(runes[272:282]))

	dadosItem.CodigoCor = strings.TrimSpace(string(runes[282:299]))

	dadosItem.CodigoTamanho = strings.TrimSpace(string(runes[299:316]))

	dadosItem.DataEntrega = int32(util.ConvertStringToInt(string(runes[316:324])))

	dadosItem.DataInicioEntrega = int32(util.ConvertStringToInt(string(runes[324:332])))

	dadosItem.DataFimEntrega = int32(util.ConvertStringToInt(string(runes[332:340])))

	dadosItem.CodigoComplementarItem = int64(util.ConvertStringToInt(string(runes[340:353])))

	dadosItem.CodigoProjetoPharmalink = int32(util.ConvertStringToInt(string(runes[353:360])))

	dadosItem.DescontoProjetoPharmalink = float32(util.ConvertStringToFloat(string(runes[360:364])))

	dadosItem.NumeroItemRequisicaoCompra = int32(util.ConvertStringToInt(string(runes[364:369])))

	dadosItem.EmbalagemProduto = strings.TrimSpace(string(runes[369:371]))

	dadosItem.DescontoNegociado = util.ConvertStringToFloat(string(runes[371:380]))

	return dadosItem
}


package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Embarque struct {
	TipoRegistro             string  `json:"TipoRegistro"`
	TipoDOcumento            string  `json:"TipoDOcumento"`
	NumeroPedido             string  `json:"NumeroPedido"`
	EanComprador             string  `json:"EanComprador"`
	TipoCondicaoEntrega      string  `json:"TipoCondicaoEntrega"`
	TipoCodigoTransportadora string  `json:"TipoCodigoTransportadora"`
	EanCGCTransportadora     string  `json:"EanCGCTransportadora"`
	TipoVeiculo              string  `json:"TipoVeiculo"`
	ModoTransporte           string  `json:"ModoTransporte"`
	NomeTransportadora       string  `json:"NomeTransportadora"`
}

func (e *Embarque) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesEmbarque

	err = posicaoParaValor.ReturnByType(&e.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&e.TipoDOcumento, "TipoDOcumento")
	err = posicaoParaValor.ReturnByType(&e.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&e.EanComprador, "EanComprador")
	err = posicaoParaValor.ReturnByType(&e.TipoCondicaoEntrega, "TipoCondicaoEntrega")
	err = posicaoParaValor.ReturnByType(&e.TipoCodigoTransportadora, "TipoCodigoTransportadora")
	err = posicaoParaValor.ReturnByType(&e.EanCGCTransportadora, "EanCGCTransportadora")
	err = posicaoParaValor.ReturnByType(&e.TipoVeiculo, "TipoVeiculo")
	err = posicaoParaValor.ReturnByType(&e.ModoTransporte, "ModoTransporte")
	err = posicaoParaValor.ReturnByType(&e.NomeTransportadora, "NomeTransportadora")

	e.TipoDOcumento = strings.TrimSpace(e.TipoDOcumento)
	e.NumeroPedido = strings.TrimSpace(e.NumeroPedido)
	e.EanComprador = strings.TrimSpace(e.EanComprador)
	e.TipoCondicaoEntrega = strings.TrimSpace(e.TipoCondicaoEntrega)
	e.TipoCodigoTransportadora = strings.TrimSpace(e.TipoCodigoTransportadora)
	e.EanCGCTransportadora = strings.TrimSpace(e.EanCGCTransportadora)
	e.TipoVeiculo = strings.TrimSpace(e.TipoVeiculo)
	e.ModoTransporte = strings.TrimSpace(e.ModoTransporte)
	e.NomeTransportadora = strings.TrimSpace(e.NomeTransportadora)

	return err
}

var PosicoesEmbarque = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":             {0, 2, 0},
	"TipoDOcumento":            {2, 3, 0},
	"NumeroPedido":             {3, 23, 0},
	"EanComprador":             {23, 36, 0},
	"TipoCondicaoEntrega":      {36, 39, 0},
	"TipoCodigoTransportadora": {39, 42, 0},
	"EanCGCTransportadora":     {42, 57, 0},
	"TipoVeiculo":              {57, 61, 0},
	"ModoTransporte":           {61, 64, 0},
	"NomeTransportadora":       {64, 99, 0},
}

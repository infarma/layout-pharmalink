package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pharmalink/util"
	"strings"
)

type EncargosAbatimentos struct {
	TipoRegistro                          string  `json:"TipoRegistro"`
	TipoDocumento                         int32   `json:"TipoDocumento"`
	NumeroPedido                          string  `json:"NumeroPedido"`
	EanComprador                          int64   `json:"EanComprador"`
	NumeroSequencialLinha                 int32   `json:"NumeroSequencialLinha"`
	CodigoItem                            int64   `json:"CodigoItem"`
	QualificadorAbatimentoEncargo         string  `json:"QualificadorAbatimentoEncargo"`
	TipoAcordoAbatimentoEncargo           string  `json:"TipoAcordoAbatimentoEncargo"`
	TipoRazaoAbatimentoEncargo            string  `json:"TipoRazaoAbatimentoEncargo"`
	PercentualAbatimentoEncargo           float32 `json:"PercentualAbatimentoEncargo"`
	ValorAbatimentoEncargo                float64 `json:"ValorAbatimentoEncargo"`
	QuantidadeIndividualAbatimentoEncargo float64 `json:"QuantidadeIndividualAbatimentoEncargo"`
	TaxaAbatimento                        float32 `json:"TaxaAbatimento"`
}

func GetEncargosAbatimentos(runes []rune) EncargosAbatimentos {
	encargosAbatimentos := EncargosAbatimentos{}

	encargosAbatimentos.TipoRegistro = strings.TrimSpace(string(runes[0:2]))
	encargosAbatimentos.TipoDocumento = int32(util.ConvertStringToInt(string(runes[2:3])))
	encargosAbatimentos.NumeroPedido = strings.TrimSpace(string(runes[3:23]))
	encargosAbatimentos.EanComprador = int64(util.ConvertStringToInt(string(runes[23:36])))
	encargosAbatimentos.NumeroSequencialLinha = int32(util.ConvertStringToInt(string(runes[36:40])))
	encargosAbatimentos.CodigoItem = int64(util.ConvertStringToInt(string(runes[40:54])))
	encargosAbatimentos.QualificadorAbatimentoEncargo = strings.TrimSpace(string(runes[54:57]))
	encargosAbatimentos.TipoAcordoAbatimentoEncargo = strings.TrimSpace(string(runes[57:60]))
	encargosAbatimentos.TipoRazaoAbatimentoEncargo = strings.TrimSpace(string(runes[60:63]))
	encargosAbatimentos.PercentualAbatimentoEncargo = float32(util.ConvertStringToFloat(string(runes[63:68])))
	encargosAbatimentos.ValorAbatimentoEncargo = util.ConvertStringToFloat(string(runes[68:85]))
	encargosAbatimentos.QuantidadeIndividualAbatimentoEncargo = util.ConvertStringToFloat(string(runes[85:102]))
	encargosAbatimentos.TaxaAbatimento = float32(util.ConvertStringToFloat(string(runes[102:107]))/100)

	return encargosAbatimentos
}

package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ObservacaoItem struct {
	TipoRegistro          string `json:"TipoRegistro"`
	TipoDocumento         int32  `json:"TipoDocumento"`
	NumeroPedido          string `json:"NumeroPedido"`
	EanComprador          int64  `json:"EanComprador"`
	NumeroSequencialLinha int32  `json:"NumeroSequencialLinha"`
	CodigoItem            int64  `json:"CodigoItem"`
	Observacao1           string `json:"Observacao1"`
	Observacao2           string `json:"Observacao2"`
	Observacao3           string `json:"Observacao3"`
	Observacao4           string `json:"Observacao4"`
	Observacao5           string `json:"Observacao5"`
	Observacao6           string `json:"Observacao6"`
	Observacao7           string `json:"Observacao7"`
	Observacao8           string `json:"Observacao8"`
	Observacao9           string `json:"Observacao9"`
	Observacao10          string `json:"Observacao10"`
	Observacao11          string `json:"Observacao11"`
	Observacao12          string `json:"Observacao12"`
	Observacao13          string `json:"Observacao13"`
	Observacao14          string `json:"Observacao14"`
	Observacao15          string `json:"Observacao15"`
}

func (o *ObservacaoItem) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesObservacaoItem

	err = posicaoParaValor.ReturnByType(&o.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&o.TipoDocumento, "TipoDocumento")
	err = posicaoParaValor.ReturnByType(&o.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&o.EanComprador, "EanComprador")
	err = posicaoParaValor.ReturnByType(&o.NumeroSequencialLinha, "NumeroSequencialLinha")
	err = posicaoParaValor.ReturnByType(&o.CodigoItem, "CodigoItem")
	err = posicaoParaValor.ReturnByType(&o.Observacao1, "Observacao1")
	err = posicaoParaValor.ReturnByType(&o.Observacao2, "Observacao2")
	err = posicaoParaValor.ReturnByType(&o.Observacao3, "Observacao3")
	err = posicaoParaValor.ReturnByType(&o.Observacao4, "Observacao4")
	err = posicaoParaValor.ReturnByType(&o.Observacao5, "Observacao5")
	err = posicaoParaValor.ReturnByType(&o.Observacao6, "Observacao6")
	err = posicaoParaValor.ReturnByType(&o.Observacao7, "Observacao7")
	err = posicaoParaValor.ReturnByType(&o.Observacao8, "Observacao8")
	err = posicaoParaValor.ReturnByType(&o.Observacao9, "Observacao9")
	err = posicaoParaValor.ReturnByType(&o.Observacao10, "Observacao10")
	err = posicaoParaValor.ReturnByType(&o.Observacao11, "Observacao11")
	err = posicaoParaValor.ReturnByType(&o.Observacao12, "Observacao12")
	err = posicaoParaValor.ReturnByType(&o.Observacao13, "Observacao13")
	err = posicaoParaValor.ReturnByType(&o.Observacao14, "Observacao14")
	err = posicaoParaValor.ReturnByType(&o.Observacao15, "Observacao15")

	return err
}

var PosicoesObservacaoItem = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":          {0, 2, 0},
	"TipoDocumento":         {2, 3, 0},
	"NumeroPedido":          {3, 23, 0},
	"EanComprador":          {23, 36, 0},
	"NumeroSequencialLinha": {36, 40, 0},
	"CodigoItem":            {40, 54, 0},
	"Observacao1":           {54, 124, 0},
	"Observacao2":           {124, 194, 0},
	"Observacao3":           {194, 264, 0},
	"Observacao4":           {264, 334, 0},
	"Observacao5":           {334, 404, 0},
	"Observacao6":           {404, 474, 0},
	"Observacao7":           {474, 544, 0},
	"Observacao8":           {544, 614, 0},
	"Observacao9":           {614, 684, 0},
	"Observacao10":          {684, 754, 0},
	"Observacao11":          {754, 824, 0},
	"Observacao12":          {824, 894, 0},
	"Observacao13":          {894, 964, 0},
	"Observacao14":          {964, 1034, 0},
	"Observacao15":          {1034, 1104, 0},
}

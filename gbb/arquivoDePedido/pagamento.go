package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Pagamento struct {
	TipoRegistro                      string  `json:"TipoRegistro"`
	TipoDocumento                     int32   `json:"TipoDocumento"`
	NumeroPedido                      string  `json:"NumeroPedido"`
	EanComprador                      int32   `json:"EanComprador"`
	QualificadorTipoCondicaoPagamento string  `json:"QualificadorTipoCondicaoPagamento"`
	ReferenciaPrazoPagamento          string  `json:"ReferenciaPrazoPagamento"`
	ReferenciaTempoCodificada         string  `json:"ReferenciaTempoCodificada"`
	TipoPeriodoCodificado             string  `json:"TipoPeriodoCodificado"`
	NumeroPeriodos                    int32   `json:"NumeroPeriodos"`
	DataVencimentoLiquido             int32   `json:"DataVencimentoLiquido"`
	PorcentagemFatura                 float32 `json:"PorcentagemFatura"`
	ValorParcela                      float64 `json:"ValorParcela"`
	PorcentagemDescontoFinanceiro     float32 `json:"PorcentagemDescontoFinanceiro"`
	ValorImpostos                     float64 `json:"ValorImpostos"`
	IdentificacaoCondicaoPagamento    string  `json:"IdentificacaoCondicaoPagamento"`
	DescricaoCondicaoPagamento        string  `json:"DescricaoCondicaoPagamento"`
	DataBase                          int32   `json:"DataBase"`
	Juros                             float32 `json:"Juros"`
}

func (p *Pagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPagamento

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&p.TipoDocumento, "TipoDocumento")
	err = posicaoParaValor.ReturnByType(&p.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&p.EanComprador, "EanComprador")
	err = posicaoParaValor.ReturnByType(&p.QualificadorTipoCondicaoPagamento, "QualificadorTipoCondicaoPagamento")
	err = posicaoParaValor.ReturnByType(&p.ReferenciaPrazoPagamento, "ReferenciaPrazoPagamento")
	err = posicaoParaValor.ReturnByType(&p.ReferenciaTempoCodificada, "ReferenciaTempoCodificada")
	err = posicaoParaValor.ReturnByType(&p.TipoPeriodoCodificado, "TipoPeriodoCodificado")
	err = posicaoParaValor.ReturnByType(&p.NumeroPeriodos, "NumeroPeriodos")
	err = posicaoParaValor.ReturnByType(&p.DataVencimentoLiquido, "DataVencimentoLiquido")
	err = posicaoParaValor.ReturnByType(&p.PorcentagemFatura, "PorcentagemFatura")
	err = posicaoParaValor.ReturnByType(&p.ValorParcela, "ValorParcela")
	err = posicaoParaValor.ReturnByType(&p.PorcentagemDescontoFinanceiro, "PorcentagemDescontoFinanceiro")
	err = posicaoParaValor.ReturnByType(&p.ValorImpostos, "ValorImpostos")
	err = posicaoParaValor.ReturnByType(&p.IdentificacaoCondicaoPagamento, "IdentificacaoCondicaoPagamento")
	err = posicaoParaValor.ReturnByType(&p.DescricaoCondicaoPagamento, "DescricaoCondicaoPagamento")
	err = posicaoParaValor.ReturnByType(&p.DataBase, "DataBase")
	err = posicaoParaValor.ReturnByType(&p.Juros, "Juros")

	return err
}

var PosicoesPagamento = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"TipoDocumento":                     {2, 3, 0},
	"NumeroPedido":                      {3, 23, 0},
	"EanComprador":                      {23, 36, 0},
	"QualificadorTipoCondicaoPagamento": {36, 39, 0},
	"ReferenciaPrazoPagamento":          {39, 42, 0},
	"ReferenciaTempoCodificada":         {42, 45, 0},
	"TipoPeriodoCodificado":             {45, 48, 0},
	"NumeroPeriodos":                    {48, 51, 0},
	"DataVencimentoLiquido":             {51, 59, 0},
	"PorcentagemFatura":                 {59, 64, 2},
	"ValorParcela":                      {64, 82, 2},
	"PorcentagemDescontoFinanceiro":     {82, 87, 2},
	"ValorImpostos":                     {87, 105, 2},
	"IdentificacaoCondicaoPagamento":    {105, 106, 0},
	"DescricaoCondicaoPagamento":        {106, 141, 0},
	"DataBase":                          {141, 149, 0},
	"Juros":                             {149, 154, 2},
}

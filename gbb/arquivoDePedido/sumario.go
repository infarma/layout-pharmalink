package arquivoDePedido

import (
	"bitbucket.org/infarma/layout-pharmalink/util"
	"strings"
)

type Sumario struct {
	TipoRegistro                string  `json:"TipoRegistro"`
	TipoDocumento               int32   `json:"TipoDocumento"`
	NumeroPedido                string  `json:"NumeroPedido"`
	EanComprador                int64   `json:"EanComprador"`
	ValorTotalPedido            float64 `json:"ValorTotalPedido"`
	ValorTotalEncargos          float64 `json:"ValorTotalEncargos"`
	ValorTotalMercadorias       float64 `json:"ValorTotalMercadorias"`
	ValorTotalDescontoComercial float64 `json:"ValorTotalDescontoComercial"`
	ValorDespesasAcessorias     float64 `json:"ValorDespesasAcessorias"`
	ValorEncargosFinanceiros    float64 `json:"ValorEncargosFinanceiros"`
	ValorFrete                  float64 `json:"ValorFrete"`
	ValorTotalIPI               float64 `json:"ValorTotalIPI"`
	ValorTotalBonificacao       float64 `json:"ValorTotalBonificacao"`
	NumeroTotalLinhasItem       int32   `json:"NumeroTotalLinhasItem"`
}

func GetSumario(runes []rune) Sumario {
	sumario := Sumario{}

	sumario.TipoRegistro = strings.TrimSpace(string(runes[0:2]))
	sumario.TipoDocumento = int32(util.ConvertStringToInt(string(runes[2:3])))
	sumario.NumeroPedido = strings.TrimSpace(string(runes[3:23]))
	sumario.EanComprador = int64(util.ConvertStringToInt(string(runes[23:36])))
	sumario.ValorTotalPedido = util.ConvertStringToFloat(string(runes[36:53]))
	sumario.ValorTotalEncargos = util.ConvertStringToFloat(string(runes[53:70]))
	sumario.ValorTotalMercadorias = util.ConvertStringToFloat(string(runes[70:87]))
	sumario.ValorTotalDescontoComercial = util.ConvertStringToFloat(string(runes[87:104]))
	sumario.ValorDespesasAcessorias = util.ConvertStringToFloat(string(runes[104:121]))
	sumario.ValorEncargosFinanceiros = util.ConvertStringToFloat(string(runes[121:138]))
	sumario.ValorFrete = util.ConvertStringToFloat(string(runes[138:155]))
	sumario.ValorTotalIPI = util.ConvertStringToFloat(string(runes[155:172]))
	sumario.ValorTotalBonificacao = util.ConvertStringToFloat(string(runes[172:189]))
	sumario.NumeroTotalLinhasItem = int32(util.ConvertStringToInt(string(runes[189:194])))

	return sumario
}
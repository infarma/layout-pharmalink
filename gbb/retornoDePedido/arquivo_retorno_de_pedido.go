package retornoDePedido

import (
	"fmt"
	"time"
)

func GetCabecalho(idePedidoCliente string, cnpjFornecedor string, ideIniPedidoFornecedor string, preDataFaturamento time.Time,
									preDataEntregaMercadoria time.Time, statusProcePedido int8, codInternoLoja string, idePedidoPharmalink string) string{

	cabecalho := fmt.Sprint("01") + "|"
	cabecalho += fmt.Sprintf("%-12s", idePedidoCliente) + "|"
	cabecalho += fmt.Sprintf("%-14s", cnpjFornecedor) + "|"

	return cabecalho
}
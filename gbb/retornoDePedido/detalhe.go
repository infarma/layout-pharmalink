package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	CodigoRegistro             string  `json:"CodigoRegistro"`
	TipoIdentificacaoProduto   int32   `json:"TipoIdentificacaoProduto"`
	IndentificacaoProduto      string  `json:"IndentificacaoProduto"`
	QuantidadeAtendida         float64 `json:"QuantidadeAtendida"`
	QuantidadeRecusada         float64 `json:"QuantidadeRecusada"`
	MotivoRejeicao             string  `json:"MotivoRejeicao"`
	NumeroItemRequisicaoCompra int32   `json:"NumeroItemRequisicaoCompra"`
	EmbalagemProduto           string  `json:"EmbalagemProduto"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.CodigoRegistro, "CodigoRegistro")
	err = posicaoParaValor.ReturnByType(&d.TipoIdentificacaoProduto, "TipoIdentificacaoProduto")
	err = posicaoParaValor.ReturnByType(&d.IndentificacaoProduto, "IndentificacaoProduto")
	err = posicaoParaValor.ReturnByType(&d.QuantidadeAtendida, "QuantidadeAtendida")
	err = posicaoParaValor.ReturnByType(&d.QuantidadeRecusada, "QuantidadeRecusada")
	err = posicaoParaValor.ReturnByType(&d.MotivoRejeicao, "MotivoRejeicao")
	err = posicaoParaValor.ReturnByType(&d.NumeroItemRequisicaoCompra, "NumeroItemRequisicaoCompra")
	err = posicaoParaValor.ReturnByType(&d.EmbalagemProduto, "EmbalagemProduto")

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":             {0, 2, 0},
	"TipoIdentificacaoProduto":   {2, 3, 0},
	"IndentificacaoProduto":      {3, 17, 0},
	"QuantidadeAtendida":         {17, 28, 3},
	"QuantidadeRecusada":         {28, 39, 3},
	"MotivoRejeicao":             {39, 69, 0},
	"NumeroItemRequisicaoCompra": {69, 74, 0},
	"EmbalagemProduto":           {74, 76, 0},
}

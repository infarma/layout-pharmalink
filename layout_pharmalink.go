package layout_pharmalink

import (
	arquivoDePedido2_5 "bitbucket.org/infarma/layout-pharmalink/V_2_5/arquivoDePedido"
	arquivoDePedidoBPR "bitbucket.org/infarma/layout-pharmalink/bpr/arquivoDePedido"
	arquivoDePedidoPDA "bitbucket.org/infarma/layout-pharmalink/pda/arquivoDePedido"
	arquivoDePedidoGBB "bitbucket.org/infarma/layout-pharmalink/gbb/arquivoDePedido"
	arquivoDePedido2_0 "bitbucket.org/infarma/layout-pharmalink/v_2_0/arquivoDePedido"
	arquivoDePedido3_2 "bitbucket.org/infarma/layout-pharmalink/v_3_2/arquivoDePedido"
	"os"
)

func GetArquivoDePedidoBPR(fileHandle *os.File) (arquivoDePedidoBPR.ArquivoDePedido, error) {
	return arquivoDePedidoBPR.GetStruct(fileHandle)
}

func GetArquivoDePedidoPDA(fileHandle *os.File) (arquivoDePedidoPDA.ArquivoDePedido, error) {
	return arquivoDePedidoPDA.GetStruct(fileHandle)
}

func GetArquivoDePedidoGBB(fileHandle *os.File) (arquivoDePedidoGBB.ArquivoDePedido, error) {
	return arquivoDePedidoGBB.GetStruct(fileHandle)
}

func GetArquivoDePedido2_0(fileHandle *os.File) (arquivoDePedido2_0.ArquivoDePedido, error) {
	return arquivoDePedido2_0.GetStruct(fileHandle)
}

func GetArquivoDeEnvio2_5(fileHandle *os.File) arquivoDePedido2_5.ArquivoDeEnvio {
	return arquivoDePedido2_5.ConvertToStruct(fileHandle)
}

func GetArquivoDeEnvio3_2(fileHandle *os.File) arquivoDePedido3_2.ArquivoDePedido {
	return arquivoDePedido3_2.ConvertToStruct(fileHandle)
}

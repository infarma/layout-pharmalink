package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type LoteFabricacaoProdutos struct {
	CodigoRegistro    string  `json:"CodigoRegistro"`
	LoteFabricacao    string  `json:"LoteFabricacao"`
	QuantidadeProduto float64 `json:"QuantidadeProduto"`
	DataValidade      int32   `json:"DataValidade"`
	DataFabricacao    int32   `json:"DataFabricacao"`
}

func (l *LoteFabricacaoProdutos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesLoteFabricacaoProdutos

	err = posicaoParaValor.ReturnByType(&l.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&l.LoteFabricacao, "LoteFabricacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&l.QuantidadeProduto, "QuantidadeProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&l.DataValidade, "DataValidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&l.DataFabricacao, "DataFabricacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesLoteFabricacaoProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":    {0, 2, 0},
	"LoteFabricacao":    {2, 22, 0},
	"QuantidadeProduto": {22, 33, 3},
	"DataValidade":      {33, 41, 0},
	"DataFabricacao":    {41, 49, 0},
}

package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produtos struct {
	CodigoRegistro              string  `json:"CodigoRegistro"`
	TipoIdentificacaoProduto    int32   `json:"TipoIdentificacaoProduto"`
	IdentificacaoProduto        string  `json:"IdentificacaoProduto"`
	EmbalagemProduto            string  `json:"EmbalagemProduto"`
	QuantidadeEmbalagens        float64 `json:"QuantidadeEmbalagens"`
	CodigoFiscalOperacao        int32   `json:"CodigoFiscalOperacao"`
	SituacaoTributaria          string  `json:"SituacaoTributaria"`
	PrecoProduto                float64 `json:"PrecoProduto"`
	PercentualDescontoItem      float32 `json:"PercentualDescontoItem"`
	ValorDescontoItem           float64 `json:"ValorDescontoItem"`
	PercentualDescontoRepasse   float32 `json:"PercentualDescontoRepasse"`
	ValorDescontoRepasse        float64 `json:"ValorDescontoRepasse"`
	PercentualDescontoComercial float32 `json:"PercentualDescontoComercial"`
	ValorDescontoComercial      float64 `json:"ValorDescontoComercial"`
	ValorDespesaAcessorias      float64 `json:"ValorDespesaAcessorias"`
	ValorDespesaEmbalagem       float64 `json:"ValorDespesaEmbalagem"`
	NumeroItemRequicicaoCompra  int32   `json:"NumeroItemRequicicaoCompra"`
	NumeroNotaFiscal            int32   `json:"NumeroNotaFiscal"`
	SerieNotaFiscal             string  `json:"SerieNotaFiscal"`
}

func (p *Produtos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProdutos

	err = posicaoParaValor.ReturnByType(&p.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoIdentificacaoProduto, "TipoIdentificacaoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.IdentificacaoProduto, "IdentificacaoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.EmbalagemProduto, "EmbalagemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadeEmbalagens, "QuantidadeEmbalagens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoFiscalOperacao, "CodigoFiscalOperacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.SituacaoTributaria, "SituacaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PrecoProduto, "PrecoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualDescontoItem, "PercentualDescontoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorDescontoItem, "ValorDescontoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualDescontoRepasse, "PercentualDescontoRepasse")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorDescontoRepasse, "ValorDescontoRepasse")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualDescontoComercial, "PercentualDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorDescontoComercial, "ValorDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorDespesaAcessorias, "ValorDespesaAcessorias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorDespesaEmbalagem, "ValorDespesaEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroItemRequicicaoCompra, "NumeroItemRequicicaoCompra")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.SerieNotaFiscal, "SerieNotaFiscal")
	if err != nil {
		return err
	}

	return err
}

var PosicoesProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":              {0, 2, 0},
	"TipoIdentificacaoProduto":    {2, 3, 0},
	"IdentificacaoProduto":        {3, 17, 0},
	"EmbalagemProduto":            {17, 19, 0},
	"QuantidadeEmbalagens":        {19, 30, 3},
	"CodigoFiscalOperacao":        {30, 34, 0},
	"SituacaoTributaria":          {34, 36, 0},
	"PrecoProduto":                {36, 51, 5},
	"PercentualDescontoItem":      {51, 57, 4},
	"ValorDescontoItem":           {57, 72, 5},
	"PercentualDescontoRepasse":   {72, 78, 2},
	"ValorDescontoRepasse":        {78, 93, 5},
	"PercentualDescontoComercial": {93, 99, 5},
	"ValorDescontoComercial":      {99, 114, 5},
	"ValorDespesaAcessorias":      {114, 129, 5},
	"ValorDespesaEmbalagem":       {129, 144, 5},
	"NumeroItemRequicicaoCompra":  {144, 149, 0},
	"NumeroNotaFiscal":            {149, 155, 0},
	"SerieNotaFiscal":             {155, 158, 0},
}

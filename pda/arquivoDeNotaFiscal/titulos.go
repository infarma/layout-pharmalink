package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Titulos struct {
	CodigoRegistro string `json:"CodigoRegistro"`
	NumeroTitulo       string  `json:"NumeroTitulo"`
	LocalPagamento     string  `json:"LocalPagamento"`
	DataVencimento     int32   `json:"DataVencimento"`
	ValorBruto         float64 `json:"ValorBruto"`
	ValorLiquido       float64 `json:"ValorLiquido"`
	DataAntecipacao    int32   `json:"DataAntecipacao"`
	DescontoAntecipado float32 `json:"DescontoAntecipado"`
	DescontoLimite     float32 `json:"DescontoLimite"`
}

func (t *Titulos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTitulos

	err = posicaoParaValor.ReturnByType(&t.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroTitulo, "NumeroTitulo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.LocalPagamento, "LocalPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.DataVencimento, "DataVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorBruto, "ValorBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorLiquido, "ValorLiquido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.DataAntecipacao, "DataAntecipacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.DescontoAntecipado, "DescontoAntecipado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.DescontoLimite, "DescontoLimite")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTitulos = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":     {0, 2, 0},
	"NumeroTitulo":       {2, 12, 0},
	"LocalPagamento":     {12, 16, 0},
	"DataVencimento":     {16, 24, 0},
	"ValorBruto":         {24, 39, 2},
	"ValorLiquido":       {39, 54, 2},
	"DataAntecipacao":    {54, 62, 0},
	"DescontoAntecipado": {62, 66, 2},
	"DescontoLimite":     {66, 70, 2},
}

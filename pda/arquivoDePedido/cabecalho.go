package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro                      string	`json:"TipoRegistro"`
	TipoDocumento                     int32 	`json:"TipoDocumento"`
	NumeroPedido                      string	`json:"NumeroPedido"`
	IndicadorSegundaVia               int32 	`json:"IndicadorSegundaVia"`
	IndicadorCancelamento             int32 	`json:"IndicadorCancelamento"`
	ClassePedido                      string	`json:"ClassePedido"`
	DataEmissao                       int32 	`json:"DataEmissao"`
	HoraEmissao                       string	`json:"HoraEmissao"`
	DataInicioEntrega                 int32 	`json:"DataInicioEntrega"`
	HoraInicioEntrega                 string	`json:"HoraInicioEntrega"`
	DataFimEntrega                    int32 	`json:"DataFimEntrega"`
	HoraFimEntrega                    string	`json:"HoraFimEntrega"`
	DataEntrega                       int32 	`json:"DataEntrega"`
	HoraEntrega                       string	`json:"HoraEntrega"`
	DataInicioPromocao                int32 	`json:"DataInicioPromocao"`
	DataFimPromocao                   int32 	`json:"DataFimPromocao"`
	DataEmbarque                      int32 	`json:"DataEmbarque"`
	DataLimiteEmbarque                int32 	`json:"DataLimiteEmbarque"`
	NumeroCOntato                     string	`json:"NumeroCOntato"`
	NumeroTabelaPreco                 string	`json:"NumeroTabelaPreco"`
	TipoPedido                        int32 	`json:"TipoPedido"`
	CodigoTextoLivre                  string	`json:"CodigoTextoLivre"`
	TipoNegociacao                    string	`json:"TipoNegociacao"`
	CodigoCondicaoAceite              string	`json:"CodigoCondicaoAceite"`
	EamComprador                      int64 	`json:"EamComprador"`
	CGCCOmprador                      int64 	`json:"CGCCOmprador"`
	NomeComprador                     string	`json:"NomeComprador"`
	EamFornecedor                     int64 	`json:"EamFornecedor"`
	CGCFornecedor                     int64 	`json:"CGCFornecedor"`
	NomeFornecedor                    string	`json:"NomeFornecedor"`
	CodigoInternoFOrnecedor           string	`json:"CodigoInternoFOrnecedor"`
	EanLocalEntrega                   int64 	`json:"EanLocalEntrega"`
	CGCLocalEntrega                   int64 	`json:"CGCLocalEntrega"`
	EanLocalFatura                    int64 	`json:"EanLocalFatura"`
	CGCLocalFatura                    int64 	`json:"CGCLocalFatura"`
	EanEmissorPedido                  int64 	`json:"EanEmissorPedido"`
	EanLocalEmbarque                  int64 	`json:"EanLocalEmbarque"`
	IdentificacaoDocaDescarga         string	`json:"IdentificacaoDocaDescarga"`
	NumeroPromocao                    string	`json:"NumeroPromocao"`
	IdentificacaoDepartamentoVendas   string	`json:"IdentificacaoDepartamentoVendas"`
	CodigoMoedaUtilizada              string	`json:"CodigoMoedaUtilizada"`
	CodigoInternoLoja                 string	`json:"CodigoInternoLoja"`
	NumeroCorrelativoPedidoFornecedor int32 	`json:"NumeroCorrelativoPedidoFornecedor"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IndicadorSegundaVia, "IndicadorSegundaVia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IndicadorCancelamento, "IndicadorCancelamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ClassePedido, "ClassePedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataEmissao, "DataEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraEmissao, "HoraEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataInicioEntrega, "DataInicioEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraInicioEntrega, "HoraInicioEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataFimEntrega, "DataFimEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraFimEntrega, "HoraFimEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataEntrega, "DataEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraEntrega, "HoraEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataInicioPromocao, "DataInicioPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataFimPromocao, "DataFimPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataEmbarque, "DataEmbarque")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataLimiteEmbarque, "DataLimiteEmbarque")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroCOntato, "NumeroCOntato")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroTabelaPreco, "NumeroTabelaPreco")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoPedido, "TipoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoTextoLivre, "CodigoTextoLivre")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoNegociacao, "TipoNegociacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCondicaoAceite, "CodigoCondicaoAceite")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EamComprador, "EamComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CGCCOmprador, "CGCCOmprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NomeComprador, "NomeComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EamFornecedor, "EamFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CGCFornecedor, "CGCFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NomeFornecedor, "NomeFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoInternoFOrnecedor, "CodigoInternoFOrnecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalEntrega, "EanLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CGCLocalEntrega, "CGCLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalFatura, "EanLocalFatura")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CGCLocalFatura, "CGCLocalFatura")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanEmissorPedido, "EanEmissorPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalEmbarque, "EanLocalEmbarque")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoDocaDescarga, "IdentificacaoDocaDescarga")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPromocao, "NumeroPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IdentificacaoDepartamentoVendas, "IdentificacaoDepartamentoVendas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoMoedaUtilizada, "CodigoMoedaUtilizada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoInternoLoja, "CodigoInternoLoja")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroCorrelativoPedidoFornecedor, "NumeroCorrelativoPedidoFornecedor")
	if err != nil {
		return err
	}


	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"TipoDocumento":                      {2, 3, 0},
	"NumeroPedido":                      {3, 23, 0},
	"IndicadorSegundaVia":                      {23, 24, 0},
	"IndicadorCancelamento":                      {24, 25, 0},
	"ClassePedido":                      {25, 28, 0},
	"DataEmissao":                      {28, 36, 0},
	"HoraEmissao":                      {36, 40, 0},
	"DataInicioEntrega":                      {40, 48, 0},
	"HoraInicioEntrega":                      {48, 52, 0},
	"DataFimEntrega":                      {51, 60, 0},
	"HoraFimEntrega":                      {60, 64, 0},
	"DataEntrega":                      {64, 72, 0},
	"HoraEntrega":                      {72, 76, 0},
	"DataInicioPromocao":                      {76, 84, 0},
	"DataFimPromocao":                      {84, 92, 0},
	"DataEmbarque":                      {92, 100, 0},
	"DataLimiteEmbarque":                      {100, 108, 0},
	"NumeroCOntato":                      {108, 128, 0},
	"NumeroTabelaPreco":                      {128, 148, 0},
	"TipoPedido":                      {148, 150, 0},
	"CodigoTextoLivre":                      {150, 155, 0},
	"TipoNegociacao":                      {155, 156, 0},
	"CodigoCondicaoAceite":                      {156, 157, 0},
	"EamComprador":                      {157, 170, 0},
	"CGCCOmprador":                      {170, 185, 0},
	"NomeComprador":                      {185, 220, 0},
	"EamFornecedor":                      {220, 233, 0},
	"CGCFornecedor":                      {233, 248, 0},
	"NomeFornecedor":                      {248, 283, 0},
	"CodigoInternoFOrnecedor":                      {283, 292, 0},
	"EanLocalEntrega":                      {292, 305, 0},
	"CGCLocalEntrega":                      {305, 320, 0},
	"EanLocalFatura":                      {320, 333, 0},
	"CGCLocalFatura":                      {333, 348, 0},
	"EanEmissorPedido":                      {348, 361, 0},
	"EanLocalEmbarque":                      {361, 374, 0},
	"IdentificacaoDocaDescarga":                      {374, 384, 0},
	"NumeroPromocao":                      {384, 394, 0},
	"IdentificacaoDepartamentoVendas":                      {394, 396, 0},
	"CodigoMoedaUtilizada":                      {396, 399, 0},
	"CodigoInternoLoja":                      {399, 403, 0},
	"NumeroCorrelativoPedidoFornecedor":                      {403, 407, 0},
}
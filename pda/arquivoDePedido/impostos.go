package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Impostos struct {
	TipoRegistro          string 	`json:"TipoRegistro"`
	TipoDocumento         int32  	`json:"TipoDocumento"`
	NumeroPedido          string 	`json:"NumeroPedido"`
	EanComprador          int64  	`json:"EanComprador"`
	NumeroSequencialLinha int32  	`json:"NumeroSequencialLinha"`
	CodigoItem            int64  	`json:"CodigoItem"`
	AliquotaIPI           float32	`json:"AliquotaIPI"`
	ValorIPI              float64	`json:"ValorIPI"`
	ValorICMS             float64	`json:"ValorICMS"`
}

func (i *Impostos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesImpostos

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.EanComprador, "EanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroSequencialLinha, "NumeroSequencialLinha")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoItem, "CodigoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.AliquotaIPI, "AliquotaIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorIPI, "ValorIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorICMS, "ValorICMS")
	if err != nil {
		return err
	}


	return err
}

var PosicoesImpostos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"TipoDocumento":                      {2, 3, 0},
	"NumeroPedido":                      {3, 23, 0},
	"EanComprador":                      {23, 36, 0},
	"NumeroSequencialLinha":                      {36, 40, 0},
	"CodigoItem":                      {40, 54, 0},
	"AliquotaIPI":                      {54, 59, 2},
	"ValorIPI":                      {59, 76, 2},
	"ValorICMS":                      {76, 93, 2},
}
package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Sumario struct {
	TipoRegistro                string  `json:"TipoRegistro"`
	TipoDocumento               int32   `json:"TipoDocumento"`
	NumeroPedido                string  `json:"NumeroPedido"`
	EanComprador                int64   `json:"EanComprador"`
	ValorTotalPedido            float64 `json:"ValorTotalPedido"`
	ValorTotalEncargos          float64 `json:"ValorTotalEncargos"`
	ValorTotalMercadorias       float64 `json:"ValorTotalMercadorias"`
	ValorTotalDescontoComercial float64 `json:"ValorTotalDescontoComercial"`
	ValorDespesasAcessorias     float64 `json:"ValorDespesasAcessorias"`
	ValorEncargosFinanceiros    float64 `json:"ValorEncargosFinanceiros"`
	ValorFrete                  float64 `json:"ValorFrete"`
	ValorTotalIPI               float64 `json:"ValorTotalIPI"`
	ValorTotalBonificacao       float64 `json:"ValorTotalBonificacao"`
	NumeroTotalLinhasItem       int32   `json:"NumeroTotalLinhasItem"`
}

func (s *Sumario) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesSumario

	err = posicaoParaValor.ReturnByType(&s.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.EanComprador, "EanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalPedido, "ValorTotalPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalEncargos, "ValorTotalEncargos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalMercadorias, "ValorTotalMercadorias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalDescontoComercial, "ValorTotalDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorDespesasAcessorias, "ValorDespesasAcessorias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorEncargosFinanceiros, "ValorEncargosFinanceiros")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorFrete, "ValorFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalIPI, "ValorTotalIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalBonificacao, "ValorTotalBonificacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.NumeroTotalLinhasItem, "NumeroTotalLinhasItem")
	if err != nil {
		return err
	}

	return err
}

var PosicoesSumario = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                {0, 2, 0},
	"TipoDocumento":               {2, 3, 0},
	"NumeroPedido":                {3, 23, 0},
	"EanComprador":                {23, 36, 0},
	"ValorTotalPedido":            {36, 53, 2},
	"ValorTotalEncargos":          {53, 70, 2},
	"ValorTotalMercadorias":       {70, 87, 2},
	"ValorTotalDescontoComercial": {87, 104, 2},
	"ValorDespesasAcessorias":     {104, 121, 2},
	"ValorEncargosFinanceiros":    {121, 138, 2},
	"ValorFrete":                  {138, 155, 2},
	"ValorTotalIPI":               {155, 172, 2},
	"ValorTotalBonificacao":       {172, 189, 2},
	"NumeroTotalLinhasItem":       {189, 194, 0},
}

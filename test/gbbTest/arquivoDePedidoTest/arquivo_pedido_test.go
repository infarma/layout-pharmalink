package arquivoDePedidoTest

import (
	"layout-pharmalink/gbb/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {
	f, err := os.Open("../file_gbb_test/Orders.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	//Header do Arquivo
	var registro arquivoDePedido.Cabecalho
	registro.TipoRegistro = "01"
	registro.TipoDocumento = 1
	registro.NumeroPedido = "4599507095"
	registro.IndicadorSegundaVia = 0
	registro.IndicadorCancelamento = 0
	registro.ClassePedido = "220"
	registro.DataEmissao = 20190101
	registro.HoraEmissao = "0949"
	registro.DataInicioEntrega = 20190101
	registro.HoraInicioEntrega = "0949"
	registro.DataFimEntrega = 20190101
	registro.HoraFimEntrega = "0949"
	registro.DataInicioPromocao = 20190101
	registro.DataFimPromocao = 20190101
	registro.DataEmbarque = 20190101
	registro.DataLimiteEmbarque = 20190101
	registro.NumeroContato =  "459"
	registro.NumeroTabelaPreco = "459"
	registro.TipoPedido = 1
	registro.CodigoTextoLivre = "10"
	registro.TipoNegociacao = "1"
	registro.CodigoCondicaoAceite = "0"
	registro.EanComprador = 7892445567711
	registro.CGCComprador = "24455677000182"
	registro.NomeComprador = "InfarmaSistemas"
	registro.EanFornecedor = 7891737010690
	registro.CGCFornecedor = "00063960049299"
	registro.NomeFornecedor = "InfarmaSistemas"
	registro.CodigoInternoFornecedor = "12"
	registro.EanLocalEntrega = 7891737010690
	registro.CGCLocalEntrega = "24455677000182"
	registro.EanLocalFatura = 7891737010690
	registro.CGCLocalFatura = 24455677000182
	registro.EanEmissorPedido = 7891737010690
	registro.EanLocalEmbarque = 7891737010690
	registro.IdentificacaoDocaDescarga = "0"
	registro.NumeroPromocao = "0"
	registro.IdentificacaoDepartamentoVendas = "0"
	registro.CodigoMoedaUtilizada = "BRL"
	registro.CodigoInternoLoja = "BXXX"
	registro.NumeroCorrelativoPedidoFornecedor = 404

	if registro.TipoRegistro != arq.Cabecalho.TipoRegistro {
		t.Error("Tipo do registro não é compativel")
	}
	if registro.TipoDocumento != arq.Cabecalho.TipoDocumento {
		t.Error("Tipo Documento não é compativel")
	}
	if registro.NumeroPedido != arq.Cabecalho.NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if registro.IndicadorSegundaVia != arq.Cabecalho.IndicadorSegundaVia {
		t.Error("Indicador Segunda Via não é compativel")
	}
	if registro.IndicadorCancelamento != arq.Cabecalho.IndicadorCancelamento {
		t.Error("Indicador Cancelamento não é compativel")
	}
	if registro.DataEmissao != arq.Cabecalho.DataEmissao {
		t.Error("Data Emissao não é compativel")
	}
	if registro.HoraEmissao != arq.Cabecalho.HoraEmissao {
		t.Error("HoraEmissao não é compativel")
	}
	if registro.DataInicioEntrega != arq.Cabecalho.DataInicioEntrega {
		t.Error("Data Inicio Entrega não é compativel")
	}
	if registro.HoraInicioEntrega != arq.Cabecalho.HoraInicioEntrega {
		t.Error("Hora Inicio Entrega não é compativel")
	}
	if registro.DataFimEntrega != arq.Cabecalho.DataFimEntrega {
		t.Error("Data Fim Entrega não é compativel")
	}
	if registro.HoraFimEntrega != arq.Cabecalho.HoraFimEntrega {
		t.Error("Hora Fim Entrega não é compativel")
	}
	if registro.DataInicioPromocao != arq.Cabecalho.DataInicioPromocao {
		t.Error("Data Inicio Promocao não é compativel")
	}
	if registro.DataFimPromocao != arq.Cabecalho.DataFimPromocao {
		t.Error("Data Fim Promocao não é compativel")
	}
	if registro.DataEmbarque != arq.Cabecalho.DataEmbarque {
		t.Error("Data Embarque não é compativel")
	}
	if registro.DataLimiteEmbarque != arq.Cabecalho.DataLimiteEmbarque {
		t.Error("Data Limite Embarque não é compativel")
	}
	if registro.NumeroContato != arq.Cabecalho.NumeroContato {
		t.Error("Numero Contato não é compativel")
	}
	if registro.NumeroTabelaPreco != arq.Cabecalho.NumeroTabelaPreco {
		t.Error("Numero Tabela Preco não é compativel")
	}
	if registro.TipoPedido != arq.Cabecalho.TipoPedido {
		t.Error("Tipo Pedido não é compativel")
	}
	if registro.CodigoTextoLivre != arq.Cabecalho.CodigoTextoLivre {
		t.Error("Codigo Texto Livre não é compativel")
	}
	if registro.TipoNegociacao != arq.Cabecalho.TipoNegociacao {
		t.Error("Tipo Negociacao não é compativel")
	}
	if registro.CodigoCondicaoAceite != arq.Cabecalho.CodigoCondicaoAceite {
		t.Error("Codigo Condicao Aceite não é compativel")
	}
	if registro.EanComprador != arq.Cabecalho.EanComprador {
		t.Error("EamComprador não é compativel")
	}
	if registro.CGCComprador != arq.Cabecalho.CGCComprador {
		t.Error("CGC Comprador não é compativel")
	}
	if registro.NomeComprador != arq.Cabecalho.NomeComprador {
			t.Error("Nome Comprador não é compativel")
	}
	if registro.EanFornecedor != arq.Cabecalho.EanFornecedor {
		t.Error("Ean Fornecedor não é compativel")
	}
	if registro.CGCFornecedor != arq.Cabecalho.CGCFornecedor {
		t.Error("CGC Fornecedor não é compativel")
	}
	if registro.NomeFornecedor != arq.Cabecalho.NomeFornecedor {
		t.Error("Nome Fornecedor não é compativel")
	}
	if registro.CodigoInternoFornecedor != arq.Cabecalho.CodigoInternoFornecedor {
		t.Error("Codigo Interno Fornecedor não é compativel")
	}
	if registro.EanLocalEntrega != arq.Cabecalho.EanLocalEntrega {
		t.Error("Ean Local Entrega não é compativel")
	}
	if registro.CGCLocalEntrega != arq.Cabecalho.CGCLocalEntrega {
		t.Error("CGC Local Entrega não é compativel")
	}
	if registro.EanLocalFatura != arq.Cabecalho.EanLocalFatura {
		t.Error("Ean Local Fatura não é compativel")
	}
	if registro.CGCLocalFatura != arq.Cabecalho.CGCLocalFatura {
		t.Error("CGC Local Fatura não é compativel")
	}
	if registro.EanEmissorPedido != arq.Cabecalho.EanEmissorPedido {
		t.Error("Ean Emissor Pedido não é compativel")
	}
	if registro.EanLocalEmbarque != arq.Cabecalho.EanLocalEmbarque {
		t.Error("Ean Local Embarque não é compativel")
	}
	if registro.IdentificacaoDocaDescarga != arq.Cabecalho.IdentificacaoDocaDescarga {
		t.Error("Identificacao Doca Descarga não é compativel")
	}
	if registro.NumeroPromocao != arq.Cabecalho.NumeroPromocao {
		t.Error("Numero Promocao não é compativel")
	}
	if registro.IdentificacaoDepartamentoVendas != arq.Cabecalho.IdentificacaoDepartamentoVendas {
		t.Error("Identificacao Departamento Vendas não é compativel")
	}
	if registro.CodigoMoedaUtilizada != arq.Cabecalho.CodigoMoedaUtilizada {
		t.Error("Codigo Moeda Utilizada não é compativel")
	}
	if registro.CodigoInternoLoja != arq.Cabecalho.CodigoInternoLoja {
		t.Error("Codigo Interno Loja não é compativel")
	}
	if registro.NumeroCorrelativoPedidoFornecedor != arq.Cabecalho.NumeroCorrelativoPedidoFornecedor {
		t.Error("Numero Correlativo Pedido Fornecedor não é compativel")
	}
	if registro.NumeroCorrelativoPedidoFornecedor != arq.Cabecalho.NumeroCorrelativoPedidoFornecedor {
		t.Error("Numero Correlativo Pedido Fornecedor não é compativel")
	}

	//Dados do Item
	var registro06 arquivoDePedido.DadosItem
	registro06.TipoRegistro = "06"
	registro06.TipoDocumento = 1
	registro06.NumeroPedido = "4599507095"
	registro06.EanComprador = 7896004732855
	registro06.NumeroSequencialLinha = 112
	registro06.CodigoItem = 7896004732855
	registro06.TipoCodigoItem = "11"
	registro06.QuantidadePedida = 20.000
	registro06.QuantidadeGratuita = 5.000
	registro06.UnidadeMedida = "CX"
	registro06.QuantidadeUnidadesConsumo = 25
	registro06.ValorTotalLiquidoItem = 123.98
	registro06.ValorTotalBrutoItem = 123.98
	registro06.PrecoUnitarioBruto = 123.98
	registro06.PrecoUnitarioLiquido = 123.98
	registro06.PrecoUnitarioCalculoLiquido = 123.98
	registro06.PrecoUnitarioCalculoBruto = 123.98
	registro06.NumeroEmbalagens = 13
	registro06.NumeroEmbalagensInternas = 13
	registro06.NumeroTabelaPreco = "13"
	registro06.DescricaoItem = "InfarmaSistemas"
	registro06.CodigoProdutoFornecedor = "7896004"
	registro06.CodigoProdutoComprador = "7896004"
	registro06.CodigoCor = "123"
	registro06.CodigoTamanho = "123"
	registro06.DataEntrega = 20190101
	registro06.DataInicioEntrega = 20190101
	registro06.DataFimEntrega = 20190101
	registro06.CodigoComplementarItem = 123
	registro06.CodigoProjetoPharmalink = 123
	registro06.DescontoProjetoPharmalink = 123
	registro06.NumeroItemRequisicaoCompra = 123
	registro06.EmbalagemProduto = "8"
	registro06.DescontoNegociado = 21.54

	if registro06.TipoRegistro != arq.DadosItem[0].TipoRegistro {
		t.Error(" não é compativel")
	}
	if registro06.TipoDocumento != arq.DadosItem[0].TipoDocumento {
		t.Error("Tipo Documento não é compativel")
	}
	if registro06.NumeroPedido != arq.DadosItem[0].NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if registro06.EanComprador != arq.DadosItem[0].EanComprador {
		t.Error("Ean Comprador não é compativel")
	}
	if registro06.NumeroSequencialLinha != arq.DadosItem[0].NumeroSequencialLinha {
		t.Error("Numero Sequencial Linha não é compativel")
	}
	if registro06.CodigoItem != arq.DadosItem[0].CodigoItem {
		t.Error("Codigo Item não é compativel")
	}
	if registro06.TipoCodigoItem != arq.DadosItem[0].TipoCodigoItem {
		t.Error("Tipo Codigo Item não é compativel")
	}
	if registro06.QuantidadePedida != arq.DadosItem[0].QuantidadePedida {
		t.Error("Quantidade Pedida não é compativel")
	}
	if registro06.QuantidadeGratuita != arq.DadosItem[0].QuantidadeGratuita {
		t.Error("Quantidade Gratuita não é compativel")
	}
	if registro06.UnidadeMedida != arq.DadosItem[0].UnidadeMedida {
		t.Error("Unidade Medida não é compativel")
	}
	if registro06.QuantidadeUnidadesConsumo != arq.DadosItem[0].QuantidadeUnidadesConsumo {
		t.Error("Quantidade Unidades Consumo não é compativel")
	}
	if registro06.ValorTotalLiquidoItem != arq.DadosItem[0].ValorTotalLiquidoItem {
		t.Error("Valor Total Liquido Item não é compativel")
	}
	if registro06.ValorTotalBrutoItem != arq.DadosItem[0].ValorTotalBrutoItem {
		t.Error("Valor Total Bruto Item não é compativel")
	}
	if registro06.PrecoUnitarioBruto != arq.DadosItem[0].PrecoUnitarioBruto {
		t.Error("Preco Unitario Bruto não é compativel")
	}
	if registro06.PrecoUnitarioLiquido != arq.DadosItem[0].PrecoUnitarioLiquido {
		t.Error("Preco Unitario Liquido não é compativel")
	}
	if registro06.PrecoUnitarioCalculoLiquido != arq.DadosItem[0].PrecoUnitarioCalculoLiquido {
		t.Error("Preco Unitario Calculo Liquido não é compativel")
	}
	if registro06.PrecoUnitarioCalculoBruto != arq.DadosItem[0].PrecoUnitarioCalculoBruto {
		t.Error("Preco Unitario Calculo Bruto não é compativel")
	}
	if registro06.NumeroEmbalagens != arq.DadosItem[0].NumeroEmbalagens {
		t.Error("Numero Embalagens não é compativel")
	}
	if registro06.NumeroEmbalagensInternas != arq.DadosItem[0].NumeroEmbalagensInternas {
		t.Error("Numero Embalagens Internas não é compativel")
	}
	if registro06.NumeroTabelaPreco != arq.DadosItem[0].NumeroTabelaPreco {
		t.Error("Numero Tabela Preco não é compativel")
	}
	if registro06.DescricaoItem != arq.DadosItem[0].DescricaoItem {
		t.Error("Descricao Item não é compativel")
	}
	if registro06.CodigoProdutoFornecedor != arq.DadosItem[0].CodigoProdutoFornecedor {
		t.Error("Codigo Produto Fornecedor não é compativel")
	}
	if registro06.CodigoProdutoComprador != arq.DadosItem[0].CodigoProdutoComprador {
		t.Error("Codigo Produto Comprador não é compativel")
	}
	if registro06.CodigoCor != arq.DadosItem[0].CodigoCor {
		t.Error("Codigo Cor não é compativel")
	}
	if registro06.CodigoTamanho != arq.DadosItem[0].CodigoTamanho {
		t.Error("Codigo Tamanho não é compativel")
	}
	if registro06.DataEntrega != arq.DadosItem[0].DataEntrega {
		t.Error("Data Entrega não é compativel")
	}
	if registro06.DataInicioEntrega != arq.DadosItem[0].DataInicioEntrega {
		t.Error("Data Inicio Entrega não é compativel")
	}
	if registro06.DataFimEntrega != arq.DadosItem[0].DataFimEntrega {
		t.Error("Data Fim Entrega não é compativel")
	}
	if registro06.CodigoComplementarItem != arq.DadosItem[0].CodigoComplementarItem {
		t.Error("Codigo Complementar Item não é compativel")
	}
	if registro06.CodigoProjetoPharmalink != arq.DadosItem[0].CodigoProjetoPharmalink {
		t.Error("Codigo Projeto Pharmalink não é compativel")
	}
	if registro06.DescontoProjetoPharmalink != arq.DadosItem[0].DescontoProjetoPharmalink {
		t.Error("Desconto Projeto Pharmalink não é compativel")
	}
	if registro06.NumeroItemRequisicaoCompra != arq.DadosItem[0].NumeroItemRequisicaoCompra {
		t.Error("Numero Item Requisicao Compra não é compativel")
	}
	if registro06.EmbalagemProduto != arq.DadosItem[0].EmbalagemProduto {
		t.Error("Embalagem Produto não é compativel")
	}
	if registro06.DescontoNegociado != arq.DadosItem[0].DescontoNegociado {
		t.Error(" Desconto Negociado não é compativel")
	}

	var registro10 arquivoDePedido.EncargosAbatimentos
	registro10.TipoRegistro = "10"
	registro10.TipoDocumento = 1
	registro10.NumeroPedido = "4599507095"
	registro10.EanComprador = 7896004732855
	registro10.NumeroSequencialLinha = 0
	registro10.CodigoItem = 7896004732855
	registro10.QualificadorAbatimentoEncargo = "4"
	registro10.TipoAcordoAbatimentoEncargo = "1"
	registro10.TipoRazaoAbatimentoEncargo = "1"
	registro10.PercentualAbatimentoEncargo = 500
	registro10.ValorAbatimentoEncargo = 21.99
	registro10.QuantidadeIndividualAbatimentoEncargo = 21.99
	registro10.TaxaAbatimento = 1.88

	if registro10.TipoRegistro != arq.EncargosAbatimentos.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if registro10.TipoDocumento != arq.EncargosAbatimentos.TipoDocumento {
		t.Error("Tipo Documento não é compativel")
	}
	if registro10.NumeroPedido != arq.EncargosAbatimentos.NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if registro10.EanComprador != arq.EncargosAbatimentos.EanComprador {
		t.Error("Ean Comprador não é compativel")
	}
	if registro10.NumeroSequencialLinha != arq.EncargosAbatimentos.NumeroSequencialLinha {
		t.Error("Numero Sequencial Linha não é compativel")
	}
	if registro10.CodigoItem != arq.EncargosAbatimentos.CodigoItem {
		t.Error("Codigo Item não é compativel")
	}
	if registro10.QualificadorAbatimentoEncargo != arq.EncargosAbatimentos.QualificadorAbatimentoEncargo {
		t.Error("Qualificador Abatimento Encargo não é compativel")
	}
	if registro10.TipoAcordoAbatimentoEncargo != arq.EncargosAbatimentos.TipoAcordoAbatimentoEncargo {
		t.Error("Tipo Acordo Abatimento Encargo não é compativel")
	}
	if registro10.TipoRazaoAbatimentoEncargo != arq.EncargosAbatimentos.TipoRazaoAbatimentoEncargo {
		t.Error("Tipo Razao Abatimento Encargo não é compativel")
	}
	if registro10.PercentualAbatimentoEncargo != arq.EncargosAbatimentos.PercentualAbatimentoEncargo {
		t.Error("Percentual Abatimento Encargo não é compativel")
	}
	if registro10.ValorAbatimentoEncargo != arq.EncargosAbatimentos.ValorAbatimentoEncargo {
		t.Error("Valor Abatimento Encargo não é compativel")
	}
	if registro10.QuantidadeIndividualAbatimentoEncargo != arq.EncargosAbatimentos.QuantidadeIndividualAbatimentoEncargo {
		t.Error("Quantidade Individual Abatimento Encargo não é compativel")
	}
	if registro10.TaxaAbatimento != arq.EncargosAbatimentos.TaxaAbatimento {
		t.Error("Taxa Abatimento não é compativel")
	}

	var registro99 arquivoDePedido.Sumario
	registro99.TipoRegistro = "99"
	registro99.TipoDocumento = 1
	registro99.NumeroPedido = "4599507095"
	registro99.EanComprador = 7896004732855
	registro99.ValorTotalPedido = 12.34
	registro99.ValorTotalEncargos = 12.34
	registro99.ValorTotalMercadorias = 12.34
	registro99.ValorTotalDescontoComercial = 12.34
	registro99.ValorDespesasAcessorias = 12.34
	registro99.ValorEncargosFinanceiros = 12.34
	registro99.ValorFrete = 12.34
	registro99.ValorTotalIPI = 12.34
	registro99.ValorTotalBonificacao = 12.34
	registro99.NumeroTotalLinhasItem = 123

	if registro99.TipoRegistro != arq.Sumario.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if registro99.TipoDocumento != arq.Sumario.TipoDocumento {
		t.Error("Tipo Documento não é compativel")
	}
	if registro99.NumeroPedido != arq.Sumario.NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if registro99.EanComprador != arq.Sumario.EanComprador {
		t.Error("Ean Comprador não é compativel")
	}
	if registro99.ValorTotalPedido != arq.Sumario.ValorTotalPedido {
		t.Error("Valor Total Pedido não é compativel")
	}
	if registro99.ValorTotalEncargos != arq.Sumario.ValorTotalEncargos {
		t.Error("Valor Total Encargos não é compativel")
	}
	if registro99.ValorTotalMercadorias != arq.Sumario.ValorTotalMercadorias {
		t.Error("Valor Total Mercadorias não é compativel")
	}
	if registro99.ValorTotalDescontoComercial != arq.Sumario.ValorTotalDescontoComercial {
		t.Error("Valor Total Desconto Comercial não é compativel")
	}
	if registro99.ValorDespesasAcessorias != arq.Sumario.ValorDespesasAcessorias {
		t.Error("Valor Despesas Acessorias não é compativel")
	}
	if registro99.ValorEncargosFinanceiros != arq.Sumario.ValorEncargosFinanceiros {
		t.Error("Valor Encargos Financeiros não é compativel")
	}
	if registro99.ValorFrete != arq.Sumario.ValorFrete {
		t.Error("Valor Frete não é compativel")
	}
	if registro99.ValorTotalIPI != arq.Sumario.ValorTotalIPI {
		t.Error("Valor Total IPI não é compativel")
	}
	if registro99.ValorTotalBonificacao != arq.Sumario.ValorTotalBonificacao {
		t.Error("Valor Total Bonificacao não é compativel")
	}
	if registro99.NumeroTotalLinhasItem != arq.Sumario.NumeroTotalLinhasItem {
		t.Error("Numero Total Linhas Item não é compativel")
	}
}
package arquivoDeCancelamentoTest

import (
	"testing"
	"layout-pharmalink/v_3_2/arquivoDeCancelamento"
	"time"
)

func TestDadosDeCancelamento01(t *testing.T){
	//TODO: Implementar teste conforme layout de exemplo
	verLayout := "2"
	src := "2019-02-27T14:36:13.001Z"
	datCancel, _ := time.Parse(time.RFC3339, src)
	numCnpjGerPed := "1141490000109"
	numCnpj := "1141490000109"
	codCtrPrj := ""
	codPedCli := ""
	codMtvCanCab := 1

	dadosDeCancelamento01 := arquivoDeCancelamento.DadosDeCancelamento01(verLayout, datCancel, numCnpjGerPed, numCnpj, codCtrPrj, codPedCli, int8(codMtvCanCab))

	if (len(dadosDeCancelamento01) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if dadosDeCancelamento01 != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestItensDoCancelamento02(t *testing.T){
	//TODO: Implementar teste conforme layout de exemplo
	codPrdCli := "7896094903234"
	qtdAtendi := 20
	codMtvCanCab := 1


	itensDoCancelamento02 := arquivoDeCancelamento.ItensDoCancelamento02(codPrdCli, int32(qtdAtendi), int8(codMtvCanCab))

	if (len(itensDoCancelamento02) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if itensDoCancelamento02 != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestVerificadoresETotaisDoCancelamento03(t *testing.T){
	//TODO: Implementar teste conforme layout de exemplo
	totItens := 40
	totAtendi := 20
	totNaoAten := 20

	verificadoresETotaisDoCancelamento03 := arquivoDeCancelamento.VerificadoresETotaisDoCancelamento03(int32(totItens), int32(totAtendi), int32(totNaoAten))

	if (len(verificadoresETotaisDoCancelamento03) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if verificadoresETotaisDoCancelamento03 != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}
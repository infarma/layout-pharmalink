package arquivoDeNotaFiscalTest

import (
	"testing"
	"layout-pharmalink/v_3_2/arquivoDeNotaFiscal"
	"time"
)

func TestCabecalhoDaNota(t *testing.T){
	//TODO: Implementar teste conforme layout de exemplo
	numeroNF := 1
	serieNF := "1"
	tipoNF := "NF"
	codigoPrazoDeterminado := ""
	qtdeNFPedido := 2
	cnpjPedido := "24455677000182"
	cnpjEntrega := "24455677000182"
	cnpjDistribuidor := "24455677000182"
	controleProjeto := ""
	numeroPedidoPharmaLink := 1
	numeroPedidoCliente := ""
	Chave_NFE := ""

	cabecalhoDaNota := arquivoDeNotaFiscal.CabecalhoDaNota(int64(numeroNF), serieNF, tipoNF, codigoPrazoDeterminado, int32(qtdeNFPedido), cnpjPedido, cnpjEntrega, cnpjDistribuidor,
																													controleProjeto, int64(numeroPedidoPharmaLink), numeroPedidoCliente, Chave_NFE)

	if (len(cabecalhoDaNota) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if cabecalhoDaNota != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestDatasDaNota(t *testing.T){
	//TODO: Implementar teste conforme layout de exemplo
	dtFaturamentoPed := "2019-02-27T14:36:13.001Z"
	dtFaturamentoPed1, _ := time.Parse(time.RFC3339, dtFaturamentoPed)

	dtGerNf := "2019-02-27T14:36:13.001Z"
	dtGerNf1, _ := time.Parse(time.RFC3339, dtGerNf)

	dtSaidaMerc := "2019-02-27T14:36:13.001Z"
	dtSaidaMerc1, _ := time.Parse(time.RFC3339, dtSaidaMerc)

	dtEmissNF := "2019-02-27T14:36:13.001Z"
	dtEmissNF1, _ := time.Parse(time.RFC3339, dtEmissNF)

	valorTotal := 2.45
	valorTotalDesc := 2.45
	valorTotalLiq := 2.45
	valorTotalEncarg := 2.45
	valorTotalFrete := 2.45
	valorBaseICMS := 2.45
	valorTotalBaseICMS := 2.45
	valorBaseICMS_ST := 2.45
	valorTotalBaseICMS_ST := 2.45
	valorBaseICMS_RT := 2.45
	valorTotalBaseICMS_RT := 2.45
	valorBaseIPI := 2.45
	valorTotalIPE := 2.45

	datasDaNota := arquivoDeNotaFiscal.DatasDaNota(dtFaturamentoPed1, dtGerNf1, dtSaidaMerc1, dtEmissNF1, valorTotal, valorTotalDesc, valorTotalLiq, valorTotalEncarg,
																								valorTotalFrete, valorBaseICMS, valorTotalBaseICMS, valorBaseICMS_ST, valorTotalBaseICMS_ST, valorBaseICMS_RT,
																								valorTotalBaseICMS_RT, valorBaseIPI, valorTotalIPE)

	if (len(datasDaNota) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if datasDaNota != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestItensDaNota(t *testing.T) {
	//TODO: Implementar teste conforme layout de exemplo
	ean := "7896094903234"
	codProdPLK := ""
	codProdDist := "0"
	quantidadeFaturada := 20
	tipoEmbalagem := ""
	descontoComercial := 1.00
	descontoFinanceiro := 1.00
	descontoTotal := 2.00
	valorBrutoUnit := 50.00
	valorDescontoUnit := 20
	valorLiquidoUnit := 50
	valorBrutoLinha := 20
	valorLiquidoLinha := 30
	motivo := ""

	itensDaNota := arquivoDeNotaFiscal.ItensDaNota(ean, codProdPLK, codProdDist, int64(quantidadeFaturada), tipoEmbalagem, float32(descontoComercial), float32(descontoFinanceiro), float32(descontoTotal),
		valorBrutoUnit, float64(valorDescontoUnit), float64(valorLiquidoUnit), float64(valorBrutoLinha), float64(valorLiquidoLinha), motivo)

	if (len(itensDaNota) != 72) {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	} else {
		if itensDaNota != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestVerificadoresESomadoresDaNota(t *testing.T) {
	//TODO: Implementar teste conforme layout de exemplo
	qtdeItens := 3
	qtdUnidFaturadas := 2

	verificadoresESomadoresDaNota := arquivoDeNotaFiscal.VerificadoresESomadoresDaNota(int32(qtdeItens), int64(qtdUnidFaturadas))

	if (len(verificadoresESomadoresDaNota) != 72) {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	} else {
		if verificadoresESomadoresDaNota != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}
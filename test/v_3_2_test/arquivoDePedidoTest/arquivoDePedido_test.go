package arquivoDePedidoTest

import (
	"layout-pharmalink/v_3_2/arquivoDePedido"
	"os"
	"testing"
)

func TestArquivoDePedido(t *testing.T){
	file, err := os.Open("../file_v_3_2_test/PHL001424455677000182000000004820181024151801.TXT")
	if err != nil {
		t.Error("não abriu arquivo", err)}

	arquivo := arquivoDePedido.ConvertToStruct(file)

	//Dados do Comprador
  var registro1 arquivoDePedido.DadosDoCompradorEDeFaturamentoDoPedido
	registro1.TipoLinha = "01"
	registro1.VerLayout = "221"
	registro1.CnpjPedido = "01141490000109"
	registro1.CnpjEntrega = "01141490000109"
	registro1.Faturamento = 1
	registro1.TipoPedido = 0
	registro1.Origem = "PHLINK"
	registro1.ControleProjeto = "0014"
	registro1.FillerLinha1 = "000000000000000000000000000000000000000000000"

	if registro1.TipoLinha != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.TipoLinha {
		t.Error("Tipo Linha não é compativel")
	}
	if registro1.VerLayout != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.VerLayout {
		t.Error("Ver Layout não é compativel")
	}
	if registro1.CnpjPedido != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.CnpjPedido {
		t.Error("Cnpj Pedido não é compativel")
	}
	if registro1.CnpjEntrega != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.CnpjEntrega {
		t.Error("Cnpj Entrega não é compativel")
	}
	if registro1.Faturamento != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.Faturamento {
		t.Error("Faturamento não é compativel")
	}
	if registro1.TipoLinha != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.TipoLinha {
		t.Error("Tipo Linha não é compativel")
	}
	if registro1.Origem != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.Origem {
		t.Error("Origem não é compativel")
	}
	if registro1.ControleProjeto != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.ControleProjeto {
		t.Error("Controle Projeto não é compativel")
	}

	if registro1.FillerLinha1 != arquivo.DadosDoCompradorEDeFaturamentoDoPedido.FillerLinha1 {
		t.Error("Filler Linha 1 não é compativel")
	}

	//Numero do Pedido
	var registro2 arquivoDePedido.NumeroDoPedido
	registro2.TipoLinha = "02"
	registro2.CodigoProjeto = "H"
	registro2.NumeroPedidoPharmaLink = 48
	registro2.NumeroPedidoEletronico = 0
	registro2.NumeroPedidoCliente = ""
	registro2.CnpjDistribuidor = "24455677000182"
	registro2.FillerLinha2 = "00000000000000000000000000000000000000"

	if registro2.TipoLinha != arquivo.NumeroDoPedido[0].TipoLinha {
		t.Error("Tipo Linha não é compativel")
	}
	if registro2.CodigoProjeto != arquivo.NumeroDoPedido[0].CodigoProjeto {
		t.Error("Codigo Projeto não é compativel")
	}
	if registro2.NumeroPedidoPharmaLink != arquivo.NumeroDoPedido[0].NumeroPedidoPharmaLink {
		t.Error("Numero Pedido PharmaLink não é compativel")
	}
	if registro2.NumeroPedidoEletronico != arquivo.NumeroDoPedido[0].NumeroPedidoEletronico {
		t.Error("Numero Pedido Eletronico não é compativel")
	}
	if registro2.NumeroPedidoCliente != arquivo.NumeroDoPedido[0].NumeroPedidoCliente {
		t.Error("Numero Pedido Cliente não é compativel")
	}
	if registro2.CnpjDistribuidor != arquivo.NumeroDoPedido[0].CnpjDistribuidor {
		t.Error("Cnpj Distribuidor não é compativel")
	}
	if registro2.FillerLinha2 != arquivo.NumeroDoPedido[0].FillerLinha2 {
		t.Error("Filler Linha 2 não é compativel")
	}

	//Formas de Pagamento do Pedido
	var registro3 arquivoDePedido.FormasDePagamentoDoPedido
	registro3.TipoLinha = "03"
	registro3.TipoPagamento = "01"
	registro3.CodigoPrazoDeterminado = "0000"
	registro3.NumeroDiasPrazo = 0
	registro3.NumeroPedidoPrincipal = 0
	registro3.FillerLinha3 = "00000000000000000000000000000000000000000000000000000000000000000000000"

	if registro3.TipoLinha != arquivo.FormasDePagamentoDoPedido[0].TipoLinha {
		t.Error("Tipo de Linha não é compativel")
	}
	if registro3.TipoPagamento != arquivo.FormasDePagamentoDoPedido[0].TipoPagamento {
		t.Error("Tipo de Pagamento não é compativel")
	}
	if registro3.CodigoPrazoDeterminado != arquivo.FormasDePagamentoDoPedido[0].CodigoPrazoDeterminado {
		t.Error("Codigo Prazo Determinado não é compativel")
	}
	if registro3.NumeroDiasPrazo != arquivo.FormasDePagamentoDoPedido[0].NumeroDiasPrazo {
		t.Error("Numero Dias Prazo não é compativel")
	}
	if registro3.NumeroPedidoPrincipal != arquivo.FormasDePagamentoDoPedido[0].NumeroPedidoPrincipal {
		t.Error("Numero Pedido Principal não é compativel")
	}
	if registro3.FillerLinha3 != arquivo.FormasDePagamentoDoPedido[0].FillerLinha3 {
		t.Error("Filler Linha 3 não é compativel")
	}

	//Datas do Pedido
	var registro4 arquivoDePedido.DatasDoPedido
	registro4.TipoLinha = "04"
	registro4.DtGeraPedidoPdv = "24102018"
	registro4.HrGeraPedidoPdv = "151504"
	registro4.DtGeraPedidoPlk = "24102018"
	registro4.HrGeraPedidoPlk = "151504"
	registro4.DtEnvioArquivoPlk = "24102018"
	registro4.HrEnvioArquivoPlk = "151504"
	registro4.DtFaturamentoPedido = "24102018"
	registro4.FillerLinha4 = "0000000000000000000000000000000000000000"

	if registro4.TipoLinha != arquivo.DatasDoPedido[0].TipoLinha {
		t.Error("Tipo de Linha não é compativel")
	}
	if registro4.DtGeraPedidoPdv != arquivo.DatasDoPedido[0].DtGeraPedidoPdv {
		t.Error("Dt Gera Pedido Pdv não é compativel")
	}
	if registro4.HrGeraPedidoPdv != arquivo.DatasDoPedido[0].HrGeraPedidoPdv {
		t.Error("Hr Gera Pedido Pdv não é compativel")
	}
	if registro4.DtGeraPedidoPlk != arquivo.DatasDoPedido[0].DtGeraPedidoPlk {
		t.Error("Dt Gera Pedido Plk não é compativel")
	}
	if registro4.HrGeraPedidoPlk != arquivo.DatasDoPedido[0].HrGeraPedidoPlk {
		t.Error("Hr Gera Pedido Plk não é compativel")
	}
	if registro4.DtEnvioArquivoPlk != arquivo.DatasDoPedido[0].DtEnvioArquivoPlk {
		t.Error("Dt Envio Arquivo Plk não é compativel")
	}
	if registro4.HrEnvioArquivoPlk != arquivo.DatasDoPedido[0].HrEnvioArquivoPlk {
		t.Error("Hr  Envio Arquivo Plk não é compativel")
	}
	if registro4.DtFaturamentoPedido != arquivo.DatasDoPedido[0].DtFaturamentoPedido {
		t.Error("Dt Faturamento Pedido não é compativel")
	}
	if registro4.FillerLinha4 != arquivo.DatasDoPedido[0].FillerLinha4 {
		t.Error("Filler Linha4 não é compativel")
	}

	//Itens Pedido
	var registro5 arquivoDePedido.ItensDoPedido
	registro5.TipoLinha = "05"
	registro5.Ean = "7896094903234"
	registro5.CodProdPlk = "0000000000000"
	registro5.CodProdDist = ""
	registro5.QuantidadePedida = 1
	registro5.TipoEmbalagem = ""
	registro5.TipoDesconto = 0
	registro5.Desconto = 14.00
	registro5.ValorBrutoUnit = 51.12
	registro5.ValorDescUnit = 0
	registro5.ValorLiquidoUnit = 0
	registro5.FillerLinha5 = "00000000"

	if registro5.TipoLinha != arquivo.ItensDoPedido[0].TipoLinha {
		t.Error("Tipo de Linha não é compativel")
	}
	if registro5.Ean != arquivo.ItensDoPedido[0].Ean {
		t.Error("Ean não é compativel")
	}
	if registro5.CodProdPlk != arquivo.ItensDoPedido[0].CodProdPlk {
		t.Error("Cod Prod Plk não é compativel")
	}
	if registro5.QuantidadePedida != arquivo.ItensDoPedido[0].QuantidadePedida {
		t.Error("Quantidade Pedida não é compativel")
	}
	if registro5.TipoEmbalagem != arquivo.ItensDoPedido[0].TipoEmbalagem {
		t.Error("Tipo Embalagem não é compativel")
	}
	if registro5.TipoDesconto != arquivo.ItensDoPedido[0].TipoDesconto {
		t.Error("Tipo Desconto não é compativel")
	}
	if registro5.Desconto != arquivo.ItensDoPedido[0].Desconto {
		t.Error("Desconto não é compativel")
	}
	if registro5.ValorBrutoUnit != arquivo.ItensDoPedido[0].ValorBrutoUnit {
		t.Error("Valor Bruto Unit não é compativel")
	}
	if registro5.ValorDescUnit != arquivo.ItensDoPedido[0].ValorDescUnit {
		t.Error("Valor Desc Unit não é compativel")
	}
	if registro5.ValorLiquidoUnit != arquivo.ItensDoPedido[0].ValorLiquidoUnit {
		t.Error("Valor Liquido Unit não é compativel")
	}
	if registro5.FillerLinha5 != arquivo.ItensDoPedido[0].FillerLinha5 {
		t.Error("Fille rLinha 5 não é compativel")
	}

	var registro6 arquivoDePedido.VerificadoresETotalizadoresDoPedido
	registro6.TipoLinha = "06"
	registro6.QtdeLinhasDeItens = 13
	registro6.SomaUnidades = 0
	registro6.ValorBrutoPedido = 0
	registro6.ValorLiquidoPedido = 0
	registro6.FillerLinha6 = "00000000000000000000000000000000000000000000000000"

	if registro6.TipoLinha != arquivo.VerificadoresETotalizadoresDoPedido.TipoLinha {
		t.Error(" não é compativel")
	}
	if registro6.QtdeLinhasDeItens != arquivo.VerificadoresETotalizadoresDoPedido.QtdeLinhasDeItens {
		t.Error("Qt de Linhas De Itens não é compativel")
	}
	if registro6.SomaUnidades != arquivo.VerificadoresETotalizadoresDoPedido.SomaUnidades {
		t.Error("Soma Unidades não é compativel")
	}
	if registro6.ValorBrutoPedido != arquivo.VerificadoresETotalizadoresDoPedido.ValorBrutoPedido {
		t.Error("Valor Bruto Pedido não é compativel")
	}
	if registro6.ValorLiquidoPedido != arquivo.VerificadoresETotalizadoresDoPedido.ValorLiquidoPedido {
		t.Error("Valor Liquido Pedido não é compativel")
	}
	if registro6.FillerLinha6 != arquivo.VerificadoresETotalizadoresDoPedido.FillerLinha6 {
		t.Error("Filler Linha 6 não é compativel")
	}
}
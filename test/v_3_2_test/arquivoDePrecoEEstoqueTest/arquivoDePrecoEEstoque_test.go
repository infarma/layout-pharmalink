package arquivoDePrecoEEstoqueTest

import (
	"testing"
	"layout-pharmalink/v_3_2/arquivoDePrecoEEstoque"
	"time"
)

func TestDadosDoEmissor(t *testing.T){
	//TODO: Impletar teste
	src := "2019-02-27T14:36:13.001Z"
	dataConsulta, _ := time.Parse(time.RFC3339, src)
	cnpjCD := "1141490000109"
	controleProjeto := "s"
	tipoAtualizacao := 1
	uf := "CE"

	dadosDoPedido := arquivoDePrecoEEstoque.DadosDoEmissor(dataConsulta, cnpjCD, controleProjeto, int32(tipoAtualizacao), uf)

	if (len(dadosDoPedido) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if dadosDoPedido != "012210114149000010901141490000109     0014000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestInformacoesDosItens(t *testing.T){
	//TODO: Impletar teste
	ean := "7896094903234"
	codProdCD := ""
	estoque := 4
	preco := 40.00
	desconto := 10.00
	status := 1

	informaçõesDosItens := arquivoDePrecoEEstoque.InformacoesDosItens(ean, codProdCD, int64(estoque), preco, desconto, int32(status))

	if (len(informaçõesDosItens) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if informaçõesDosItens != "012210114149000010901141490000109     0014000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestVerificadoresESomarizadoresDoRetorno(t *testing.T){
	//TODO: Impletar teste
	qtdeLinhasItens := 3

	informaçõesDosItens := arquivoDePrecoEEstoque.VerificadoresESomarizadoresDoRetorno(int64(qtdeLinhasItens))

	if (len(informaçõesDosItens) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if informaçõesDosItens != "012210114149000010901141490000109     0014000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}
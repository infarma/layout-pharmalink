package arquivoDeRetorno

import (
	"layout-pharmalink/v_3_2/arquivoDeRetorno"
	"testing"
	"time"
)

func TestDadosDoPedido(t *testing.T){
	cnpjPedido := "1141490000109"
	cnpjEntrega := "1141490000109"
	origem := ""
	controleProjeto := "14"

	dadosDoPedido := arquivoDeRetorno.DadosDoPedido(cnpjPedido, cnpjEntrega, origem, controleProjeto)

	if (len(dadosDoPedido) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if dadosDoPedido != "012210114149000010901141490000109     0014000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestDadosDoPedidoDeFaturamento(t *testing.T){
	codigoProjeto := "H"
	numeroPedidoPha := 48
	numeroPedidoCliente := "48"
	cnpjDistribuidora := "24455677000182"

	dadosDoPedidoDeFaturamento := arquivoDeRetorno.DadosDoPedidoDeFaturamento(codigoProjeto, int64(numeroPedidoPha), numeroPedidoCliente, cnpjDistribuidora)

	if (len(dadosDoPedidoDeFaturamento) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if dadosDoPedidoDeFaturamento != "02H  00000000480000000048     244556770001820000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestDatasDoRetorno(t *testing.T){
	datageraRetornoDist := "2019-02-27T14:36:13.001Z"
	datageraRetornoDist2, _ := time.Parse(time.RFC3339, datageraRetornoDist)

	dataRecPedido := "2019-02-27T14:36:13.001Z"
	dataRecPedido2, _ := time.Parse(time.RFC3339, dataRecPedido)

	dtFaturamento := "2019-02-27T14:36:13.001Z"
	dtFaturamento2, _ := time.Parse(time.RFC3339, dtFaturamento)

	datasDoRetorno := arquivoDeRetorno.DatasDoRetorno(datageraRetornoDist2, dataRecPedido2, dtFaturamento2)

	if (len(datasDoRetorno) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if datasDoRetorno != "032702201914362702201914362702201900000000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestItensDoRetorno(t *testing.T){
	ean := "7896094903234"
	codProdPLK := ""
	codProdDist := "0"
	quatidadeAtendida := 0
	quatidadeNaoAtendida := 1
	tipoEmbalagem := ""
	motivoRecusa := "05"
	statusItem := "3"

	itensDoRetorno := arquivoDeRetorno.ItensDoRetorno(ean, codProdPLK, codProdDist, int64(quatidadeAtendida), int64(quatidadeNaoAtendida), tipoEmbalagem, motivoRecusa, statusItem)

	if (len(itensDoRetorno) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if itensDoRetorno != "0407896094903234              0             00000000000000000001   00503" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}

func TestVerificadoresESomarizadoresDoRetorno(t *testing.T){
	qtdLinhasDelt := 13
	qtdUnidAtendidas := 40
	qtdUniNaoAtendidas := 154

	verificadoresESomarizadoresDoRetorno := arquivoDeRetorno.VerificadoresESomarizadoresDoRetorno(int32(qtdLinhasDelt), int64(qtdUnidAtendidas), int64(qtdUniNaoAtendidas))

	if (len(verificadoresESomarizadoresDoRetorno) != 72)  {
		t.Error("Dados Do Pedido não tem o tamanho adequado")
	}else{
		if verificadoresESomarizadoresDoRetorno != "050001300000000000004000000000000015400000000000000000000000000000000000" {
			t.Error("Dados Do Pedido não é compativel")
		}
	}
}
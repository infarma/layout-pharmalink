package util

import (
	"strconv"
	"strings"
)

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(strings.TrimSpace(valor))
	return v
}
func ConvertStringToFloat(valor string) float64 {
	v, _ := strconv.ParseFloat(strings.TrimSpace(valor), 10)
	return v
}

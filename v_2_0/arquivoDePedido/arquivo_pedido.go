package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	IdentificacoesCliente IdentificacoesCliente `json:"IdentificacoesCliente"`
	NumeroPedido          NumeroPedido          `json:"NumeroPedido"`
	EmBrancoRegistro3     EmBrancoRegistro3     `json:"EmBrancoRegistro3"`
	EmBrancoRegistro4     EmBrancoRegistro4     `json:"EmBrancoRegistro4"`
	DataPedido            DataPedido            `json:"DataPedido"`
	HoraPedido            HoraPedido            `json:"HoraPedido"`
	Item                  []Item                `json:"Item"`
	Rodape                Rodape                `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		var index int32
		if identificador == "1" {
			err = arquivo.IdentificacoesCliente.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.NumeroPedido.ComposeStruct(string(runes))
		} else if identificador == "3" {
			err = arquivo.EmBrancoRegistro3.ComposeStruct(string(runes))
		} else if identificador == "4" {
			err = arquivo.EmBrancoRegistro4.ComposeStruct(string(runes))
		} else if identificador == "5" {
			err = arquivo.DataPedido.ComposeStruct(string(runes))
		} else if identificador == "6" {
			err = arquivo.HoraPedido.ComposeStruct(string(runes))
		} else if identificador == "7" {
			err = arquivo.Item[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "8" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}

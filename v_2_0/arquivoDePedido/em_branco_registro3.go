package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type EmBrancoRegistro3 struct {
	Identificador int32  `json:"Identificador"`
	TipoPagamento int32  `json:"TipoPagamento"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (e *EmBrancoRegistro3) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesEmBrancoRegistro3

	err = posicaoParaValor.ReturnByType(&e.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.TipoPagamento, "TipoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesEmBrancoRegistro3 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 1, 0},
	"TipoPagamento": {1, 2, 0},
	"SemUtilizacao": {2, 31, 0},
}

package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type EmBrancoRegistro4 struct {
	Identificador int32  `json:"Identificador"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (e *EmBrancoRegistro4) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesEmBrancoRegistro4

	err = posicaoParaValor.ReturnByType(&e.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesEmBrancoRegistro4 = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 1, 0},
	"SemUtilizacao": {1, 31, 0},
}

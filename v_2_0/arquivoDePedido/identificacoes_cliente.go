package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type IdentificacoesCliente struct {
	Identificador     int32  `json:"Identificador"`
	CnpjCliente       string `json:"CnpjCliente"`
	TipoFaturamento   int32  `json:"TipoFaturamento"`
	ApontadorPromocao string `json:"ApontadorPromocao"`
	CodigoControle    string `json:"CodigoControle"`
}

func (i *IdentificacoesCliente) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIdentificacoesCliente

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoFaturamento, "TipoFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ApontadorPromocao, "ApontadorPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoControle, "CodigoControle")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIdentificacoesCliente = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":     {0, 1, 0},
	"CnpjCliente":       {1, 15, 0},
	"TipoFaturamento":   {15, 16, 0},
	"ApontadorPromocao": {16, 29, 0},
	"CodigoControle":    {29, 31, 0},
}

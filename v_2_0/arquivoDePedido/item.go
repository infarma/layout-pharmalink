package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Item struct {
	Identificador        int32  `json:"Identificador"`
	CodigoEanProduto     int64  `json:"CodigoEanProduto"`
	QuantidadePedida     int32  `json:"QuantidadePedida"`
	TipoOcorrencia       int32  `json:"TipoOcorrencia"`
	CodigoProdutoCliente string `json:"CodigoProdutoCliente"`
	SemUtilizacao        string `json:"SemUtilizacao"`
}

func (i *Item) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItem

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEanProduto, "CodigoEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadePedida, "QuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoOcorrencia, "TipoOcorrencia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProdutoCliente, "CodigoProdutoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItem = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":        {0, 1, 0},
	"CodigoEanProduto":     {1, 14, 0},
	"QuantidadePedida":     {14, 18, 0},
	"TipoOcorrencia":       {18, 20, 0},
	"CodigoProdutoCliente": {20, 27, 0},
	"SemUtilizacao":        {27, 31, 0},
}

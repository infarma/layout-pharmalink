package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type NumeroPedido struct {
	Identificador          int32  `json:"Identificador"`
	NumeroPedidoPharmaLink string `json:"NumeroPedidoPharmaLink"`
	CnpjDistribuidor       string `json:"CnpjDistribuidor"`
	NumeroPedidoCliente    string `json:"NumeroPedidoCliente"`
}

func (n *NumeroPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesNumeroPedido

	err = posicaoParaValor.ReturnByType(&n.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.NumeroPedidoPharmaLink, "NumeroPedidoPharmaLink")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.CnpjDistribuidor, "CnpjDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesNumeroPedido = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":          {0, 1, 0},
	"NumeroPedidoPharmaLink": {1, 9, 0},
	"CnpjDistribuidor":       {9, 23, 0},
	"NumeroPedidoCliente":    {23, 31, 0},
}

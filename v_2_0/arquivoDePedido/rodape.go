package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	Identificador   int32  `json:"Identificador"`
	QUantidadeItens int32  `json:"QUantidadeItens"`
	SemUtilizacao   string `json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QUantidadeItens, "QUantidadeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":   {0, 1, 0},
	"QUantidadeItens": {1, 3, 0},
	"SemUtilizacao":   {3, 31, 0},
}

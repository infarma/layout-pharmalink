package retornoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosDescontos struct {
	Identificador  string  `json:"Identificador"`
	SemUtilizacao1 float64 `json:"SemUtilizacao1"`
	SemUtilizacao2 float64 `json:"SemUtilizacao2"`
	SemUtilizacao3 string  `json:"SemUtilizacao3"`
}

func (d *DadosDescontos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosDescontos

	err = posicaoParaValor.ReturnByType(&d.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao1, "SemUtilizacao1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao2, "SemUtilizacao2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao3, "SemUtilizacao3")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosDescontos = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":  {0, 2, 0},
	"SemUtilizacao1": {2, 12, 2},
	"SemUtilizacao2": {12, 22, 2},
	"SemUtilizacao3": {22, 33, 0},
}

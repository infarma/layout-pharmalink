package retornoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosMercadorias struct {
	Identificador  string  `json:"Identificador"`
	DataEmissao    string  `json:"DataEmissao"`
	SemUtilizacao1 float64 `json:"SemUtilizacao1"`
	SemUtilizacao2 string  `json:"SemUtilizacao2"`
}

func (d *DadosMercadorias) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosMercadorias

	err = posicaoParaValor.ReturnByType(&d.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataEmissao, "DataEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao1, "SemUtilizacao1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao2, "SemUtilizacao2")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosMercadorias = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":  {0, 2, 0},
	"DataEmissao":    {2, 10, 0},
	"SemUtilizacao1": {10, 20, 2},
	"SemUtilizacao2": {20, 33, 0},
}

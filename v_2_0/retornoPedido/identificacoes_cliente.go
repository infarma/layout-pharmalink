package retornoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type IdentificacoesCliente struct {
	Indetificador          string `json:"Indetificador"`
	CnpjCliente            string `json:"CnpjCliente"`
	NumeroPedidoCliente    string `json:"NumeroPedidoCliente"`
	NumeroPedidoPharmaLink string `json:"NumeroPedidoPharmaLink"`
	SemUtilizacao          string `json:"SemUtilizacao"`
}

func (i *IdentificacoesCliente) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIdentificacoesCliente

	err = posicaoParaValor.ReturnByType(&i.Indetificador, "Indetificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroPedidoPharmaLink, "NumeroPedidoPharmaLink")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIdentificacoesCliente = map[string]gerador_layouts_posicoes.Posicao{
	"Indetificador":          {0, 2, 0},
	"CnpjCliente":            {2, 16, 0},
	"NumeroPedidoCliente":    {16, 24, 0},
	"NumeroPedidoPharmaLink": {24, 32, 0},
	"SemUtilizacao":          {32, 33, 0},
}

package retornoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	Identificador             string `json:"Identificador"`
	CodigoEanProduto          int64  `json:"CodigoEanProduto"`
	QuantidadeAtendidaProduto int32  `json:"QuantidadeAtendidaProduto"`
	SemUtilizacao             int64  `json:"SemUtilizacao"`
	MotivoNaoAtendimento      int32  `json:"MotivoNaoAtendimento"`
	Retorno                   string `json:"Retorno"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEanProduto, "CodigoEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeAtendidaProduto, "QuantidadeAtendidaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.MotivoNaoAtendimento, "MotivoNaoAtendimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Retorno, "Retorno")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":             {0, 2, 0},
	"CodigoEanProduto":          {2, 15, 0},
	"QuantidadeAtendidaProduto": {15, 19, 0},
	"SemUtilizacao":             {19, 30, 0},
	"MotivoNaoAtendimento":      {30, 32, 0},
	"Retorno":                   {32, 33, 0},
}

package retornoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	Indentificador   int32  `json:"Indentificador"`
	LivreParaCliente string `json:"LivreParaCliente"`
	SemUtilizacao    string `json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.Indentificador, "Indentificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.LivreParaCliente, "LivreParaCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"Indentificador":   {0, 2, 0},
	"LivreParaCliente": {2, 24, 0},
	"SemUtilizacao":    {24, 33, 0},
}

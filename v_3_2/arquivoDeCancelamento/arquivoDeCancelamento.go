package arquivoDeCancelamento

import (
	"fmt"
	"time"
)

// GeraRetornoPedidosCanceladosPharmalink32
func DadosDeCancelamento01(verLayout string, datCancel time.Time, numCnpjGerPed string, numCnpj string, codCtrPrj string, codPedCli string, codMtvCanCab int8) string {
	cabecalho := fmt.Sprint("01")
	cabecalho += fmt.Sprintf("%-3d", verLayout)
	cabecalho += fmt.Sprint(datCancel.Format("02012006"))
	cabecalho += fmt.Sprint(datCancel.Format("304"))
	cabecalho += fmt.Sprintf("%014d", numCnpjGerPed)
	cabecalho += fmt.Sprintf("%014d", numCnpj)
	cabecalho += fmt.Sprintf("%-4d", codCtrPrj)
	cabecalho += fmt.Sprintf("%010d", codPedCli)
	cabecalho += fmt.Sprintf("%04d", codMtvCanCab)
	return cabecalho
}

func ItensDoCancelamento02(codPrdCli string, qtdAtendi int32, codMtvCanCab int8) string {
	itens := fmt.Sprint("02")
	itens += fmt.Sprintf("%014d", codPrdCli)
	itens += fmt.Sprintf("%010d", qtdAtendi)
	itens += fmt.Sprintf("%010d", qtdAtendi)
	itens += fmt.Sprintf("%03d", codMtvCanCab)
	itens += fmt.Sprint("03")
	itens += fmt.Sprintf("%022d", "0")

	return itens
}

func VerificadoresETotaisDoCancelamento03(totItens int32, totAtendi int32, totNaoAten int32) string {
	itens := fmt.Sprint("03")
	itens += fmt.Sprintf("%05d", totItens)
	itens += fmt.Sprintf("%015d", totAtendi)
	itens += fmt.Sprintf("%015d", totNaoAten)
	itens += fmt.Sprintf("%026d", "0")

	return itens
}

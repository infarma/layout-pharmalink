package arquivoDeNotaFiscal

import (
	"fmt"
	"time"
)

func CabecalhoDaNota(numeroNF int64, serieNF string, tipoNF string, codigoPrazoDeterminado string, qtdeNFPedido int32,
										cnpjPedido string, cnpjEntrega string, cnpjDistribuidor string, controleProjeto string,
										numeroPedidoPharmaLink int64, numeroPedidoCliente string, Chave_NFE string) string{

	tipoLinha := "01"
	verLayout := "221"
	fillerLinha1 := fmt.Sprintf("%02d", 0)

	cabecalhoDaNota := tipoLinha
	cabecalhoDaNota += verLayout
	cabecalhoDaNota += fmt.Sprintf("%09d", numeroNF)
	cabecalhoDaNota += fmt.Sprintf("%03s", serieNF)
	cabecalhoDaNota += fmt.Sprintf("%02s", tipoNF)
	cabecalhoDaNota += fmt.Sprintf("%04s", codigoPrazoDeterminado)
	cabecalhoDaNota += fmt.Sprintf("%02d", qtdeNFPedido)
	cabecalhoDaNota += fmt.Sprintf("%014s", cnpjPedido)
	cabecalhoDaNota += fmt.Sprintf("%014s", cnpjEntrega)
	cabecalhoDaNota += fmt.Sprintf("%014s", cnpjDistribuidor)
	cabecalhoDaNota += fmt.Sprintf("%03s", controleProjeto)
	cabecalhoDaNota += fmt.Sprintf("%010d", numeroPedidoPharmaLink)
	cabecalhoDaNota += fmt.Sprintf("%015s", numeroPedidoCliente)
	cabecalhoDaNota += fmt.Sprintf("%50s", Chave_NFE)
	cabecalhoDaNota += fillerLinha1

	return cabecalhoDaNota
}

func DatasDaNota(dtFaturamentoPed time.Time, dtGerNf time.Time, dtSaidaMerc time.Time, dtEmissNF time.Time, valorTotal float64,
									valorTotalDesc float64, valorTotalLiq float64, valorTotalEncarg float64, valorTotalFrete float64, valorBaseICMS float64,
									valorTotalBaseICMS float64, valorBaseICMS_ST float64, valorTotalBaseICMS_ST float64, valorBaseICMS_RT float64,
									valorTotalBaseICMS_RT float64, valorBaseIPI float64, valorTotalIPE float64) string{

	tipoLinha := "02"
	fillerLinha2 := fmt.Sprintf("%01d", 0)

	cabecalhoDaNota := tipoLinha
	cabecalhoDaNota += fmt.Sprint(dtFaturamentoPed.Format("02012006"))
	cabecalhoDaNota += fmt.Sprint(dtGerNf.Format("020120061504"))
	cabecalhoDaNota += fmt.Sprint(dtSaidaMerc.Format("020120061504"))
	cabecalhoDaNota += fmt.Sprint(dtEmissNF.Format("020120061504"))
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotal)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalDesc)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalLiq)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalEncarg)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalFrete)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorBaseICMS)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalBaseICMS)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorBaseICMS_ST)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalBaseICMS_ST)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorBaseICMS_RT)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalBaseICMS_RT)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorBaseIPI)
	cabecalhoDaNota += fmt.Sprintf("%08d", valorTotalIPE)
	cabecalhoDaNota += fillerLinha2

	return cabecalhoDaNota
}

func ItensDaNota(ean string, codProdPLK string, codProdDist string, quantidadeFaturada int64, tipoEmbalagem string,
								descontoComercial float32, descontoFinanceiro float32, descontoTotal float32, valorBrutoUnit float64,
								valorDescontoUnit float64, valorLiquidoUnit float64, valorBrutoLinha float64, valorLiquidoLinha float64,
								motivo string) string{

	tipoLinha := "03"
	fillerLinha3 := fmt.Sprintf("%036d", 0)

	itensDaNota := tipoLinha
	itensDaNota += fmt.Sprintf("%014s", ean)
	itensDaNota += fmt.Sprintf("%014s", codProdPLK)
	itensDaNota += fmt.Sprintf("%014s", codProdDist)
	itensDaNota += fmt.Sprintf("%010d", quantidadeFaturada)
	itensDaNota += fmt.Sprintf("%03s", tipoEmbalagem)
	itensDaNota += fmt.Sprintf("%05d", descontoComercial)
	itensDaNota += fmt.Sprintf("%05d", descontoFinanceiro)
	itensDaNota += fmt.Sprintf("%05d", descontoTotal)
	itensDaNota += fmt.Sprintf("%08d", valorBrutoUnit)
	itensDaNota += fmt.Sprintf("%08d", valorDescontoUnit)
	itensDaNota += fmt.Sprintf("%08d", valorLiquidoUnit)
	itensDaNota += fmt.Sprintf("%08d", valorBrutoLinha)
	itensDaNota += fmt.Sprintf("%08d", valorLiquidoLinha)
	itensDaNota += fmt.Sprintf("%03s", motivo)
	itensDaNota += fillerLinha3

	return itensDaNota
}

func VerificadoresESomadoresDaNota(qtdeItens int32, qtdUnidFaturadas int64) string{

	tipoLinha := "04"
	fillerLinha4 := fmt.Sprintf("%0129d", 0)

	itensDaNota := tipoLinha
	itensDaNota += fmt.Sprintf("%05d", qtdeItens)
	itensDaNota += fmt.Sprintf("%015d", qtdUnidFaturadas)
	itensDaNota += fillerLinha4

	return itensDaNota
}
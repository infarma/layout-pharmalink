package arquivoDePedido

import (
	"bufio"
	"os"
)

// Arquivo de Pedido
type ArquivoDePedido struct {
	DadosDoCompradorEDeFaturamentoDoPedido DadosDoCompradorEDeFaturamentoDoPedido `json:"DadosDoCompradorEDeFaturamentoDoPedido"`
	NumeroDoPedido                         []NumeroDoPedido                       `json:"NumeroDoPedido"`
	FormasDePagamentoDoPedido              []FormasDePagamentoDoPedido            `json:"FormasDePagamentoDoPedido"`
	DatasDoPedido                          []DatasDoPedido                        `json:"DatasDoPedido"`
	ItensDoPedido                          []ItensDoPedido                        `json:"ItensDoPedido"`
	VerificadoresETotalizadoresDoPedido    VerificadoresETotalizadoresDoPedido    `json:"VerificadoresETotalizadoresDoPedido"`
}

func ConvertToStruct(fileHandle *os.File) ArquivoDePedido {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			arquivo.DadosDoCompradorEDeFaturamentoDoPedido = GetDadosDoComprador(runes)
		} else if identificador == "02" {
			arquivo.NumeroDoPedido = append(arquivo.NumeroDoPedido, GetNumeroDoPedido(runes))
		} else if identificador == "03" {
			arquivo.FormasDePagamentoDoPedido = append(arquivo.FormasDePagamentoDoPedido, GetFormasDePagamentoDoPedido(runes))
		} else if identificador == "04" {
			arquivo.DatasDoPedido = append(arquivo.DatasDoPedido, GetDatasDoPedido(runes))
		} else if identificador == "05" {
			arquivo.ItensDoPedido = append(arquivo.ItensDoPedido, GetItensDoPedido(runes))
		} else if identificador == "06" {
			arquivo.VerificadoresETotalizadoresDoPedido = GetVerificadoresETotalizadoresDoPedido(runes)
		}
	}
	return arquivo
}

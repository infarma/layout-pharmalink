package arquivoDePedido

import (
	"strconv"
	"strings"
)

type DadosDoCompradorEDeFaturamentoDoPedido struct {
	TipoLinha       string `json:"TipoLinha"`
	VerLayout       string `json:"VerLayout"`
	CnpjPedido      string `json:"CnpjPedido"`
	CnpjEntrega     string `json:"CnpjEntrega"`
	Faturamento     int64  `json:"Faturamento"`
	TipoPedido      int64  `json:"TipoPedido"`
	Origem          string `json:"Origem"`
	ControleProjeto string `json:"ControleProjeto"`
	FillerLinha1    string `json:"FillerLinha1"`
}

func GetDadosDoComprador(runes []rune) DadosDoCompradorEDeFaturamentoDoPedido {
	dadosDoComprador := DadosDoCompradorEDeFaturamentoDoPedido{}

	dadosDoComprador.TipoLinha = string(runes[0:2])

	dadosDoComprador.VerLayout = strings.TrimSpace(string(runes[2:5]))

	dadosDoComprador.CnpjPedido = strings.TrimSpace(string(runes[5:19]))

	dadosDoComprador.CnpjEntrega = strings.TrimSpace(string(runes[19:33]))

	faturamento, _ := strconv.ParseInt(string(runes[33:35]), 10, 64)
	dadosDoComprador.Faturamento = faturamento

	tipoPedido, _ := strconv.ParseInt(string(runes[35:37]), 10, 64)
	dadosDoComprador.TipoPedido = tipoPedido

	dadosDoComprador.Origem = strings.TrimSpace(string(runes[37:43]))

	dadosDoComprador.ControleProjeto = strings.TrimSpace(string(runes[43:47]))

	dadosDoComprador.FillerLinha1 = strings.TrimSpace(string(runes[47:92]))

	return dadosDoComprador
}

package arquivoDePedido

import (
	"strings"
)

type DatasDoPedido struct {
	TipoLinha           string `json:"TipoLinha"`
	DtGeraPedidoPdv     string `json:"DtGeraPedidoPdv"`
	HrGeraPedidoPdv     string `json:"HrGeraPedidoPdv"`
	DtGeraPedidoPlk     string `json:"DtGeraPedidoPlk"`
	HrGeraPedidoPlk     string `json:"HrGeraPedidoPlk"`
	DtEnvioArquivoPlk   string `json:"DtEnvioArquivoPlk"`
	HrEnvioArquivoPlk   string `json:"HrEnvioArquivoPlk"`
	DtFaturamentoPedido string `json:"DtFaturamentoPedido"`
	FillerLinha4        string `json:"FillerLinha4"`
}

func GetDatasDoPedido(runes []rune) DatasDoPedido {
	datasDoPedido := DatasDoPedido{}

	datasDoPedido.TipoLinha = string(runes[0:2])

	datasDoPedido.DtGeraPedidoPdv = strings.TrimSpace(string(runes[2:10]))

	datasDoPedido.HrGeraPedidoPdv = strings.TrimSpace(string(runes[10:16]))

	datasDoPedido.DtGeraPedidoPlk = strings.TrimSpace(string(runes[16:24]))

	datasDoPedido.HrGeraPedidoPlk = strings.TrimSpace(string(runes[24:30]))

	datasDoPedido.DtEnvioArquivoPlk = strings.TrimSpace(string(runes[30:38]))

	datasDoPedido.HrEnvioArquivoPlk = strings.TrimSpace(string(runes[38:44]))

	datasDoPedido.DtFaturamentoPedido = strings.TrimSpace(string(runes[44:52]))

	datasDoPedido.FillerLinha4 = strings.TrimSpace(string(runes[52:92]))

	return datasDoPedido
}
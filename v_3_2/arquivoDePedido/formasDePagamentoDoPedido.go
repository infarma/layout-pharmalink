package arquivoDePedido

import (
	"strconv"
	"strings"
)

type FormasDePagamentoDoPedido struct {
	TipoLinha              string `json:"TipoLinha"`
	TipoPagamento          string `json:"TipoPagamento"`
	CodigoPrazoDeterminado string `json:"CodigoPrazoDeterminado"`
	NumeroDiasPrazo        int64  `json:"NumeroDiasPrazo"`
	NumeroPedidoPrincipal  int64  `json:"NumeroPedidoCliente"`
	FillerLinha3           string `json:"FillerLinha3"`
}

func GetFormasDePagamentoDoPedido(runes []rune) FormasDePagamentoDoPedido {
	formasDePagamentoDoPedido := FormasDePagamentoDoPedido{}

	formasDePagamentoDoPedido.TipoLinha = string(runes[0:2])

	formasDePagamentoDoPedido.TipoPagamento = strings.TrimSpace(string(runes[2:4]))

	formasDePagamentoDoPedido.CodigoPrazoDeterminado = strings.TrimSpace(string(runes[4:8]))

	numeroDiasPrazo, _ := strconv.ParseInt(string(runes[8:11]), 10, 64)
	formasDePagamentoDoPedido.NumeroDiasPrazo = numeroDiasPrazo

	numeroPedidoPrincipal, _ := strconv.ParseInt(string(runes[11:21]), 10, 64)
	formasDePagamentoDoPedido.NumeroPedidoPrincipal = numeroPedidoPrincipal

	formasDePagamentoDoPedido.FillerLinha3 = strings.TrimSpace(string(runes[21:92]))

	return formasDePagamentoDoPedido
}
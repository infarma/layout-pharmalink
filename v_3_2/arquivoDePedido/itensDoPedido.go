package arquivoDePedido

import (
	"strconv"
	"strings"
)

type ItensDoPedido struct {
	TipoLinha        string `json:"TipoLinha"`
	Ean              string `json:"Ean"`
	CodProdPlk       string `json:"CodProdPlk"`
	CodProdDist      string `json:"CodProdDist"`
	QuantidadePedida int64  `json:"QuantidadePedida"`
	TipoEmbalagem    string `json:"TipoEmbalagem"`
	TipoDesconto     int64  `json:"TipoDesconto"`
	Desconto         float64 `json:"Desconto"`
	ValorBrutoUnit   float64 `json:"ValorBrutoUnit"`
	ValorDescUnit    float64 `json:"ValorDescUnit"`
	ValorLiquidoUnit float64 `json:"ValorLiquidoUnit"`
	FillerLinha5     string `json:"FillerLinha5"`
}

func GetItensDoPedido(runes []rune) ItensDoPedido {
	itensDoPedido := ItensDoPedido{}

	itensDoPedido.TipoLinha = string(runes[0:2])

	itensDoPedido.Ean = strings.TrimSpace(string(runes[2:15]))

	itensDoPedido.CodProdPlk = strings.TrimSpace(string(runes[15:28]))

	itensDoPedido.CodProdDist = strings.TrimSpace(string(runes[28:41]))

	quantidadePedida, _ := strconv.ParseInt(string(runes[41:51]), 10, 64)
	itensDoPedido.QuantidadePedida = quantidadePedida

	itensDoPedido.TipoEmbalagem = strings.TrimSpace(string(runes[51:54]))

	tipoDesconto, _ := strconv.ParseInt(string(runes[54:55]), 10, 64)
	itensDoPedido.TipoDesconto = tipoDesconto

	desconto, _ := strconv.ParseFloat(string(runes[55:60]), 64)
	itensDoPedido.Desconto = desconto/100

	valorBrutoUnit, _ := strconv.ParseFloat(string(runes[60:68]), 64)
	itensDoPedido.ValorBrutoUnit = valorBrutoUnit/100

	valorDescUnit, _ := strconv.ParseFloat(string(runes[68:76]), 64)
	itensDoPedido.ValorDescUnit = valorDescUnit/100

	valorLiquidoUnit, _ := strconv.ParseFloat(string(runes[76:84]), 64)
	itensDoPedido.ValorLiquidoUnit = valorLiquidoUnit/100

	itensDoPedido.FillerLinha5 = strings.TrimSpace(string(runes[84:92]))

	return itensDoPedido
}

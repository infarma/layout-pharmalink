package arquivoDePedido

import (
	"strconv"
	"strings"
)

type NumeroDoPedido struct {
	TipoLinha              string `json:"TipoLinha"`
	CodigoProjeto          string `json:"CodigoProjeto"`
	NumeroPedidoPharmaLink int64  `json:"NumeroPedidoPharmaLink"`
	NumeroPedidoEletronico int64  `json:"NumeroPedidoEletronico"`
	NumeroPedidoCliente    string `json:"NumeroPedidoCliente"`
	CnpjDistribuidor       string `json:"CnpjDistribuidor"`
	FillerLinha2           string `json:"FillerLinha2"`
}

func GetNumeroDoPedido(runes []rune) NumeroDoPedido {
	numeroDoPedido := NumeroDoPedido{}

	numeroDoPedido.TipoLinha = string(runes[0:2])

	numeroDoPedido.CodigoProjeto = strings.TrimSpace(string(runes[2:5]))

	numeroPedidoPharmaLink, _ := strconv.ParseInt(string(runes[5:15]), 10, 64)
	numeroDoPedido.NumeroPedidoPharmaLink = numeroPedidoPharmaLink

	numeroPedidoEletronico, _ := strconv.ParseInt(string(runes[15:25]), 10, 64)
	numeroDoPedido.NumeroPedidoEletronico = numeroPedidoEletronico

	numeroDoPedido.NumeroPedidoCliente = strings.TrimSpace(string(runes[25:40]))

	numeroDoPedido.CnpjDistribuidor = strings.TrimSpace(string(runes[40:54]))

	numeroDoPedido.FillerLinha2 = strings.TrimSpace(string(runes[54:92]))

	return numeroDoPedido
}

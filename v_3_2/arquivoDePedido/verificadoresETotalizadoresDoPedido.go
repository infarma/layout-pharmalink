package arquivoDePedido

import (
	"strconv"
	"strings"
)

type VerificadoresETotalizadoresDoPedido struct {
	TipoLinha          string `json:"TipoLinha"`
	QtdeLinhasDeItens  int64  `json:"QtdeLinhasDeItens"`
	SomaUnidades       int64  `json:"SomaUnidades"`
	ValorBrutoPedido   float64 `json:"ValorBrutoPedido"`
	ValorLiquidoPedido float64 `json:"ValorLiquidoPedido"`
	FillerLinha6       string `json:"FillerLinha6"`
}

func GetVerificadoresETotalizadoresDoPedido(runes []rune) VerificadoresETotalizadoresDoPedido {
	totalizadorPedido := VerificadoresETotalizadoresDoPedido{}

	totalizadorPedido.TipoLinha = string(runes[0:2])

	qtdeLinhasDeItens, _ := strconv.ParseInt(string(runes[2:7]), 10, 64)
	totalizadorPedido.QtdeLinhasDeItens = qtdeLinhasDeItens

	somaUnidades, _ := strconv.ParseInt(string(runes[7:22]), 10, 64)
	totalizadorPedido.SomaUnidades = somaUnidades

	valorBrutoPedido, _ := strconv.ParseFloat(string(runes[22:32]), 64)
	totalizadorPedido.ValorBrutoPedido = valorBrutoPedido/100

	valorLiquidoUnit, _ := strconv.ParseFloat(string(runes[32:42]), 64)
	totalizadorPedido.ValorLiquidoPedido = valorLiquidoUnit/100

	totalizadorPedido.FillerLinha6 = strings.TrimSpace(string(runes[42:92]))

	return totalizadorPedido
}

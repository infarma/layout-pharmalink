package arquivoDePrecoEEstoque

import (
	"fmt"
	"time"
)

func DadosDoEmissor(dataConsulta time.Time, cnpjCD string, controleProjeto string, tipoAtualizacao int32, uf string) string{

	tipoLinha := "01"
	verLayout := "221"
	fillerLinha1 := fmt.Sprintf("%019d", 0)

	dadosDoEmissor := tipoLinha
	dadosDoEmissor += verLayout
	dadosDoEmissor += fmt.Sprint(dataConsulta.Format("02012006150405"))
	dadosDoEmissor += fmt.Sprintf("%014s", cnpjCD)
	dadosDoEmissor += fmt.Sprintf("%04s", controleProjeto)
	dadosDoEmissor += fmt.Sprintf("%02d", tipoAtualizacao)
	dadosDoEmissor += fmt.Sprintf("%02s", uf)
	dadosDoEmissor += fillerLinha1

	return dadosDoEmissor
}

func InformacoesDosItens(ean string, codProdCD string, estoque int64, preco float64, desconto float64, status int32) string{

	tipoLinha := "02"
	fillerLinha2 := fmt.Sprintf("%03d", 0)

	dadosDoEmissor := tipoLinha
	dadosDoEmissor += fmt.Sprintf("%014s", ean)
	dadosDoEmissor += fmt.Sprintf("%014s", codProdCD)
	dadosDoEmissor += fmt.Sprintf("%010d", estoque)
	dadosDoEmissor += fmt.Sprintf("%010d", preco)
	dadosDoEmissor += fmt.Sprintf("%05d", desconto)
	dadosDoEmissor += fmt.Sprintf("%02d", status)
	dadosDoEmissor += fillerLinha2

	return dadosDoEmissor
}

func VerificadoresESomarizadoresDoRetorno(qtdeLinhasItens int64) string{

	tipoLinha := "03"
	fillerLinha3 := fmt.Sprintf("%053d", 0)

	dadosDoEmissor := tipoLinha
	dadosDoEmissor += fmt.Sprintf("%05d", qtdeLinhasItens)
	dadosDoEmissor += fillerLinha3

	return dadosDoEmissor
}
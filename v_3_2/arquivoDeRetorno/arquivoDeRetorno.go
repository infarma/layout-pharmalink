package arquivoDeRetorno

import (
	"fmt"
	"strconv"
	"time"
)

// * Motivos do não Atendimento
const problemasCadastrais01 string = "PROBLEMAS CADASTRAIS"
const faltaDeLimiteDeCredito02 string = "FALTA DE LIMITE DE CRÉDITO"
const naoAlcancouValorMinimo03 string = "NÃO ALCANÇOU VALOR MÍNIMO"
const layoutIncorreto04 string = "LAYOUT INCORRETO"
const produtoNaoCadastradoDesativadoEanIncorreto05 string = "PRODUTO NÃO CADASTRADO / DESATIVADO / EAN INCORRETO"
const faltaDeEstoque06 string = "FALTA DE ESTOQUE"
const estoqueInsuficiente07 string = "ESTOQUE INSUFICIENTE"
const clienteSemAlvaraVencido08 string = "CLIENTE SEM ALVARÁ / VENCIDO"
const clienteInvalido09 string = "CLIENTE INVÁLIDO"
const produtoBloqueado10 string = "PRODUTO BLOQUEADO"
const produtoNaoCadastradoNoProjeto11 string = "PRODUTO NÃO CADASTRADO NO PROJETO"
const pedidoJaEnviado12 string = "PEDIDO JÁ ENVIADO"
const clienteBloqueado13 string = "CLIENTE BLOQUEADO"
const pedidoNaoProcessadoPorHorarioUltrapassado51 string = "PEDIDO NÃO PROCESSADO POR HORÁRIO ULTRAPASSADO"
const cdIncorreto83 string = "CD INCORRETO"
const motivoNaoEspecificado99 string = "MOTIVO NÃO ESPECIFICADO"

func DadosDoPedido(cnpjPedido string, cnpjEntrega string, origem string, controleProjeto string) string{
	tipoLinha := "01"
	verLayout := "221"
	fillerLinha1 := fmt.Sprintf("%030d", 0)

	dadosDoPedido := tipoLinha
	dadosDoPedido += verLayout
	dadosDoPedido += fmt.Sprintf("%014s", cnpjPedido)
	dadosDoPedido += fmt.Sprintf("%014s", cnpjEntrega)
	dadosDoPedido += fmt.Sprintf("%5s", origem)
	dadosDoPedido += fmt.Sprintf("%04s", controleProjeto)
	dadosDoPedido += fillerLinha1

	return dadosDoPedido
}

func DadosDoPedidoDeFaturamento(controleProjeto string, numeroPedidoPha int64, numeroPedidoCliente string, cnpjDistribuidora string) string{
	tipoLinha := "02"
	fillerLinha2 := fmt.Sprintf("%028d", 0)

	dadosDoPedidoDeFaturamento := tipoLinha
	dadosDoPedidoDeFaturamento += fmt.Sprintf("%-3s", controleProjeto)
	dadosDoPedidoDeFaturamento += fmt.Sprintf("%010d", numeroPedidoPha)
	dadosDoPedidoDeFaturamento += fmt.Sprintf("%010s", numeroPedidoCliente)
	dadosDoPedidoDeFaturamento += fmt.Sprintf("%-5s", "")
	dadosDoPedidoDeFaturamento += fmt.Sprintf("%014s", cnpjDistribuidora)
	dadosDoPedidoDeFaturamento += fillerLinha2

	return dadosDoPedidoDeFaturamento
}

func DatasDoRetorno(datageraRetornoDist time.Time, dataRecPedido time.Time, dtFaturamento time.Time) string{
	tipoLinha := "03"
	fillerLinha3 := fmt.Sprintf("%038d", 0)

	datasDoRetorno := tipoLinha
	datasDoRetorno += fmt.Sprint(datageraRetornoDist.Format("020120061504"))
	datasDoRetorno += fmt.Sprint(dataRecPedido.Format("020120061504"))
	datasDoRetorno += fmt.Sprint(dtFaturamento.Format("02012006"))
	datasDoRetorno += fillerLinha3

	return datasDoRetorno
}

func ItensDoRetorno(ean string, codProdPLK string, codProdDist string, quatidadeAtendida int64, quatidadeNaoAtendida int64, tipoEmbalagem string, motivoRecusa string, statusItem string) string{
	tipoLinha := "04"

	itensDoRetorno := tipoLinha
	itensDoRetorno += fmt.Sprintf("%014s", ean)
	itensDoRetorno += fmt.Sprintf("%14s", codProdPLK)
	itensDoRetorno += fmt.Sprintf("%-14s", codProdDist)
	itensDoRetorno += fmt.Sprintf("%010d", quatidadeAtendida)
	itensDoRetorno += fmt.Sprintf("%010d", quatidadeNaoAtendida)
	itensDoRetorno += fmt.Sprintf("%3s", tipoEmbalagem)
	itensDoRetorno += fmt.Sprintf("%03s", motivoRecusa)
	itensDoRetorno += fmt.Sprintf("%02s", statusItem)

	return itensDoRetorno
}

func VerificadoresESomarizadoresDoRetorno(qtdLinhasDelt int32, qtdUnidAtendidas int64, qtdUniNaoAtendidas int64) string{
	tipoLinha := "05"
	fillerLinha5 := fmt.Sprintf("%035d", 0)

	verificadoESomarizador := tipoLinha
	verificadoESomarizador += fmt.Sprintf("%05d", qtdLinhasDelt)
	verificadoESomarizador += fmt.Sprintf("%015d", qtdUnidAtendidas)
	verificadoESomarizador += fmt.Sprintf("%015d", qtdUniNaoAtendidas)
	verificadoESomarizador += fillerLinha5

	return verificadoESomarizador
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}